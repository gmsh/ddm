
## The electromagnetic transmission-scattering problem

Dependencies:
* GmshFEM, commit: bd0fb7ebb6b76f0541e25212b9cbcc42e7a0d0b7
* Gmsh, commit: 2db112186d43fab77fca9d2b546c8f76b3ea6169
* GmshDdm, commit: 2a2746ca938924279ad9efbe15e4dc000370ed7f
* Bempp-cl, commit: 293135531f385f39ba29cf1c52cc2dba84614e6b


## Problem description
Let $` \Omega_{-} `$ be a unit sphere (the scatterer) and $`\Omega_{+} `$ the exterior domain. The boundary of $` \Omega_{-} `$  is denoted by $`\Gamma`$.
We consider an incident electromagnetic plane wave propagating in $`\Omega_{+}`$. The total wave field $` \mathbf{E}`$ verifies the following  three-dimensional electromagnetic transmission-scattering problem:

```math
\mathbf{curl}((k_{-}\mathcal{Z}_{-})^{-1}\mathbf{curl }~\mathbf{E})- k_{-}\mathcal{Z}_{-}^{-1}\mathbf{E} =  \textbf{0} \text{ ~in~ } \Omega_{-}, \\
\mathbf{curl}\mathbf{curl }~\mathbf{E}- k_{+}^{2}\mathbf{E} =  \textbf{0} \text{ ~in~ } \Omega_{+},\\ 
```
with $` k_\pm`$ and $`\mathcal{Z}_\pm`$ the wavenumbers and the impedances associated with  $` \Omega_\pm`$ respectively.  The total electric field satisfied the transmission conditions on $`\Gamma`$. For the exterior problem to be well posed and physically admissible, we assume that the scattered field verifies   the Silver-Muller radiation condition at infinity.

## Method overview:

The method is an efficient weak coupling formulation between the boundary element method
and the high-order finite element method. The approach is based on the use of a non-overlapping
domain decomposition method involving quasi-optimal transmission operators. The associated
transmission boundary conditions are constructed through a localization process based on complex
rational Padé approximants of the nonlocal Magnetic-to-Electric operators (see I. Badia, B. Caudron, X. Antoine, C. Geuzaine. "A well-conditioned weak coupling of boundary element and high-order finite element methods for time-harmonic electromagnetic scattering by inhomogeneous objects". SIAM Journal on Scientific Computing, Society for Industrial and Applied Mathematics, 2022)

## Installation

Run

```
  mkdir build && cd build
  cmake ..
  make
  cd ..
```


## Usage

Run:
```
  ./build/example [PARAM]
```
with `[PARAM]`:
* `-pade [x]`  (with `[x]` equal to 1 or 0)  is a parameter to indicate to use Padé or not
* `-pointsByWl [x]` where `[x]` is the mesh size.
* `-FEorderAlpha [x]` is the order of the finite element hierarchical basis associated with the interior electric field.
* `-FEorderAlpha [x]` is the order of the finite element hierarchical basis associated with certain fields.
* `-help` to display all other available parameters

The folder contains
* the main script (`main.cpp`)
* A python script for the BEM part (`bem.py`)
* A folder for information exchange between python and C++ (`pybind11`)
