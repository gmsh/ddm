#include <gmsh.h>
#include <math.h>
#include <iostream>

void meshExhaust3D(const double Ly, const double lc, const double lcI, const int MeshElemOrder, bool ActivePMLCore=false, bool ActivePMLByPass=false);
