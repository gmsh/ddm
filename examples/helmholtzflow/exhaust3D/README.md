# Flow acoustics problem - radiation from a turbofan engine exhaust

Dependency(ies):
* GmshDDM : v 1.0.0
* GmshFEM : v 1.0.0
* Gmsh

Specific requirements:
* Gmsh must be compiled with OpenCascade
* GmshFEM must be compiled with the Complex Bessel library
* MPI must be enabled
* Mean Flow data
* Geometry description

## Problem description

This benchmark solves the acoustic radiation of a turbofan exhaust, which is modeled by the Pierce convected time-harmonic wave operator

```math
\mathcal{P} = -\rho_0\frac{D}{Dt}\left(\frac{1}{\rho_0^2 c_0^2} \frac{D}{Dt}\right) + \nabla \cdot \left( \frac{1}{\rho_0} \nabla \right),
```

where $`\rho_0(\boldsymbol{x}), c_0(\boldsymbol{x})`$ are respectively the density and speed of sound of the mean flow, and $`\boldsymbol{v}_0(\boldsymbol{x})`$ is the 3D vector velocity field, which is assumed to be subsonic so that the condition $`\| \boldsymbol{v}_0(\boldsymbol{x}) \|/c_0(\boldsymbol{x}) < 1`$ holds.

The problem is prescribed by an input plane wave in the core duct. Perfectly matched layers are set up in the bypass duct to damp back-reflected waves, and truncate the cylindrical exterior domain.

### Running domain decomposition
1. Partitioned mesh generation
```
  ./example -nDom [x] -FEMorder [x] -meshOnly -meshSizeFactor [x] -maxThreads [x]
```
The parameter `-meshSizeFactor` can be used to adjust the global mesh refinement, and `-maxThreads` controls the number of threads.

2. Launch the solver at a given frequency `-freq`
```
  mpirun -np [x] ./example -mesh turnex3D -nDom [x] -FEMorder [x] -meshSizeFactor [x] -freq [x] -maxThreads [x] -memoryDDM
```
DDM transmission conditions are set up by default to be second order Taylor conditions with the rotation branch-cut set to $`\alpha=-\pi/2`$.

## References
> P. Marchner, H. Bériot, S. Le Bras, X. Antoine and C. Geuzaine. A domain decomposition solver for large scale time-harmonic flow acoustics problems. (2023), Preprint [HAL-04254633](https://hal.science/hal-04254633).