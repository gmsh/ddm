cd build
cmake .. && make

echo "************ Taylor0 ************"
./example -nDom 5 -Transmission Taylor0 -alpha 0 -M 0.95 > out.txt
grep "relative residual" out.txt | while read -r line ; do
    awk '{print $13}' >> resT0Gmres.txt
done
rm out.txt
echo "************ Taylor2 - alpha=-pi/4 ************"
./example -nDom 5 -Transmission Taylor2 -M 0.95 -alpha -0.78539816339 > out.txt
grep "relative residual" out.txt | while read -r line ; do
    awk '{print $13}' >> resT2Gmres.txt
done
rm out.txt
echo "************ Pade8 - alpha=-pi/4 ************"
./example -nDom 5 -Transmission Pade -padeOrder 8 -M 0.95 -alpha -0.78539816339 > out.txt
grep "relative residual" out.txt | while read -r line ; do
    awk '{print $13}' >> resP4Gmres.txt
done
rm out.txt
echo "************ Pade8 ************"
./example -nDom 5 -Transmission Pade -padeOrder 8 -M 0.95 > out.txt
grep "relative residual" out.txt | while read -r line ; do
    awk '{print $13}' >> resP8Gmres.txt
done
rm out.txt
# jacobi
echo "************ Pade8 - jacobi - alpha=-pi/4 ************"
./example -nDom 5 -Transmission Pade -padeOrder 8 -solver jacobi -M 0.95 -alpha -0.78539816339 > out.txt
grep "relative residual" out.txt | while read -r line ; do
    awk '{print $13}' >> resP4Jacobi.txt
done
rm out.txt
echo "************ Pade8 - jacobi ************"
./example -nDom 5 -Transmission Pade -padeOrder 8 -solver jacobi -M 0.95 > out.txt
grep "relative residual" out.txt | while read -r line ; do
    awk '{print $13}' >> resP8Jacobi.txt
done
rm out.txt

paste -d "\t\t\t\t\t\t" resT0Gmres.txt resT2Gmres.txt resP4Gmres.txt resP8Gmres.txt resP4Jacobi.txt resP8Jacobi.txt >> RelRes.txt
rm resT0Gmres.txt resT2Gmres.txt resP4Gmres.txt resP8Gmres.txt resP4Jacobi.txt resP8Jacobi.txt