#include "gmsh.h"
#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshfem/AnalyticalFunction.h>
#include <gmshfem/Message.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::analytics;
using namespace gmshfem::post;
using namespace gmshfem::equation;

//***************************************
// GEOMETRY
//***************************************

void circlesConcentric(const unsigned int nDom, const double rInf, const double rPml, const double xs, const double ys, const double lc, const int ElemOrder)
{
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2." << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("circlesConcentric");

  int tagPoint = gmsh::model::occ::addPoint(xs, ys, 0., lc/5.); // Source point
  double Rs = 2. * lc;
  int CircleErr = gmsh::model::occ::addCircle(xs, ys, 0., Rs);

  std::vector< int > tagsCircle(nDom + 1);
  for(unsigned int i = 0; i < nDom; ++i) {
    tagsCircle[i] = gmsh::model::occ::addCircle(0., 0., 0., std::sqrt(double((i + 1) * (rInf * rInf)) / nDom) );
  }
  tagsCircle[nDom] = gmsh::model::occ::addCircle(0., 0., 0., std::sqrt(double(rPml * rPml)));

  int tagCurveCircleErr = gmsh::model::occ::addCurveLoop({CircleErr});
  std::vector< int > tagsCurve(nDom + 1);
  for(unsigned int i = 0; i < (nDom + 1); ++i) {
    tagsCurve[i] = gmsh::model::occ::addCurveLoop({tagsCircle[i]});
  }

  int tagCircleErr = gmsh::model::occ::addPlaneSurface({tagCurveCircleErr});
  std::vector< int > tagsSurface(nDom + 1);
  tagsSurface[0] = gmsh::model::occ::addPlaneSurface({tagsCurve[0],tagCurveCircleErr});
  for(unsigned int i = 1; i < (nDom + 1); ++i) {
    tagsSurface[i] = gmsh::model::occ::addPlaneSurface({tagsCurve[i - 1], tagsCurve[i]});
  }

  gmsh::model::occ::synchronize();
  gmsh::model::mesh::embed(0, {tagPoint}, 2, tagCircleErr);//1, tagsSurface[0]);//tagCircleErr);

  for(unsigned int i = 0; i < nDom; ++i) {
    gmsh::model::addPhysicalGroup(2, {tagsCircle[i]}, 100 + i);
    gmsh::model::setPhysicalName(2, 100 + i, "omega_" + std::to_string(i));

    if(i != nDom - 1) {
      gmsh::model::addPhysicalGroup(1, {tagsCircle[i]}, 200 + i);
      gmsh::model::setPhysicalName(1, 200 + i, "sigma_" + std::to_string(i));
    }
  }

  gmsh::model::addPhysicalGroup(1, {tagsCircle[nDom - 1]}, 92);
  gmsh::model::setPhysicalName(1, 92, "gammaInf");

  gmsh::model::addPhysicalGroup(1, {tagsCircle[nDom]}, 93);
  gmsh::model::setPhysicalName(1, 93, "gammaPml");

  gmsh::model::addPhysicalGroup(0, {tagPoint}, 999);
  gmsh::model::setPhysicalName(0, 999, "source");

  gmsh::model::addPhysicalGroup(2, {tagsCircle[nDom]}, 100 + nDom);
  gmsh::model::setPhysicalName(2, 100 + nDom, "omega_pml");

  gmsh::model::addPhysicalGroup(2, {tagCircleErr}, 9000);
  gmsh::model::setPhysicalName(2, 9000, "omega_source");

  std::vector< std::pair< int, int > > out;
  gmsh::model::getEntities(out, 0);
  gmsh::model::mesh::setSize(out, lc);
  gmsh::model::mesh::setSize({out[0]}, lc/3.);
  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate(2);
  gmsh::model::mesh::setOrder(ElemOrder);
  gmsh::write("m.msh");
}

//***************************************
// FORMULATION
//***************************************

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  unsigned int nDom = 5;
  gmshDdm.userDefinedParameter(nDom, "nDom");
  const double pi = 3.14159265358979323846264338327950288;
  const std::complex< double > im(0., 1.);

  // geometry parameters
  double R = 1.; // Disk radius
  gmshDdm.userDefinedParameter(R, "R");
  double pointsByWl = 8;
  gmshDdm.userDefinedParameter(pointsByWl, "pointsByWl");
  // physical parameters
  double k = 6 * pi; // free field wavenumber
  gmshDdm.userDefinedParameter(k, "k");
  double M = 0.8; // Mach number
  gmshDdm.userDefinedParameter(M, "M");

  // numerical parameters
  int FEMorder;
  if (M >= 0.9) {
    FEMorder = 9;
  }
  else if (M < 0.9 && M >= 0.7) {
    FEMorder = 6;
  }
  else if (M < 0.7 && M >= 0.4) {
    FEMorder = 4;
  }
  else {
    FEMorder = 2;
  }

  std::string gauss = "Gauss" + std::to_string(2 * FEMorder + 1);
  double lc = (2 * pi * FEMorder * (1 - abs(M))) / (pointsByWl * k); // meshsize
  int Npml = 6; // number of PML layers
  double rPML = R + Npml * lc; // PML of width Npml*lc

  msg::info << "pointsByWl =  " << (2 * pi * FEMorder * (1 - abs(M))) / (lc * k) << msg::endl;
  // souce point location
  double xs = 0.;
  double ys = 0.;
  gmshDdm.userDefinedParameter(xs, "xs");
  gmshDdm.userDefinedParameter(ys, "ys");

  std::string Transmission = "Pade"; // transmission condition
  gmshDdm.userDefinedParameter(Transmission, "Transmission");
  double alpha = -pi/2.;
  gmshDdm.userDefinedParameter(alpha, "alpha");
  int padeOrder = 8;
  gmshDdm.userDefinedParameter(padeOrder, "padeOrder");
  msg::info << "use " << Transmission << "transmission condition with rotation branch-cut alpha = " << alpha << " rad" << msg::endl;

  // Build geometry and mesh using gmsh API
  int ElemOrder = 4;
  circlesConcentric(nDom, R, rPML, xs, ys, lc, ElemOrder);

  // Define domain
  Subdomain omega(nDom);
  Subdomain gammaInf(nDom);
  Subdomain gammaPml(nDom);
  Subdomain source(nDom);
  Interface sigma(nDom);

  for(unsigned int i = 0; i < nDom; ++i) {
    if(i == (nDom - 1)) {
      gammaPml(i) = Domain(1, 93);
      gammaInf(i) = Domain(1, 92);
      omega(i) = (Domain(2, i + 100) | Domain(2, 100 + i + 1));
    }
    else if(i == 0) {
      omega(i) = Domain(2, i + 100) | Domain(2, 9000);
    }
    else {
      omega(i) = Domain(2, i + 100);
    }

    if(i == 0) {
      source(i) = Domain(0, 999);
    }

    if(i != 0) {
      sigma(i, i - 1) = Domain(1, 200 + i - 1);
    }
    if(i != nDom - 1) {
      sigma(i, i + 1) = Domain(1, 200 + i);
    }
  }

  // Define topology
  std::vector< std::vector< unsigned int > > topology(nDom);
  for(unsigned int i = 0; i < nDom; ++i) {
    if(i != 0) {
      topology[i].push_back(i - 1);
    }
    if(i != nDom - 1) {
      topology[i].push_back(i + 1);
    }
  }

  double theta = pi / 4.; // mean flow orientation
  gmshDdm.userDefinedParameter(theta, "theta");
  msg::info << "Mach number " << M << " theta " << theta << msg::endl;
  double Mx = M * std::cos(theta);
  double My = M * std::sin(theta);

  // Parameters for Inverse Lorentz transformation
  double beta = sqrt(1 - M * M); // Jacobian
  double Alphax = 1 + Mx * Mx / (beta * (1 + beta));
  double Alphay = 1 + My * My / (beta * (1 + beta));
  double K = Mx * My / (beta * (1 + beta));

  // Inverse Lorentz transformation
  TensorFunction< std::complex< double > > Linv = tensor< std::complex< double > >(beta * Alphay, -beta * K, 0., -beta * K, beta * Alphax, 0., 0., 0., 0.);

  // Mach-velocity vector
  VectorFunction< std::complex< double > > MM = vector< std::complex< double > >(Mx, My, 0.);
  ScalarFunction< std::complex< double > > Mn = MM * normal< std::complex< double > >();
  ScalarFunction< std::complex< double > > Mt = MM * tangent< std::complex< double > >();
  ScalarFunction< std::complex< double > > beta_n = sqrt(1 - Mn * Mn); // Jacobian

  // Transmission condition
  ScalarFunction< std::complex< double > > Si, Sti, dS;

  msg::info << " - FEM basis order = " << FEMorder << "" << msg::endl;
  msg::info << " - Mesh size = " << lc << msg::endl;

  // Create DDM formulation
  std::vector< FieldInterface< std::complex< double > > * > fieldBucket;
  gmshddm::problem::Formulation< std::complex< double > > formulation("HelmholtzFlow", topology);
  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega | source | gammaPml | sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  // Tell to the formulation that g is field that have to be exchanged between subdomains
  formulation.addInterfaceField(g);

  // Define analytical solution
  Function< std::complex< double >, Degree::Degree0 > *solution = nullptr;
  solution = new AnalyticalFunction< helmholtz2D::MonopoleFreeField< std::complex< double > > >(k, M, theta, 1., xs, ys, 0., 0.);

  // PML parameters
  double Sigma0 = beta;
  ScalarFunction< std::complex< double > > det_J;
  TensorFunction< std::complex< double > > J_PML_inv_T;
  const double Wpml = rPML - R;

  ScalarFunction< std::complex< double > > cosT = x< std::complex< double > >() / r2d< std::complex< double > >();
  ScalarFunction< std::complex< double > > sinT = y< std::complex< double > >() / r2d< std::complex< double > >();
  ScalarFunction< std::complex< double > > dampingProfileR = Sigma0 * heaviside(abs(r2d< std::complex< double > >()) - R) / (Wpml - (r2d< std::complex< double > >() - R));
  ScalarFunction< std::complex< double > > dampingProfileInt = -Sigma0 * heaviside(abs(r2d< std::complex< double > >()) - R) * ln((Wpml - (r2d< std::complex< double > >() - R)) / Wpml);
  ScalarFunction< std::complex< double > > gamma = 1. - im * dampingProfileR / k;
  ScalarFunction< std::complex< double > > gamma_hat = 1. - im * (1. / r2d< std::complex< double > >()) * dampingProfileInt / k;
  det_J = gamma * gamma_hat;
  J_PML_inv_T = tensor< std::complex< double > >(cosT / gamma, sinT / gamma, 0., -sinT / gamma_hat, cosT / gamma_hat, 0., 0., 0., 0.);

  // build vector and matrix for the weak form
  VectorFunction< std::complex< double > > J_PML_inv_T_M = J_PML_inv_T * MM;
  TensorFunction< std::complex< double > > J_PML_Linv = J_PML_inv_T * Linv;
  msg::info << "Use a PML as radiation condition - circular stable formulation" << msg::endl;
  for(unsigned int i = 0; i < nDom; ++i) {
    if (i == (nDom-1)) {
      u(i).addConstraint(gammaPml(i), 0.);
    }
  }
  for(unsigned int i = 0; i < nDom; ++i) {
    // VOLUME TERMS
    if(i != (nDom - 1)) {
      // convected Helmholz weak form
      formulation(i).integral(vector< std::complex< double > >(1 - Mx * Mx, -Mx * My, 0.) * grad(dof(u(i))), vector< std::complex< double > >(1., 0., 0.) * grad(tf(u(i))), omega(i), gauss);
      formulation(i).integral(vector< std::complex< double > >(-Mx * My, 1 - My * My, 0.) * grad(dof(u(i))), vector< std::complex< double > >(0., 1., 0.) * grad(tf(u(i))), omega(i), gauss);
      formulation(i).integral(-im * k * dof(u(i)), vector< std::complex< double > >(Mx, My, 0.) * grad(tf(u(i))), omega(i), gauss);
      formulation(i).integral(vector< std::complex< double > >(im * k * Mx, im * k * My, 0.) * grad(dof(u(i))), tf(u(i)), omega(i), gauss);
      formulation(i).integral(-k * k * dof(u(i)), tf(u(i)), omega(i), gauss);
    }
    else {
      // convected Helmholz weak form with PML
      formulation(i).integral(det_J * J_PML_Linv * grad(dof(u(i))), J_PML_Linv * grad(tf(u(i))), omega(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(+k * k / (beta * beta) * det_J * J_PML_inv_T_M * dof(u(i)), J_PML_inv_T_M * tf(u(i)), omega(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(+im * k / beta * det_J * J_PML_Linv * grad(dof(u(i))), J_PML_inv_T_M * tf(u(i)), omega(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(-im * k / beta * det_J * J_PML_inv_T_M * dof(u(i)), J_PML_Linv * grad(tf(u(i))), omega(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(-k * k / (beta * beta) * det_J * dof(u(i)), tf(u(i)), omega(i), gauss);
    }

    // point source
    formulation(i).integral(formulation.physicalSource(-1.), tf(u(i)), source(i), gauss);

    // Artificial source - Zeroth order transmission condition
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i).integral(formulation.artificialSource(-g(j, i)), tf(u(i)), sigma(i, j), gauss);
    }

    // Interface weak form
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i, j).integral(dof(g(i, j)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(formulation.artificialSource(g(j, i)), tf(g(i, j)), sigma(i, j), gauss);
    }

    if(Transmission == "Taylor0") {
      Si = im * k * exp(im * alpha / 2.); // alpha=0, beta*beta*Si = i*k*(1-Mn)
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(Si * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        formulation(i, j).integral(-2.*Si * u(i), tf(g(i, j)), sigma(i, j), gauss);
      }
    }
    else if(Transmission == "Taylor2") {
      Si = im * k * cos(alpha / 2.);
      Sti = exp(-im * alpha / 2.)*Mt;
      dS = im * (beta * beta) * exp(-im * alpha / 2.) / (2.0 * k);

      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(Si * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        formulation(i).integral(Sti * tangent< std::complex< double > >() * grad(dof(u(i))), dof(u(i)), sigma(i, j), gauss);
        formulation(i).integral(-dS * grad(dof(u(i))), grad(tf(u(i))), sigma(i, j), gauss);

        formulation(i, j).integral(-2.*Si * u(i), tf(g(i, j)), sigma(i, j), gauss);
        formulation(i, j).integral(-2.*Sti * tangent< std::complex< double > >() * grad(u(i)), tf(g(i, j)), sigma(i, j), gauss);

        formulation(i, j).integral(2. * dS * grad(u(i)), grad(tf(g(i, j))), sigma(i, j), gauss);
      }
    }
    else if(Transmission == "Pade") {
      const double Np = 2. * padeOrder + 1.;
      const std::complex< double > exp1 = std::complex< double >(std::cos(alpha), std::sin(alpha));
      const std::complex< double > exp2 = std::complex< double >(std::cos(alpha / 2.), std::sin(alpha / 2.));
      const std::complex< double > coef = 2. / Np;

      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(im * k * exp2 * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        // interface unknowns
        formulation(i, j).integral(-2. * (im * k * exp2) * u(i), tf(g(i, j)), sigma(i, j), gauss);

        // define Pade coefficients
        std::vector< std::complex< double > > c(padeOrder, 0.);
        for(int l = 0; l < padeOrder; ++l) {
          c[l] = std::tan((l + 1) * pi / Np);
          c[l] *= c[l];
        }

        // define the auxiliary fields
        std::vector< Field< std::complex< double >, Form::Form0 > * > phi;
        for(int l = 0; l < padeOrder; ++l) {
          phi.push_back(new Field< std::complex< double >, Form::Form0 >("phi_" + std::to_string(l), sigma(i, j), FunctionSpaceTypeForm0::HierarchicalH1, FEMorder));
          fieldBucket.push_back(phi.back());
        }

        std::complex<double> k_eps = k;// -im*0.4*pow(k,(1/3.))*pow((beta/R),2/3.);
        // write the augmented weak form
        for(int l = 0; l < padeOrder; ++l) {
          // boundary integral terms relating the auxiliary fields
          formulation(i).integral(im * k * exp2 * coef * c[l] * dof(*phi[l]), tf(u(i)), sigma(i, j), gauss);
          formulation(i).integral(im * k * exp2 * coef * c[l] * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
          // coupling of the auxiliary equations
          formulation(i).integral(-(beta * beta) * grad(dof(*phi[l])), grad(tf(*phi[l])), sigma(i, j), gauss);
          formulation(i).integral(-2. * im * k_eps * Mt * tangent< std::complex< double > >() * grad(dof(*phi[l])), tf(*phi[l]), sigma(i, j), gauss);
          formulation(i).integral((k_eps * k_eps) * (exp1 * c[l] + 1.) * dof(*phi[l]), tf(*phi[l]), sigma(i, j), gauss);
          formulation(i).integral((k_eps * k_eps) * exp1 * (c[l] + 1.) * dof(u(i)), tf(*phi[l]), sigma(i, j), gauss);
        }
        // interfaces unknowns
        for(int l = 0; l < padeOrder; ++l) {
          formulation(i, j).integral(-2. * (im * k * exp2) * coef * c[l] * (*phi[l]), tf(g(i, j)), sigma(i, j), gauss);
          formulation(i, j).integral(-2. * (im * k * exp2) * coef * c[l] * u(i), tf(g(i, j)), sigma(i, j), gauss);
        }
      }
    }
  }
  // Solve the DDM formulation
  std::string solver = "gmres";
  gmshDdm.userDefinedParameter(solver, "solver");
  double tol = 1e-6;
  gmshDdm.userDefinedParameter(tol, "tol");
  int maxIt = 400;
  gmshDdm.userDefinedParameter(maxIt, "maxIt");

  formulation.pre();
  formulation.solve(solver, tol, maxIt, true);

  bool save_eigs = false;
  gmshDdm.userDefinedParameter(save_eigs,"save_eigs");
  if (save_eigs) {
    gmshfem::algebra::MatrixCCS<std::complex<double>> mat;
    mat = formulation.computeMatrix();
    msg::info << "nnz = " << mat.numberOfNonZero() << ", symmetric = " << mat.isSymmetric() << ", hermitian = " << mat.isHermitian() << msg::endl;
    mat.save("Mymatrix_"+Transmission);
  }

  double cum_local_err=0; // cumulated L2 error
  Subdomain omega_err(nDom);
  for(unsigned int i = 0; i < nDom; ++i) {
    omega_err(i) = Domain(2, i + 100);
    save(+u(i), omega(i), "u_" + std::to_string(i));
    //save(*solution - u(i), omega_err(i), "err_" + std::to_string(i));
    std::complex< double > denLocal = integrate(pow(abs(*solution), 2), omega_err(i), gauss);
    std::complex< double > numLocal = integrate(pow(abs(*solution - u(i)), 2), omega_err(i), gauss);
    cum_local_err += sqrt(numLocal.real() / denLocal.real());
    msg::info << "Local L2 error = " << sqrt(numLocal.real() / denLocal.real()) << " on subdomain " << i << msg::endl;
  }
  msg::info << "Average L2 error = " << cum_local_err/nDom << msg::endl;


  for(unsigned int i = 0; i < fieldBucket.size(); ++i) {
    delete fieldBucket[i];
  }

  bool ComputeMono = false;
  gmshDdm.userDefinedParameter(ComputeMono, "ComputeMono");
  if (ComputeMono) {
    Domain omegaMono, sourceMono, gammaPmlMono;
    for(unsigned int i = 0; i < nDom; ++i) {
      omegaMono |= omega(i);
      sourceMono = source(0);
      gammaPmlMono = gammaPml(nDom-1);
    }
    gmshfem::field::Field< std::complex< double >, Form::Form0 > uMono("uMono", omegaMono | sourceMono | gammaPmlMono, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
    gmshfem::problem::Formulation< std::complex< double > > monodomain("HelmholtzFlow");
    
    uMono.addConstraint(gammaPmlMono, 0.);
    // convected Helmholz weak form with PML
    monodomain.integral(det_J * J_PML_Linv * grad(dof(uMono)), J_PML_Linv * grad(tf(uMono)), omegaMono, gauss, term::ProductType::Scalar);
    monodomain.integral(+k * k / (beta * beta) * det_J * J_PML_inv_T_M * dof(uMono), J_PML_inv_T_M * tf(uMono), omegaMono, gauss, term::ProductType::Scalar);
    monodomain.integral(+im * k / beta * det_J * J_PML_Linv * grad(dof(uMono)), J_PML_inv_T_M * tf(uMono), omegaMono, gauss, term::ProductType::Scalar);
    monodomain.integral(-im * k / beta * det_J * J_PML_inv_T_M * dof(uMono), J_PML_Linv * grad(tf(uMono)), omegaMono, gauss, term::ProductType::Scalar);
    monodomain.integral(-k * k / (beta * beta) * det_J * dof(uMono), tf(uMono), omegaMono, gauss);
    // point source
    monodomain.integral(-1., tf(uMono), sourceMono, gauss);

    monodomain.pre();
    monodomain.assemble();
    msg::info << "Memory usage " << monodomain.getEstimatedFactorizationMemoryUsage() << msg::endl;
    monodomain.solve();

    // Monodomain L2 error
    double cum_local_errM=0;
    for(unsigned int i = 0; i < nDom; ++i) {
      std::complex< double > denLocalM = integrate(pow(abs( uMono ), 2), omega_err(i), gauss);
      std::complex< double > numLocalM = integrate(pow(abs(uMono - u(i)), 2), omega_err(i), gauss);
      cum_local_errM += sqrt(numLocalM.real() / denLocalM.real());
      gmshfem::msg::info << "ddm-monodomain L2 error = " << sqrt(numLocalM.real() / denLocalM.real()) << " on subdomain " << i << msg::endl;
    }
    msg::info << "Average L2 error mono = " << cum_local_errM/nDom << msg::endl;

  } // end Monodomain


  return 0;
}
