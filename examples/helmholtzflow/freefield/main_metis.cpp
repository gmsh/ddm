#include "gmsh.h"
#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshfem/AnalyticalFunction.h>
#include <gmshfem/Message.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::analytics;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;

void meshDisk(double xs, double ys, const int ElemOrder, const double R, const double Rpml, const double lc)
{
  gmsh::model::add("geometry");
  gmsh::option::setNumber("Mesh.CharacteristicLengthMax", lc);

  gmsh::model::geo::addPoint(0., 0., 0., lc, 1); // Center
  gmsh::model::geo::addPoint(xs, ys, 0., lc / 5., 100); // Source point
  // physical circle
  gmsh::model::geo::addPoint(R, 0., 0., lc, 2);
  gmsh::model::geo::addPoint(0, R, 0., lc, 3);
  gmsh::model::geo::addPoint(-R, 0., 0., lc, 4);
  gmsh::model::geo::addPoint(0, -R, 0., lc, 5);

  gmsh::model::geo::addCircleArc(2, 1, 3, 1);
  gmsh::model::geo::addCircleArc(3, 1, 4, 2);
  gmsh::model::geo::addCircleArc(4, 1, 5, 3);
  gmsh::model::geo::addCircleArc(5, 1, 2, 4);
  gmsh::model::geo::addCurveLoop({1, 2, 3, 4}, 1);
  gmsh::model::geo::addPlaneSurface({1}, 1);

  // PML
  gmsh::model::geo::addPoint(Rpml, 0., 0., lc, 10);
  gmsh::model::geo::addPoint(0, Rpml, 0., lc, 11);
  gmsh::model::geo::addPoint(-Rpml, 0., 0., lc, 12);
  gmsh::model::geo::addPoint(0, -Rpml, 0., lc, 13);

  gmsh::model::geo::addCircleArc(10, 1, 11, 9);
  gmsh::model::geo::addCircleArc(11, 1, 12, 10);
  gmsh::model::geo::addCircleArc(12, 1, 13, 11);
  gmsh::model::geo::addCircleArc(13, 1, 10, 12);
  gmsh::model::geo::addCurveLoop({9, 10, 11, 12}, 3);
  gmsh::model::geo::addPlaneSurface({3, 1}, 3);

  // source point 
  gmsh::model::geo::synchronize();
  gmsh::model::mesh::embed(0, {100}, 2, 1);

  // physicals
  gmsh::model::addPhysicalGroup(2, {1}, 1);
  gmsh::model::setPhysicalName(2, 1, "omega");
  gmsh::model::addPhysicalGroup(1, {1, 2, 3, 4}, 3);
  gmsh::model::setPhysicalName(1, 3, "gammaPhy");
  gmsh::model::addPhysicalGroup(1, {9, 10, 11, 12}, 4);
  gmsh::model::setPhysicalName(1, 4, "gammaPml");
  gmsh::model::addPhysicalGroup(0, {100}, 100);
  gmsh::model::setPhysicalName(0, 100, "source");
  gmsh::model::addPhysicalGroup(2, {3}, 3);
  gmsh::model::setPhysicalName(2, 3, "omega_pml");

  // generate mesh
  //gmsh::model::geo::synchronize();
  //gmsh::model::mesh::generate(2);
  //gmsh::model::mesh::setOrder(ElemOrder);
  //gmsh::write("m_circle.msh");
}


int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  const double pi = 3.14159265358979323846264338327950288;
  std::complex< double > im(0., 1.);
  
  // physical parameters
  double k = 6 * pi; // free field wavenumber
  gmshDdm.userDefinedParameter(k, "k");
  double M = 0.8; // Mach number
  gmshDdm.userDefinedParameter(M, "M");
  double theta = pi / 4.; // mean flow orientation
  gmshDdm.userDefinedParameter(theta, "theta");
  msg::info << " - Mach number M= " << M << " theta = " << theta << msg::endl;
  msg::info << " - Input freefield wavenumber k0 = " << k << msg::endl;
  // souce point location
  double xs = 0., ys = 0.;
  gmshDdm.userDefinedParameter(xs, "xs");
  gmshDdm.userDefinedParameter(ys, "ys");

  // DDM parameters
  unsigned int nDom = 16;
  gmshDdm.userDefinedParameter(nDom, "nDom");
  std::string Transmission = "Taylor2";
  gmshDdm.userDefinedParameter(Transmission, "Transmission");
  double alpha = -pi / 2.;
  gmshDdm.userDefinedParameter(alpha, "alpha");

  // FEM parameters
  int FEMorder;
  if (M >= 0.9) {
    FEMorder = 9;
  }
  else if (M < 0.9 && M >= 0.7) {
    FEMorder = 6;
  }
  else if (M < 0.7 && M >= 0.4) {
    FEMorder = 4;
  }
  else {
    FEMorder = 2;
  }
  std::string gauss = "Gauss" + std::to_string(2 * FEMorder + 1);

  // meshing/geometry parameters
  std::string mesh = "";
  // geometry parameters
  double R = 1.;
  gmshDdm.userDefinedParameter(R, "R");
  double pointsByWl = 8;
  gmshDdm.userDefinedParameter(pointsByWl, "pointsByWl");
  int Npml = 4;
  int ElemOrder = 4;
  gmshDdm.userDefinedParameter(Npml, "Npml");
  double lc = (2 * pi * FEMorder * (1 - abs(M))) / (pointsByWl * k);
  double rPML = R + Npml * lc;

  msg::info << " - pointsByWl (shortest wavelength) =  " << pointsByWl << msg::endl;
  msg::info << " - FEM basis order = " << FEMorder << msg::endl;
  msg::info << " - Mesh size = " << lc << msg::endl;
  
  gmsh::option::setNumber("Mesh.Binary", 1);
  if(mesh.empty()) {
    meshDisk(xs, ys, ElemOrder, R, rPML, lc);
    gmsh::option::setNumber("Mesh.ElementOrder", ElemOrder);
    gmsh::option::setNumber("Mesh.SecondOrderLinear", 1);
    // save all, as partitioning will create some entities without physical groups
    gmsh::option::setNumber("Mesh.SaveAll", 1);
    gmsh::model::mesh::generate(2);
    gmsh::model::mesh::partition(nDom);
    // save partitioned mesh in single file for mono-process runs
    gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 0);
    gmsh::write("disk.msh");
    // save partitioned mesh in separate files for distributed runs
    gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 1);
    // gmsh::write("disk.msh");
    mesh = "disk";
  }
  
  gmsh::model::add(mesh);
  gmsh::merge(mesh + ".msh");
  if(gmsh::model::getNumberOfPartitions() != static_cast<int>(nDom) )
    msg::error << "Wrong number of partitions in mesh file " << gmsh::model::getNumberOfPartitions() << " vs. " << nDom << msg::endl;

  Domain omegaPhy = gmshfem::domain::Domain("omega");
  Domain omegaPml = gmshfem::domain::Domain("omega_pml");
  Domain gammaPml = gmshfem::domain::Domain("gammaPml");
  Domain Source = gmshfem::domain::Domain("source");
  Subdomain omega_phy = Subdomain::buildSubdomainOf(omegaPhy);
  Subdomain omega_pml = Subdomain::buildSubdomainOf(omegaPml);
  Subdomain source = Subdomain::buildSubdomainOf(Source);
  Subdomain gamma_pml = Subdomain::buildSubdomainOf(gammaPml);

  std::vector< std::vector< unsigned int > > topology;
  Interface sigma = Interface::buildInterface(topology);
  
  double Mx = M * std::cos(theta);
  double My = M * std::sin(theta);

  // Parameters for Inverse Lorentz transformation
  double beta = sqrt(1 - M * M); // Jacobian
  double Alphax = 1 + Mx * Mx / (beta * (1 + beta));
  double Alphay = 1 + My * My / (beta * (1 + beta));
  double K = Mx * My / (beta * (1 + beta));

  // Inverse Lorentz transformation
  TensorFunction< std::complex< double > > Linv = tensor< std::complex< double > >(beta * Alphay, -beta * K, 0., -beta * K, beta * Alphax, 0., 0., 0., 0.);
  // Mach-velocity vector
  VectorFunction< std::complex< double > > MM = vector< std::complex< double > >(Mx, My, 0.);
  ScalarFunction< std::complex< double > > Mn = MM * normal< std::complex< double > >();
  ScalarFunction< std::complex< double > > Mt = MM * tangent< std::complex< double > >();

  // Define analytical solution
  Function< std::complex< double >, Degree::Degree0 > *solution = nullptr;
  solution = new AnalyticalFunction< helmholtz2D::MonopoleFreeField< std::complex< double > > >(k, M, theta, 1., xs, ys, 0., 0.);
  // PML parameters
  double Sigma0 = beta;
  ScalarFunction< std::complex< double > > detJ;
  TensorFunction< std::complex< double > > J_PML_invT;
  const double Wpml = rPML - R;

  ScalarFunction< std::complex< double > > cosT = x< std::complex< double > >() / r2d< std::complex< double > >();
  ScalarFunction< std::complex< double > > sinT = y< std::complex< double > >() / r2d< std::complex< double > >();
  ScalarFunction< std::complex< double > > dampingProfileR = Sigma0 * heaviside(abs(r2d< std::complex< double > >()) - R) / (Wpml - (r2d< std::complex< double > >() - R));
  ScalarFunction< std::complex< double > > dampingProfileInt = -Sigma0 * heaviside(abs(r2d< std::complex< double > >()) - R) * ln((Wpml - (r2d< std::complex< double > >() - R)) / Wpml);
  ScalarFunction< std::complex< double > > gamma = 1. - im * dampingProfileR / k;
  ScalarFunction< std::complex< double > > gamma_hat = 1. - im * (1. / r2d< std::complex< double > >()) * dampingProfileInt / k;
  detJ = gamma * gamma_hat;
  J_PML_invT = tensor< std::complex< double > >(cosT / gamma, sinT / gamma, 0., -sinT / gamma_hat, cosT / gamma_hat, 0., 0., 0., 0.);

  // build vector and matrix for the weak form
  VectorFunction< std::complex< double > > J_Pml_invT_M = J_PML_invT * MM;
  TensorFunction< std::complex< double > > J_Pml_Linv = J_PML_invT * Linv;
  msg::info << "Use a PML as radiation condition - circular stable formulation" << msg::endl;

  // ddm problem
  std::vector< FieldInterface< std::complex< double > > * > fieldBucket;
  gmshddm::problem::Formulation< std::complex< double > > formulation("HelmholtzFlow", topology);
  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega_phy | omega_pml | source | gamma_pml , FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  formulation.addInterfaceField(g);
  for(unsigned int i = 0; i < nDom; ++i) {
      u(i).addConstraint(gamma_pml(i), 0.);
  }
  
  // define transmission conditions 
  ScalarFunction< std::complex< double > > Si=0., Sti=0., dS=0.;
  if ( Transmission == "Taylor0" ) {
    //if (!getMPIRank())
      msg::info << " - Use Sommerfeld transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k*exp(im*alpha/2.); // if alpha=0, we get beta*beta*Si = i*k*(1-Mn)
  }
  else if ( Transmission == "Taylor2" ) {
    //if (!getMPIRank())
      msg::info << " - Use second order transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k*cos(alpha/2.);
    Sti = exp(-im*alpha/2.) * Mt;
    dS = im*(beta*beta)*exp(-im*alpha/2.) / (2.0*k);
  }

  // ddm variational formulation
  TensorFunction< std::complex< double > > TFlow = tensor< std::complex< double > > (1-Mx*Mx, -Mx*My, 0., -Mx*My, 1-My*My, 0., 0., 0., 0.);
  for(unsigned int i = 0; i < nDom; ++i) {
    // convected Helmholz weak form with PML - Lorentz formulation
    formulation(i).integral(+ detJ * J_Pml_Linv*grad(dof(u(i))) , J_Pml_Linv*grad(tf(u(i))) , omega_pml(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(+ k*k/(beta*beta) * detJ * J_Pml_invT_M * dof(u(i)) , J_Pml_invT_M * tf(u(i)), omega_pml(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(+ im*k/beta* detJ * J_Pml_Linv * grad(dof(u(i))), J_Pml_invT_M * tf(u(i)), omega_pml(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(- im*k/beta* detJ * J_Pml_invT_M * dof(u(i)), J_Pml_Linv * grad(tf(u(i))), omega_pml(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(- k*k/(beta*beta) * detJ * dof(u(i)), tf(u(i)), omega_pml(i), gauss);
    
    // convected Helmholz weak form
    formulation(i).integral( TFlow * grad(dof(u(i))), grad(tf(u(i))), omega_phy(i), gauss);
    formulation(i).integral(- im * k * dof(u(i)), MM * grad(tf(u(i))), omega_phy(i), gauss);
    formulation(i).integral(+ im * k * MM * grad(dof(u(i))), tf(u(i)), omega_phy(i), gauss);
    formulation(i).integral(- k * k * dof(u(i)), tf(u(i)), omega_phy(i), gauss);

    // point source
    formulation(i).integral(formulation.physicalSource(-1.), tf(u(i)), source(i), gauss);
    
    // Artificial source
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i).integral(formulation.artificialSource(-g(j, i)), tf(u(i)), sigma(i, j), gauss);
    }
    // Interface weak form
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i, j).integral(dof(g(i, j)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(formulation.artificialSource(g(j, i)), tf(g(i, j)), sigma(i, j), gauss);
    }

    // Apply transmission condition
    if(Transmission == "Taylor0") {
      Si = im * k * exp(im * alpha / 2.); // alpha=0, beta*beta*Si = i*k*(1-Mn)
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(Si * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        formulation(i, j).integral(-2.* Si * u(i), tf(g(i, j)), sigma(i, j), gauss);
      }
    }
    else if(Transmission == "Taylor2") {
      Si = im * k * cos(alpha / 2.);
      Sti = exp(-im * alpha / 2.) * Mt;
      dS = im * (beta * beta) * exp(-im * alpha / 2.) / (2.0 * k);
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(Si * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        formulation(i).integral(Sti * tangent< std::complex< double > >() * grad(dof(u(i))), dof(u(i)), sigma(i, j), gauss);
        formulation(i).integral(-dS * grad(dof(u(i))), grad(tf(u(i))), sigma(i, j), gauss);

        formulation(i, j).integral(-2.* Si * u(i), tf(g(i, j)), sigma(i, j), gauss);
        formulation(i, j).integral(-2.* Sti * tangent< std::complex< double > >() * grad(u(i)), tf(g(i, j)), sigma(i, j), gauss);
        formulation(i, j).integral(2. * dS * grad(u(i)), grad(tf(g(i, j))), sigma(i, j), gauss);
      }
    }
    else if(Transmission == "Pade") {
      int padeOrder = 4;
      gmshDdm.userDefinedParameter(padeOrder, "padeOrder");

      const double Np = 2. * padeOrder + 1.;
      const std::complex< double > exp1 = std::complex< double >(std::cos(alpha), std::sin(alpha));
      const std::complex< double > exp2 = std::complex< double >(std::cos(alpha / 2.), std::sin(alpha / 2.));
      const std::complex< double > coef = 2. / Np;

      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(im * k * exp2 * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
         // interface unknowns
        formulation(i, j).integral(-2. * (im * k * exp2) * u(i), tf(g(i, j)), sigma(i, j), gauss);

        // define Pade coefficients
        std::vector< std::complex< double > > c(padeOrder, 0.);
        for(int l = 0; l < padeOrder; ++l) {
          c[l] = std::tan((l + 1) * pi / Np);
          c[l] *= c[l];
        }

        // define the auxiliary fields
        std::vector< Field< std::complex< double >, Form::Form0 > * > phi;
        for(int l = 0; l < padeOrder; ++l) {
          phi.push_back(new Field< std::complex< double >, Form::Form0 >("phi_" + std::to_string(l), sigma(i, j), FunctionSpaceTypeForm0::HierarchicalH1, FEMorder));
          fieldBucket.push_back(phi.back());
        }

        std::complex<double> k_eps = k; // - im*0.4*pow(k,(1/3.))*pow((beta/R),2/3.);
        // write the augmented weak form
        for(int l = 0; l < padeOrder; ++l) {
          // boundary integral terms relating the auxiliary fields
          formulation(i).integral(im * k * exp2 * coef * c[l] * dof(*phi[l]), tf(u(i)), sigma(i, j), gauss);
          formulation(i).integral(im * k * exp2 * coef * c[l] * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
          // coupling of the auxiliary equations
          formulation(i).integral(-(beta * beta) * grad(dof(*phi[l])), grad(tf(*phi[l])), sigma(i, j), gauss);
          formulation(i).integral(-2. * im * k_eps * Mt * tangent< std::complex< double > >() * grad(dof(*phi[l])), tf(*phi[l]), sigma(i, j), gauss);
          formulation(i).integral((k_eps * k_eps) * (exp1 * c[l] + 1.) * dof(*phi[l]), tf(*phi[l]), sigma(i, j), gauss);
          formulation(i).integral((k_eps * k_eps) * exp1 * (c[l] + 1.) * dof(u(i)), tf(*phi[l]), sigma(i, j), gauss);
        }
        // interfaces unknowns
        for(int l = 0; l < padeOrder; ++l) {
          formulation(i, j).integral(-2. * (im * k * exp2) * coef * c[l] * (*phi[l]), tf(g(i, j)), sigma(i, j), gauss);
          formulation(i, j).integral(-2. * (im * k * exp2) * coef * c[l] * u(i), tf(g(i, j)), sigma(i, j), gauss);
        }
      }
    }
  }

  // solve DDM problem
  std::string solver = "gmres";
  gmshDdm.userDefinedParameter(solver, "solver");
  double tol = 1e-6;
  gmshDdm.userDefinedParameter(tol, "tol");
  int maxIt = 200;
  gmshDdm.userDefinedParameter(maxIt, "maxIt");

  formulation.pre();
  formulation.solve(solver, tol, maxIt, true);

  // save solution and compute L2 error
  double cum_local_err=0; // cumulated L2 error
  for(unsigned int i = 0; i < nDom; ++i) {
    //save(+u(i), omega_phy(i) | omega_pml(i), "u_" + std::to_string(i));
    std::complex< double > denLocal = integrate(pow(abs(*solution), 2), omega_phy(i), gauss);
    std::complex< double > numLocal = integrate(pow(abs(*solution - u(i)), 2), omega_phy(i), gauss);
    cum_local_err += sqrt(numLocal.real() / denLocal.real());
    msg::info << "Local L2 error = " << sqrt(numLocal.real() / denLocal.real()) << " on subdomain " << i << msg::endl;
  }
  msg::info << "Average L2 error = " << cum_local_err/nDom << msg::endl;

  for(unsigned int i = 0; i < fieldBucket.size(); ++i) {
    delete fieldBucket[i];
  }

  bool ComputeMono = false;
  gmshDdm.userDefinedParameter(ComputeMono, "ComputeMono");
  if (ComputeMono) {
    gmshfem::field::Field< std::complex< double >, Form::Form0 > uMono("uMono", omegaPml | omegaPhy | Source | gammaPml , FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
    gmshfem::problem::Formulation< std::complex< double > > monodomain("HelmholtzFlow");
    uMono.addConstraint(gammaPml, 0.);

    // convected Helmholz weak form with PML - Lorentz formulation
    monodomain.integral(+ detJ * J_Pml_Linv*grad(dof(uMono)) , J_Pml_Linv*grad(tf(uMono)) , omegaPml, gauss, term::ProductType::Scalar);
    monodomain.integral(+ k*k/(beta*beta) * detJ * J_Pml_invT_M * dof(uMono) , J_Pml_invT_M * tf(uMono), omegaPml, gauss, term::ProductType::Scalar);
    monodomain.integral(+ im*k/beta* detJ * J_Pml_Linv * grad(dof(uMono)), J_Pml_invT_M * tf(uMono), omegaPml, gauss, term::ProductType::Scalar);
    monodomain.integral(- im*k/beta* detJ * J_Pml_invT_M * dof(uMono), J_Pml_Linv * grad(tf(uMono)), omegaPml, gauss, term::ProductType::Scalar);
    monodomain.integral(- k*k/(beta*beta) * detJ * dof(uMono), tf(uMono), omegaPml, gauss);
    
    // convected Helmholz weak form
    monodomain.integral( TFlow * grad(dof(uMono)), grad(tf(uMono)), omegaPhy, gauss);
    monodomain.integral(- im * k * dof(uMono), MM * grad(tf(uMono)), omegaPhy, gauss);
    monodomain.integral(+ im * k * MM * grad(dof(uMono)), tf(uMono), omegaPhy, gauss);
    monodomain.integral(- k * k * dof(uMono), tf(uMono), omegaPhy, gauss);
    
    // point source
    monodomain.integral(-1., tf(uMono), Source, gauss);

    monodomain.pre();
    monodomain.assemble();
    msg::info << "Memory usage " << monodomain.getEstimatedFactorizationMemoryUsage() << msg::endl;
    monodomain.solve();

    // Monodomain L2 error
    double local_err;
    double global_err=0.;
    for(unsigned int i = 0; i < nDom; ++i) {
      std::complex< double > denLocal = integrate(pow(abs( uMono ), 2), omega_phy(i) | omega_pml(i), gauss);
      std::complex< double > numLocal = integrate(pow(abs(uMono - u(i)), 2), omega_phy(i) | omega_pml(i), gauss);
      local_err = sqrt(numLocal.real() / denLocal.real());
      //gmshfem::msg::info << "ddm-monodomain L2 error = " << local_err << " on subdomain " << i << msg::endl;
      global_err += local_err;
    }
    gmshfem::msg::info << "ddm-monodomain global error = " << global_err/nDom  << msg::endl;

  } // end Monodomain

  return 0;
}