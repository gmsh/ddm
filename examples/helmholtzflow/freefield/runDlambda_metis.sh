cd build
cmake .. && make

pi=3.14159265358979323846264338327950288
for pt in 4 6 8 10 12 14 16 18 20
do
  echo "********* pointsByWl = $pt *******************"
  echo "********* k=2pi *********"
  ./example -M 0.8 -pointsByWl $pt -k 2*$pi > test.txt # 6.28318530718
  grep "Iteration" test.txt | wc -l >> Nit1.txt
  echo "********* k=6pi *********"
  ./example -M 0.8 -pointsByWl $pt -k 6*$pi > test.txt # 18.8495559215
  grep "Iteration" test.txt | wc -l >> Nit2.txt
  echo "********* k=10pi *********"
  ./example -M 0.8 -pointsByWl $pt -k 10*$pi > test.txt #31.4159265359
  grep "Iteration" test.txt | wc -l >> Nit3.txt
  echo $pt >> Pts.txt
done 

paste Pts.txt Nit1.txt Nit2.txt Nit3.txt >> Results_Dlambda.txt
rm Pts.txt Nit1.txt Nit2.txt Nit3.txt