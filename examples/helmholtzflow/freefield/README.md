# Domain decomposition in a 2D circular domain for convected propagation

Dependency(ies):
* GmshDDM : v 1.0.0
* GmshFEM : v 1.0.0
* Gmsh

## Problem description

The routines solves convected Helmholtz problem in a disk domain, where the input is a point source.
The domain uses a perfectly matched layer as outgoing boundary condition.
The routine `main.cpp` uses a circle concentric decomposition, while the routine `main_metis.cpp` calls the automatic partitioner metis for the partitioning.
Different transmission conditions are tested, and are compared to the analytical and mono-domain solution, that is the solution obtained without domain decomposition.

## Installation and usage
Simply run

```
  mkdir build && cd build
  cmake ..
  make
  ./example [PARAM]
```
with `[PARAM]`:
* `-nDom [x]` where `[x]` is the number of subdomains
* `-R [x]` where `[x]` is the disk radius or square size, $`R > 0`$
* `-k [x]` is the freefield wavenumber, $`k > 0`$
* `-M [x]` is the mean flow Mach number, $`0 < M < 1`$
* `-theta [x]` is the mean flow angle orientation, $`\theta \in [0, 2\pi]`$
* `-xs [x]` and `-ys [x]` specify the point source location
* `pointsByWl [x]` is the number of dofs for the shortest wavelength
* `-Transmission [x]` is the choice of transmission condition, The available choices are `Taylor0`, `Taylor2` and `Pade`.
* `-padeOrder [x]` specifies the number of auxiliary function and branch rotation angle for the `Pade` transmission condition.
* `-alpha [x]` is the branch rotation angle for the transmission conditions, $`\alpha \in [0, -\pi]`$. $`\alpha = -\pi/2`$ is the default.
* `-solver [x]` selects the type of iterative solver. `gmres` is the default.
* `-tol [x]` specifies the tolerence of the iterative solver, `1e-6` is used by default.
* `-maxIt [x]` is the maximum number of iterations allowed, 400 iterations are allowed by default.
* `-ComputeMono [x]` is a boolean that allows to compute the mono-domain solution.

By default a circular domain with a Padé condition is used with 5 circle-concentric subdomains, for a centered source around the origin at the frequency $k=6\pi$, with a mean flow at $M=0.8$ oriented at $\theta=\pi/4$.


## Reference
> P. Marchner, H. Bériot, S. Le Bras, X. Antoine and C. Geuzaine. A domain decomposition solver for large scale time-harmonic flow acoustics problems. (2023), Preprint [HAL-04254633](https://hal.science/hal-04254633).

## Results reproducibility
The results from Figure 3.3, Section 3.3 can be reproduced thanks to
* running `runTests.sh` for the residual history,
* running  `runMscal.sh` for the number of iterations a function of the Mach number.

Figure 3.3 (a) can be visualized by opening the solutions `u_i.msh` for each subdomain, after running the command
```
./example -nDom 5 -Transmission Pade -padeOrder 8 -M 0.95 -maxIt 4 -ComputeMono true
```

The results from Figure 4.1, Section 4 can be reproduced using `runDlambda_metis.sh` and `runMscal_metis.sh`, with the file `main_metis.cpp` set in the `CMakeLists.txt`.