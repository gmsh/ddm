cd build
cmake .. && make

for M in 0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.85 0.9 0.95
do
  echo "------------ M = $M ------------"
  echo "************ Taylor0 ************"
  ./example -nDom 5 -Transmission Taylor0 -M $M -alpha 0 > test.txt
  grep "Iteration" test.txt | wc -l >> Niter1.txt
  echo "************ Taylor2 ************"
  ./example -nDom 5 -Transmission Taylor2 -M $M -alpha -0.78539816339 > test.txt
  grep "Iteration" test.txt | wc -l >> Niter2.txt
  echo "************ Pade8 - alpha=-pi/4 ************"
  ./example -nDom 5 -Transmission Pade -padeOrder 8 -M $M -alpha -0.78539816339 > test.txt
  grep "Iteration" test.txt | wc -l >> Niter3.txt
  echo "************ Pade8 ************"
  ./example -nDom 5 -Transmission Pade -padeOrder 8 -M $M > test.txt
  grep "Iteration" test.txt | wc -l >> Niter4.txt
  echo $M >> Ms.txt
done 

paste Ms.txt Niter1.txt Niter2.txt Niter3.txt Niter4.txt >> It_MscalConcentric.txt
rm Ms.txt Niter1.txt Niter2.txt Niter3.txt Niter4.txt