#include <iostream>
#include <fstream>
#include <gmshddm/GmshDdm.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>
#include <gmshfem/Message.h>
#include <gmsh.h>
#include "nacelleFunctions.h"
#include "resources.h"

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  const double pi = 3.14159265358979323846264338327950288;
  const std::complex< double > im(0., 1.);

  std::string mesh = "";
  gmshDdm.userDefinedParameter(mesh, "mesh");

  bool meshOnly = false;
  gmshDdm.userDefinedParameter(meshOnly, "meshOnly");

  int nDom = 6;
  gmshDdm.userDefinedParameter(nDom, "nDom");

  double h_near = 0.07; // mesh size near field
  gmshDdm.userDefinedParameter(h_near, "h_near");

  double h_far = 0.1; // mesh size far field
  gmshDdm.userDefinedParameter(h_far, "h_far");

  double meshSizeFactor = 1.0; // mesh size factor
  gmshDdm.userDefinedParameter(meshSizeFactor, "meshSizeFactor");
  gmsh::option::setNumber("Mesh.MeshSizeFactor", meshSizeFactor);

  double R = 2.5; // cylinder radius
  gmshDdm.userDefinedParameter(R, "R");

  double D = 3.; // duct length
  gmshDdm.userDefinedParameter(D, "D");

  double L_duct = -0.4; // fan face position

  double bpf_ratio = 0.5; // running frequency with respect to the bpf (blade passing frequency)
  gmshDdm.userDefinedParameter(bpf_ratio, "bpf_ratio");

  // Input BC
  int m = 6; // azimuthal mode number
  gmshDdm.userDefinedParameter(m, "m");
  int n = 0; // radial mode number
  gmshDdm.userDefinedParameter(n, "n");

  bool Active_PML = true;
  int Npml = 2; // number of PML layers (classic and active) - no PML if set to 0
  // set acoustic treatment - single dof perforated plate liner
  bool SetLiner = false;
  gmshDdm.userDefinedParameter(SetLiner, "SetLiner");

  int FEMorder = 2;
  gmshDdm.userDefinedParameter(FEMorder, "FEMorder");
  std::string gauss = "Gauss" + std::to_string(2 * FEMorder + 2);

  std::string Transmission = "Taylor2";
  double alpha = -pi / 2.;

  bool saveFlow = false; // save mean flow Mach number
  gmshDdm.userDefinedParameter(saveFlow, "saveFlow");
  std::string Flowcase = "Sideline"; // map external mean flow on the nodes of the acoustic mesh
  gmshDdm.userDefinedParameter(Flowcase, "Flowcase");
  // load mean flow data
  double M_fanFace, bpf;
  if (Flowcase == "Sideline") {
    gmsh::open("../rho0_sideline.pos");
    gmsh::open("../c0_sideline.pos");
    gmsh::open("../Vx_sideline.pos");
    gmsh::open("../Vy_sideline.pos");
    M_fanFace = -0.55;
    bpf = bpf_ratio*1300.;
  }
  else if (Flowcase == "Approach") {
    gmsh::open("../rho0_appraoch.pos");
    gmsh::open("../c0_appraoch.pos");
    gmsh::open("../Vx_appraoch.pos");
    gmsh::open("../Vy_appraoch.pos");
    M_fanFace = -0.22;
    bpf = bpf_ratio*700.;
  }
  else {
    msg::error << "Did not find mean flow data" << msg::endl;
    exit(0);
  }

  gmsh::option::setNumber("Mesh.Binary", 1);
  if(mesh.empty()) {
    if(!getMPIRank()) {
      // build geometry
      if (Active_PML) {
        Nacelle(h_near, h_far, D, R, L_duct, Npml*h_far*meshSizeFactor,  Npml*h_near*meshSizeFactor, nDom);
      }
      else {
        Nacelle(h_near, h_far, D, R, L_duct, Npml*h_far*meshSizeFactor, 0, nDom);
      }
      gmsh::option::setNumber("Mesh.ElementOrder", FEMorder);
      gmsh::option::setNumber("Mesh.SecondOrderLinear", 1);
      // save all, as partitioning will create some entities without physical groups
      gmsh::option::setNumber("General.NumThreads", omp::getMaxThreads());
      gmsh::option::setNumber("Mesh.SaveAll", 1);
      gmsh::model::mesh::generate(3);
      gmsh::model::mesh::partition(nDom);
      // save partitioned mesh in single file for mono-process runs
      gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 0);
      gmsh::write("Nacelle.msh");
      // save partitioned mesh in separate files for distributed runs
      gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 1);
      gmsh::write("Nacelle.msh");
    }
    mesh = "Nacelle";
  }

  barrier();
  if(meshOnly) return 0;
  gmsh::model::add(mesh);
  // read partitioned mesh
  if(getMPISize() == 1) {
    gmsh::merge(mesh + ".msh");
  }
  else {
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
      if (mpi::isItMySubdomain(i)) {
        gmsh::merge(mesh + "_" + std::to_string(i + 1) + ".msh");
      }
    }
  }
  if(gmsh::model::getNumberOfPartitions() != nDom)
    msg::error << "Wrong number of partitions in mesh file " << gmsh::model::getNumberOfPartitions() << " vs. " << nDom << msg::endl;

  Domain omegaPhy = gmshfem::domain::Domain("omega");
  Domain omegaPml, omegaAPml;
  if (Active_PML) {
    omegaAPml = gmshfem::domain::Domain("omega_apml");
  }
  Domain fanFaceMono = gmshfem::domain::Domain("fanFace");
  Domain linerMono = gmshfem::domain::Domain("liner");
  Domain gammaExtMono;
  if (Npml!=0) {
    omegaPml = gmshfem::domain::Domain("omega_pml");
  }
  else {
    gammaExtMono = gmshfem::domain::Domain("gamma_phy");
  }


  Subdomain omega_phy = Subdomain::buildSubdomainOf(omegaPhy);
  Subdomain omega_pml(nDom);
  Subdomain omega_apml(nDom);
  if (Active_PML) {
    omega_apml = Subdomain::buildSubdomainOf(omegaAPml);
  }
  Subdomain fanFace = Subdomain::buildSubdomainOf(fanFaceMono);
  Subdomain liner = Subdomain::buildSubdomainOf(linerMono);
  Subdomain gammaExt(nDom);
  if (Npml!=0) {
    omega_pml = Subdomain::buildSubdomainOf(omegaPml);
  }
  else {
    gammaExt = Subdomain::buildSubdomainOf(gammaExtMono);
  }

  std::vector< std::vector< unsigned int > > topology;
  Interface sigma = Interface::buildInterface(topology);

  // mean flow domain
  Domain omegaFlow;
  for(int i = 0; i < nDom; ++i) {
    if (mpi::isItMySubdomain(i)) {
      if (Active_PML && (Npml!=0) ) {
        omegaFlow |= omega_phy(i) | omega_pml(i) | omega_apml(i);
      }
      else if (Npml!=0) {
        omegaFlow |= omega_phy(i) | omega_pml(i);
      }
      else if (Active_PML) {
        omegaFlow |= omega_phy(i) | omega_apml(i);
      }
      else {
        omegaFlow |= omega_phy(i);
      }
    }
  }

  // interpolate mean flow
  Field< double, Form::Form0 > rho0d("rho0d", omegaFlow, FunctionSpaceTypeForm0::HierarchicalH1, 1);
  Field< double, Form::Form0 > c0("c0", omegaFlow, FunctionSpaceTypeForm0::HierarchicalH1, 1);
  Field< double, Form::Form0 > Vx("Vx", omegaFlow, FunctionSpaceTypeForm0::HierarchicalH1, 1);
  Field< double, Form::Form0 > Vy("Vy", omegaFlow, FunctionSpaceTypeForm0::HierarchicalH1, 1);
  Field< double, Form::Form0 > Vz("Vz", omegaFlow, FunctionSpaceTypeForm0::HierarchicalH1, 1);
  InterpolateMeanFlow(nDom, omegaFlow, rho0d, c0, Vx, Vy, Vz);

  ScalarFunction<double> Mx = Vx/c0;
  ScalarFunction<double> My = Vy/c0;
  ScalarFunction<double> Mz = Vz/c0;

  // check mean flow data
  if (saveFlow) {
    for(int i = 0; i < nDom; ++i) {
      if (mpi::isItMySubdomain(i)) {
        save(c0, omegaFlow, "c0_"+ std::to_string(i + 1), "pos");
        save(Vx, omegaFlow, "Vx_"+ std::to_string(i + 1), "pos");
        save(Vy, omegaFlow, "Vy_"+ std::to_string(i + 1), "pos");
        save(sqrt(Mx*Mx+My*My+Mz*Mz), omegaFlow, "Mach_"+ std::to_string(i + 1), "pos");
      }
    }
  }

  // reference mean flow values at infinity
  double c_inf = 340;
  double rho_inf = 1.2;
  double v_inf = -85;
  double M_inf = v_inf/c_inf;
  // mean flow values on fan face
  double v_fanFace = (-M_fanFace)*c_inf*sqrt( (2+M_inf*M_inf*0.4 ) / (2+M_fanFace*M_fanFace*0.4 ) );
  double c0_fanFace = sqrt( c_inf*c_inf* ( 1 + 0.2 * (v_inf*v_inf - v_fanFace*v_fanFace) / (c_inf*c_inf) ) );
  double rho0_fanFace = rho_inf * pow((1 + 0.2 * (v_inf * v_inf - v_fanFace * v_fanFace) / (c_inf * c_inf)), 1./0.4);
  double beta_fanFace = sqrt(1-M_fanFace*M_fanFace);
  if (!getMPIRank()) {
    msg::info << "v face = " << v_fanFace << msg::endl;
    msg::info << "Exterior Mach number = " << abs(M_inf) << msg::endl;
    msg::info << "Fan face speed of sound = " << c0_fanFace << msg::endl;
    msg::info << "Exterior speed of sound = " << c_inf << msg::endl;
    msg::info << "Fan face density = " << rho0_fanFace << msg::endl;
    msg::info << "Exterior denisty = " << rho_inf << msg::endl;
  }
  // Mach field complex valued functions for acoustic formulation
  VectorFunction< std::complex<double> > MM = complex( vector< double > (Mx,My,Mz) );
  VectorFunction< std::complex<double> > VV = complex( vector< double > (Vx,Vy,Vz) );
  ScalarFunction< std::complex<double> > beta = complex( sqrt(1 - pow(norm( vector< double > (Mx,My,Mz) ),2) ) );
  ScalarFunction< std::complex<double> > Mn = complex( vector< double > (Mx,My,Mz)*normal< double >() );
  ScalarFunction< std::complex<double> > Mt = complex( vector< double > (Mx,My,Mz)*tangent< double >() );

  // ***************************************************************************************
  // Physical parameters
  double k0_inf = 2*pi*bpf/c_inf;
  double k0_fanFace = 2*pi*bpf/c0_fanFace;
  ScalarFunction< std::complex<double> > k0 = complex( 2*pi*bpf/c0 );
  ScalarFunction< std::complex<double> > rho0 = complex(rho0d);
  // normalized impedance (by local value of rho0*c0) for Myers BC
  double Res = 2, If = 0.020, hh = 0.02286; // resistance,inertance and liner depth
  ScalarFunction< std::complex<double> > Z = Res - im*( k0*If + 1./tan(k0*hh) ); // normalized impedance (by rho0*c0)

  float pointsByWlFar = c_inf*(1-abs(M_inf))*FEMorder / (bpf*h_far*meshSizeFactor);
  float pointsByWlNear = c0_fanFace*(1-abs(M_fanFace))*FEMorder / (bpf*h_near*meshSizeFactor);
  if (!getMPIRank()) {
    msg::info << " - dof density by wavelength (far) = " << pointsByWlFar << "" << msg::endl;
    msg::info << " - dof density by wavelength (near) = " << pointsByWlNear << "" << msg::endl;
    if(pointsByWlFar < 6 || pointsByWlNear < 6) {
      msg::warning << " - less than 6 dofs per wavelength !" << msg::endl;
    }
    // msg::info << " - dof density nacelle tip (Sideline) = " << 321.0*0.17*FEMorder / (bpf*(0.5*h_near)*meshSizeFactor) << "" << msg::endl;
  }

  // Annular duct mode
  std::complex<double> kx;
  double krmn;
  ScalarFunction< std::complex<double> > psi, dy_psi, dz_psi;
  getInputMode(m, n, k0_fanFace, M_fanFace, kx, krmn, psi, dy_psi, dz_psi, 3);
  VectorFunction< std::complex<double> > grad_psi = vector< std::complex<double> > (-im*kx*psi, dy_psi, dz_psi);
  if (!getMPIRank())
    msg::info << " - Axial wavenumber kx = " << kx << ", radial wavenumber krmn = " << krmn << " , k0inf = " << k0_inf << msg::endl;

  TensorFunction< std::complex< double > > J_PML_Linv, J_PML_LinvA;
  VectorFunction< std::complex< double > > J_PML_inv_T_M, J_PML_inv_T_MA;
  ScalarFunction< std::complex< double > > detJpml, detJpmlA;
  GetCylindricalPMLFlow(L_duct,  Npml*h_far*meshSizeFactor,  Npml*h_near*meshSizeFactor, k0_inf, k0_fanFace, M_inf, M_fanFace, R, D, vector< double > (Mx,My,Mz), J_PML_Linv, J_PML_LinvA, J_PML_inv_T_M, J_PML_inv_T_MA, detJpml, detJpmlA);

  gmshddm::problem::Formulation< std::complex< double > > formulation("HelmholtzFlow", topology);
  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega_pml | omega_phy | omega_apml | sigma | liner | fanFace | gammaExt, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  formulation.addInterfaceField(g);

  ScalarFunction< std::complex< double > > Si=0., Sj=0., Sti=0., Stj=0., dS=0.;
  if ( Transmission == "Taylor0" ) {
    if (!getMPIRank())
      msg::info << " - Use Sommerfeld transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k0*(exp(im*alpha/2.)-Mn); // if alpha=0, we get beta*beta*Si = i*k*(1-Mn)
    Sj = im*k0*(exp(im*alpha/2.)+Mn);
    Sti = -Mn*Mt;
    Stj = +Mn*Mt;
    dS = 0.;
  }
  else if ( Transmission == "Taylor2" ) {
    if (!getMPIRank())
      msg::info << " - Use second order transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k0*(cos(alpha/2.)-Mn) ; // im*k0*cos(alpha/2.)
    Sj = im*k0*(cos(alpha/2.)+Mn) ;
    Sti = ( exp(-im*alpha/2.) - Mn ) * Mt ; // exp(-im*alpha/2.) * Mt
    Stj = ( exp(-im*alpha/2.) + Mn ) * Mt ;
    dS = im*(beta*beta)*exp(-im*alpha/2.) / (2.0*k0);
  }

  TensorFunction< double > TFlow = tensor< double > (1-Mx*Mx, -Mx*My, -Mx*Mz, -Mx*My, 1-My*My, -My*Mz, -Mx*Mz, -My*Mz, 1-Mz*Mz);
  for(int i = 0; i < nDom; ++i) {
    if (mpi::isItMySubdomain(i)) {
      if (Npml != 0) {
        // convected Helmholz weak form with PML - Lorentz formulation
        formulation(i).integral(+rho0 *  detJpml * J_PML_Linv*grad(dof(u(i))) , J_PML_Linv*grad(tf(u(i))) , omega_pml(i), gauss, term::ProductType::Scalar);
        formulation(i).integral(+rho0 * k0*k0/(beta*beta) * detJpml * J_PML_inv_T_M * dof(u(i)) , J_PML_inv_T_M * tf(u(i)), omega_pml(i), gauss, term::ProductType::Scalar);
        formulation(i).integral(+rho0 * im*k0/beta* detJpml * J_PML_Linv * grad(dof(u(i))), J_PML_inv_T_M * tf(u(i)), omega_pml(i), gauss, term::ProductType::Scalar);
        formulation(i).integral(-rho0 * im*k0/beta* detJpml * J_PML_inv_T_M * dof(u(i)), J_PML_Linv * grad(tf(u(i))), omega_pml(i), gauss, term::ProductType::Scalar);
        formulation(i).integral(-rho0 * k0*k0/(beta*beta) * detJpml * dof(u(i)), tf(u(i)), omega_pml(i), gauss);
      }
      else {
        formulation(i).integral(rho0 * im * k0 * dof(u(i)), tf(u(i)), gammaExt(i), gauss);
      }
      // convected Helmholz weak form
      formulation(i).integral( rho0 * complex(TFlow) * grad(dof(u(i))), grad(tf(u(i))), omega_phy(i), gauss);
      formulation(i).integral(-rho0 * im * k0* dof(u(i)), MM * grad(tf(u(i))), omega_phy(i), gauss);
      formulation(i).integral(+rho0 * im * k0 * MM * grad(dof(u(i))), tf(u(i)), omega_phy(i), gauss);
      formulation(i).integral(-rho0 * k0 * k0 * dof(u(i)), tf(u(i)), omega_phy(i), gauss);

      if (Active_PML) {
        // Active PML - Lorentz formulation - use constant mean flow values at the fan Face
        formulation(i).integral(+rho0_fanFace * detJpmlA * J_PML_LinvA*grad(dof(u(i))) , J_PML_LinvA*grad(tf(u(i))) , omega_apml(i), gauss, term::ProductType::Scalar);
        formulation(i).integral(+rho0_fanFace * k0_fanFace*k0_fanFace/(beta_fanFace*beta_fanFace) * detJpmlA * J_PML_inv_T_MA * dof(u(i)) , J_PML_inv_T_MA * tf(u(i)), omega_apml(i), gauss, term::ProductType::Scalar);
        formulation(i).integral(+rho0_fanFace * im*k0_fanFace/beta_fanFace* detJpmlA * J_PML_LinvA * grad(dof(u(i))), J_PML_inv_T_MA * tf(u(i)), omega_apml(i), gauss, term::ProductType::Scalar);
        formulation(i).integral(-rho0_fanFace * im*k0_fanFace/beta_fanFace* detJpmlA * J_PML_inv_T_MA * dof(u(i)), J_PML_LinvA * grad(tf(u(i))), omega_apml(i), gauss, term::ProductType::Scalar);
        formulation(i).integral(-rho0_fanFace * k0_fanFace*k0_fanFace/(beta_fanFace*beta_fanFace) * detJpmlA * dof(u(i)), tf(u(i)), omega_apml(i), gauss);
        // Active PML - incident field
        formulation.physicalSourceTerm(formulation(i).integral(-rho0_fanFace * detJpmlA * J_PML_LinvA * grad_psi, J_PML_LinvA*grad(tf(u(i))) , omega_apml(i), gauss, term::ProductType::Scalar));
        formulation.physicalSourceTerm(formulation(i).integral(-rho0_fanFace * k0_fanFace*k0_fanFace/(beta_fanFace*beta_fanFace) * detJpmlA * J_PML_inv_T_MA * psi, J_PML_inv_T_MA * tf(u(i)), omega_apml(i), gauss, term::ProductType::Scalar));
        formulation.physicalSourceTerm(formulation(i).integral(-rho0_fanFace * im*k0_fanFace/beta_fanFace* detJpmlA * J_PML_LinvA * grad_psi, J_PML_inv_T_MA * tf(u(i)), omega_apml(i), gauss, term::ProductType::Scalar));
        formulation.physicalSourceTerm(formulation(i).integral(+rho0_fanFace * im*k0_fanFace/beta_fanFace* detJpmlA * J_PML_inv_T_MA * psi, J_PML_LinvA * grad(tf(u(i))), omega_apml(i), gauss, term::ProductType::Scalar));
        formulation.physicalSourceTerm(formulation(i).integral(+rho0_fanFace * k0_fanFace*k0_fanFace/(beta_fanFace*beta_fanFace) * detJpmlA * psi, tf(u(i)), omega_apml(i), gauss));
        // Flux term
        formulation.physicalSourceTerm(formulation(i).integral(-rho0_fanFace * im * kx * (beta_fanFace*beta_fanFace) * psi, tf(u(i)), fanFace(i), gauss));
        formulation.physicalSourceTerm(formulation(i).integral(-rho0_fanFace * im * k0_fanFace * M_fanFace * psi, tf(u(i)), fanFace(i), gauss));
      }
      else {
        formulation.physicalSourceTerm(formulation(i).integral(-rho0_fanFace * im * kx * (beta_fanFace*beta_fanFace) * psi, tf(u(i)), fanFace(i), gauss));
        formulation(i).integral(-rho0_fanFace * im * k0_fanFace * M_fanFace * dof(u(i)), tf(u(i)), fanFace(i), gauss);
        //formulation(i).integral(rho0 * Mn * Mt * tangent< std::complex< double > >() * grad(dof(u(i))), tf(u(i)), fanFace(i), gauss);
      }

      if (SetLiner) {
        // Myer's BC on Liner
        formulation(i).integral( im*rho0*k0 / Z * dof(u(i)), tf(u(i)), liner(i), gauss);
        formulation(i).integral( -rho0 / Z * dof(u(i)), MM * grad(tf(u(i))), liner(i), gauss);
        formulation(i).integral( +rho0 / Z * MM * grad(dof(u(i))), tf(u(i)), liner(i), gauss);
        formulation(i).integral( im*rho0 / (Z*k0) * MM * grad(dof(u(i))),  MM * grad(tf(u(i))),liner(i), gauss);
      }

      // Artificial source
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        if(!sigma(i,j).isEmpty()) {
          formulation.artificialSourceTerm(formulation(i).integral(-g(j,i), tf(u(i)), sigma(i,j), gauss));
          // Contribution from the boundary terms
          formulation(i).integral(rho0 * (im*k0*Mn) * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
          formulation(i).integral(rho0 * Mn * Mt * tangent< std::complex< double > >() * grad( dof(u(i)) ), dof(u(i)), sigma(i,j), gauss);
          // Transmission conditions terms
          formulation(i).integral(rho0 * Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
          formulation(i).integral(rho0 * Sti * tangent< std::complex< double > >() * grad( dof(u(i)) ), dof(u(i)), sigma(i,j), gauss);
          formulation(i).integral(-rho0 * dS * grad( dof(u(i)) ), grad( tf(u(i)) ), sigma(i,j), gauss);
        }
      }
      // Interface terms
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        if(!sigma(i,j).isEmpty()) {
          formulation(i,j).integral(dof(g(i,j)), tf(g(i,j)), sigma(i,j), gauss);
          formulation.artificialSourceTerm(formulation(i,j).integral(g(j,i), tf(g(i,j)), sigma(i,j), gauss));
          // Transmission conditions terms
          formulation(i,j).integral(-rho0 * (Si+Sj) * u(i) , tf(g(i,j)), sigma(i,j), gauss);
          formulation(i,j).integral(-rho0 * (Sti+Stj) * tangent< std::complex< double > >() * grad( u(i) ) , tf(g(i,j)), sigma(i,j), gauss);
          formulation(i,j).integral( 2. *rho0 * dS * grad( u(i) ), grad( tf(g(i,j)) ), sigma(i,j), gauss);
        }
      }
    }
  }

  // Solve DDM
  s_printResources("After definition of formulations", true);
  // solve DDM problem
  std::string solver = "gmres";
  gmshDdm.userDefinedParameter(solver, "solver");
  double tol = 1e-6;
  gmshDdm.userDefinedParameter(tol, "tol");
  int maxIt = 5000;
  gmshDdm.userDefinedParameter(maxIt, "maxIt");

  formulation.pre();
  formulation.solve(solver, tol, maxIt, true);

  // save solutions
  gmsh::option::setNumber("Mesh.Binary", 1);
  gmsh::option::setNumber("PostProcessing.Binary", 1);
  gmsh::option::setNumber("PostProcessing.SaveMesh", 0);
  for(int i = 0; i < nDom; ++i) {
    if(gmshddm::mpi::isItMySubdomain(i)) {
      int tag = save(u(i), omega_phy(i) | omega_pml(i) | omega_apml(i), "u", "", "", true, 0, 0., i + 1);
      gmsh::view::write(tag, "u_" + std::to_string(i + 1) + ".msh");

      // also save some cuts
      for(int step = 0; step < 2; step++) {
        gmsh::view::option::setNumber(tag, "TimeStep", step);
        gmsh::view::option::setNumber(tag, "AdaptVisualizationGrid", 1);
        gmsh::plugin::setNumber("CutPlane", "View", gmsh::view::getIndex(tag));
        gmsh::plugin::setNumber("CutPlane", "RecurLevel", FEMorder);
        gmsh::plugin::setNumber("CutPlane", "TargetError", -1); // 1e-4 to get decent AMR
        { // z = 0
          gmsh::plugin::setNumber("CutPlane", "A", 0);
          gmsh::plugin::setNumber("CutPlane", "B", 0);
          gmsh::plugin::setNumber("CutPlane", "C", 1);
          gmsh::plugin::setNumber("CutPlane", "D", 0);
          int tag2 = gmsh::plugin::run("CutPlane");
          std::string name2 = std::string("u_cut_z0_step") + std::to_string(step) + "_" + std::to_string(i + 1);
          gmsh::view::option::setString(tag2, "Name", name2);
          gmsh::view::write(tag2, name2 + ".pos");
        }
        { // y = 0
          gmsh::plugin::setNumber("CutPlane", "A", 0);
          gmsh::plugin::setNumber("CutPlane", "B", 1);
          gmsh::plugin::setNumber("CutPlane", "C", 0);
          gmsh::plugin::setNumber("CutPlane", "D", 0);
          int tag2 = gmsh::plugin::run("CutPlane");
          std::string name2 = std::string("u_cut_y0_step") + std::to_string(step) + "_" + std::to_string(i + 1);
          gmsh::view::option::setString(tag2, "Name", name2);
          gmsh::view::write(tag2, name2 + ".pos");
        }
        { // x = -0.39, 1.176, 2
          std::vector<double> x = {-0.39};//, 1.176, 2};
          for(auto d : x) {
            gmsh::plugin::setNumber("CutPlane", "A", 1);
            gmsh::plugin::setNumber("CutPlane", "B", 0);
            gmsh::plugin::setNumber("CutPlane", "C", 0);
            gmsh::plugin::setNumber("CutPlane", "D", -d);
            int tag2 = gmsh::plugin::run("CutPlane");
            std::string name2 = std::string("u_cut_x") + std::to_string(d) + "_step" + std::to_string(step) + "_" + std::to_string(i + 1);
            gmsh::view::option::setString(tag2, "Name", name2);
            gmsh::view::write(tag2, name2 + ".pos");
          }
        }
      }
    }
  }

  ScalarFunction< std::complex<double> > pressure; // compute acoustic pressure
  for(int i = 0; i < nDom; ++i) {
    if(gmshddm::mpi::isItMySubdomain(i)) {
      pressure = -rho0*( im*2.*pi*bpf*u(i) + VV*grad(u(i)) );
      int tagP = save(pressure, omega_phy(i) | omega_pml(i) | omega_apml(i), "p", "", "", true, 0, 0., i + 1);
      gmsh::view::write(tagP, "p_" + std::to_string(i + 1) + ".msh");
    }
  }

  barrier();

  return 0;
}
