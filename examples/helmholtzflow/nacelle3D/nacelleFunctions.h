#include <iostream>
#include <fstream>
#include <gmshddm/GmshDdm.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>
#include <gmshfem/Message.h>
#include <gmsh.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;

void Nacelle(const double h_near=0.4, const double h_far=0.4, const double D=4., const double R=3., const double L_duct=-0.4, const double L_pml=0.6, const double L_pml_a=0.4, const int nDom=6);

void getInputMode(int m, int n, double w, double M, std::complex<double> &kzmn, double &krmn, ScalarFunction< std::complex<double> > &psi,
  ScalarFunction< std::complex<double> > &dy_psi, ScalarFunction< std::complex<double> > &dz_psi, int dim, double Rout = 1.2, double Rin = 0.3586206896556000);

void GetCylindricalPMLFlow(double L_duct, double Lpml, double Lpml_a, double k0_inf, double k0f, double M_inf, double Mf, double R, double D,
  VectorFunction< double > MM, TensorFunction< std::complex< double > > &J_PML_Linv, TensorFunction< std::complex< double > > &J_PML_LinvA,
  VectorFunction< std::complex< double > > &J_PML_inv_T_M, VectorFunction< std::complex< double > > &J_PML_inv_T_MA,
  ScalarFunction< std::complex< double > > &detJpml, ScalarFunction< std::complex< double > > &detJpmlA);

void InterpolateMeanFlow(int nDom, Domain omegaFlow, Field< double, Form::Form0 > &rho0, Field< double, Form::Form0 > &c0,
  Field< double, Form::Form0 > &Vx, Field< double, Form::Form0 > &Vy, Field< double, Form::Form0 > &Vz);
