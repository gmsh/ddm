# Flow acoustics problem - radiation from a turbofan engine intake

Dependency(ies):
* GmshDDM : v 1.0.0
* GmshFEM : v 1.0.0
* Gmsh

Specific requirements:
* Gmsh must be compiled with OpenCascade
* GmshFEM must be compiled with the Complex Bessel library
* MPI must be enabled
* Mean flow data

## Problem description

The routine solves a three-dimensional convected Helmholtz wave propgation problem, which represents the acoustic radiation from a generic turbofan engine intake. The wave propagates through a non-uniform mean flow with a given mean density $`\rho_0(\mathbf{x})`$, speed of sound $`c_0(\mathbf{x})`$ and velocity vector field $`\boldsymbol{v}_0(\boldsymbol{x})`$. The mean flow is assumed to be subsonic so that the condition $`\| \boldsymbol{v}_0(\boldsymbol{x}) \|/c_0(\mathbf{x}) < 1`$ holds. A version without mean flow is available in the folder `example/helmholtz/nacelle3D`.

### Mean flow
The mean flow is computed externally, and given by a data file (e.g .pos or CGNS) which is read by Gmsh as a pre-processing step. 
The mean flow is here provided by four .pos files, that have been generated with GmshFEM from an axisymmetric computation.
Different mean flow configurations are available (Approach, Sideline, etc.), which represent a specific flight configuration such as landing or take-off.
 
### Boundary value problem
The specificities of the boundary value problem are:

* an annular acoustic mode of azimuthal and radial order $`(m,n)`$ is imposed at the fan face. It models the tonal noise generated from the blades of the rotating fan at a fixed frequency, called the Blade Passing Frequency (BPF). We consider a fan of 24 blades, such that the relevant computations include all propagative modes of the form $`(24 \times p,0)`$ for the frequency $`BPF \times p`$, where $`p`$ is a non-zero integer.
For the Sideline mean flow configuration, the first BPF is 1300 Hz and can be associated to the mode (24,0). The second BPF is at 2600 Hz and can be associated to the modes (24,0) or (48,0), which are both propagative modes.
The modes are set through the parameters `-m [x]` and `-n [x]`, and the frequency is set relatively to the BPF through `-bpf_ratio [x]`.

* an acoustic treatment, called the acoustic lining or liner, may be imposed on the nacelle. It is described by an acoustic impedance in the frequency domain $`Z(\omega)`$, and models a single degree-of-freedom (SDOF) perforated plate. It is set by the boolean parameter `-SetLiner [x]`.

* a radiation condition is required to truncate the computational domain. Here we use a PML written in cylindrical coordinates $`(x, r, \theta)`$. We also use a PML in the negative x direction to impose the input mode. It is called an active PML, and allows to absorb back-scattered modes.

* the other walls are perfectly reflecting, that is we use a homogeneous Neumann boundary condition.

### Running domain decomposition
1. For a parallel domain decomposition run, a partitioned mesh with `-nDom [x]` subdomains must first be generated with Gmsh and METIS. The running command is
```
  ./example -nDom [x] -FEMorder 4 -meshOnly -meshSizeFactor [x] -maxThreads [x]
```
The parameter `-meshSizeFactor` can be used to adjust the global mesh refinement (1 is set by default), and `-maxThreads` controls the number of threads.

2. To run the solver at the first BPF,
```
  mpirun -np [x] ./example -mesh Nacelle -nDom [x] -m 24 -FEMorder 4 -bpf_ratio 1 -SetLiner true -meshSizeFactor [x] -memoryDDM -maxThreads [x]
```
DDM transmission conditions are set up by default to be second order Taylor conditions with the rotation branch-cut set to $`\alpha=-\pi/2`$.
By default the `bpf_ratio` is 0.5 and the mode number `m` is 6.

## References
> P. Marchner, H. Bériot, S. Le Bras, X. Antoine and C. Geuzaine. A domain decomposition solver for large scale time-harmonic flow acoustics problems. (2023), Preprint [HAL-04254633](https://hal.science/hal-04254633).

> Marchner, Philippe. "Non-reflecting boundary conditions and domain decomposition methods for industrial flow acoustics." PhD thesis, Universités de Lorraine et Liège, 2022.  

## Results reproducibility
The data from Figure 4.3 for the intake problem can be reproduced by running the domain decomposition problem with the corresponding number of subdomains.
Sufficient computational resources are needed to run the cases. 
