# Simple domain decomposition in a straight waveguide for convected propagation

Dependency(ies):
* GmshDDM : v 1.0.0
* GmshFEM : v 1.0.0
* Gmsh

## Problem description

The routine `main_multi.cpp` solves a convected Helmholtz propagation problem in a straight waveguide, where the input source is a superposition of modes.
The propagation occurs in the `x`-direction, while a homogeneous Neumann or Dirichlet condition is imposed on the upper and lower walls.
Different transmission conditions are tested, and compared to the analytical solution and mono-domain solution, that is the solution obtained without domain decomposition.
A PML is set as output boundary condition.
The routine `main.cpp` solves the propagation problem for a single mode, and uses the exact DtN as outgoing condition.

## Installation and usage
Simply run

```
  mkdir build && cd build
  cmake ..
  make
  ./example [PARAM]
```
with `[PARAM]`:
* `-nDom [x]` where `[x]` is the number of subdomains
* `-wallType [x]`, where `[x]` can be `Neumann` or `Dirichlet`
* `-FEMorder [x]` is the polynomial order of the finite element basis functions
* `-k [x]` is the freefield wavenumber, $`k > 0`$
* `-Mx [x]` is the x-component of the vector Mach number, $`-1 < M_x < 1`$.
* `-Transmission [x]` is the choice of transmission condition, The available choices are `Taylor0`, `Taylor2` and `Pade`. `Pade` is the default.
* `-alpha [x]` is the branch rotation angle for the transmission condition, $\alpha \in [0, -\pi]$. $`\alpha=-\pi/4`$ is the default.
* `-padeOrder [x]` specify the number of auxiliary function for the `Pade` condition.
* `-solver [x]` selects the type of iterative solver. `gmres` is the default, `jacobi` can also be used.
* `-tol [x]` specifies the tolerence of the iterative solver, `1e-6` is used by default.
* `-maxIt [x]` is the maximum number of iterations allowed, 200 iterations are allowed by default.

By default 4 subdomains are used for the frequency $`k=30`$, with a mean flow at $`M_x=0.7`$. The finite element order is 4 and the mesh size is chosen in order to have 12 dofs per wavelength.

## Reference
> P. Marchner, H. Bériot, S. Le Bras, X. Antoine and C. Geuzaine. A domain decomposition solver for large scale time-harmonic flow acoustics problems. (2023), Preprint [HAL-04254633](https://hal.science/hal-04254633).

## Results reproducibility
The results from Table 3.1, Section 3.3 can be reproduced thanks to the running commands

```
./example -nDom [x] -Transmission Taylor0 -Mx [x] -alpha 0 -solver [x]
./example -nDom [x] -Transmission Taylor2 -Mx [x] -solver [x]
./example -nDom [x] -Transmission Pade -Mx [x] -solver [x]
```

where the parameters are `-nDom`, `-Mx` and `-solver` need to be selected according to the required data.