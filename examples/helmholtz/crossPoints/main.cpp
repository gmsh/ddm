#include "monoDomain.h"
#include "ddm2D.h"
#include "ddm3D.h"

#include <gmshddm/GmshDdm.h>

#include <gmshfem/Message.h>

int main(int argc, char **argv)
{
  gmshddm::common::GmshDdm gmshDdm(argc, argv);
  
  std::string problem = "monoDomain2D"; // monoDomain, ddm
  gmshDdm.userDefinedParameter(problem, "problem");
  
  if(problem == "monoDomain2D") {
    D2::monoDomain();
  }
  else if(problem == "monoDomain3D") {
    D3::monoDomain();
  }
  else if(problem == "ddm2D") {
    D2::ddm();
  }
  else if(problem == "ddm3D") {
    D3::ddm();
  }
  else {
    gmshfem::msg::error << "Unknown 'problem = " << problem << "'" << gmshfem::msg::endl;
  }
  
  return 0;
}
