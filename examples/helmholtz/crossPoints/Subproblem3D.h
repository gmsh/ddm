#ifndef H_SUBPROBLEM3D
#define H_SUBPROBLEM3D

#include "SubproblemDomains.h"
#include "SubproblemParameters.h"

#include <gmshfem/Formulation.h>

namespace D3 {


  class Boundary
  {
   protected:
    const unsigned int _boundary;
    
   public:
    Boundary(const unsigned int boundary);
    virtual ~Boundary();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };
  
  class Edge
  {
   protected:
    const unsigned int _edge;
    Boundary *_bnd[2];
    
   public:
    Edge(const unsigned int edge, Boundary *first, Boundary *second);
    virtual ~Edge();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };

  class Corner
  {
   protected:
    const unsigned int _corner;
    Edge *_bnd[3];
    
   public:
    Corner(const unsigned int corner, Edge *first, Edge *second, Edge *third);
    virtual ~Corner();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };

  class Subproblem
  {
   private:
    const SubproblemDomains _domains;
    const SubproblemParameters _parameters;
    gmshfem::problem::Formulation< std::complex< double > > &_formulation;
    const std::string _fieldName;
    std::vector< Boundary * > _boundary;
    std::vector< Edge * > _edge;
    std::vector< Corner * > _corner;
    
    void _parseParameters(std::string &method, std::vector< std::string > &parameters, const std::string &str) const;
    
   public:
    Subproblem(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters, const std::string &E, const std::string &N, const std::string &W, const std::string &S, const std::string &D, const std::string &U);
    ~Subproblem();
    
    void writeFormulation();
    
    Boundary *getBoundary(const unsigned int b) const;
    Edge *getEdge(const unsigned int e) const;
    Corner *getCorner(const unsigned int c) const;
  };




  class Sommerfeld : public Boundary
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _v;
    
   public:
    Sommerfeld(const unsigned int boundary);
    ~Sommerfeld();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class HABC : public Boundary
  {
   protected:
    const unsigned int _N;
    const double _theta;
    std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > _uHABC;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _v;
    
   public:
    HABC(const unsigned int boundary, const unsigned int N, const double theta);
    ~HABC();
    
    unsigned int getN() const;
    double getTheta() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUHABC(const unsigned int m) const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class Pml : public Boundary
  {
   protected:
    const double _size;
    const std::string _type;
    
   public:
    Pml(const unsigned int boundary, const double pmlSize, const std::string type);
    virtual ~Pml();
    
    double getSize() const;
    gmshfem::function::ScalarFunction< std::complex< double > >  pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double > > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E, gmshfem::domain::Domain &bnd, const gmshfem::function::ScalarFunction< std::complex< double > > &kappa) const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
  };
  
  class PmlContinuous : public Pml
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPml;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _v;
    
   public:
    PmlContinuous(const unsigned int boundary, const double pmlSize, const std::string type);
    ~PmlContinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  // Marche pas, il faudrait du vrai H(div)
  class PmlDiscontinuous : public Pml
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPml;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 > * _v;
    
   public:
    PmlDiscontinuous(const unsigned int boundary, const double pmlSize, const std::string type);
    ~PmlDiscontinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlWithoutMultiplier : public Pml
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPml;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _v;
    
   public:
    PmlWithoutMultiplier(const unsigned int boundary, const double pmlSize, const std::string type);
    ~PmlWithoutMultiplier();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };




  class HABC_HABC : public Edge
  {
   protected:
    std::vector< std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > > _uHABC_E;
    std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > _vE[2];
    
   public:
    HABC_HABC(const unsigned int corner, HABC *first, HABC *second);
    ~HABC_HABC();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *getUHABC(const unsigned int m, const unsigned int n) const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *getV(const unsigned int m, const unsigned int i) const;
    
    HABC *firstBoundary() const;
    HABC *secondBoundary() const;
      
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuous_PmlContinuous : public Edge
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPmlEdge;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vC[2];
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vEdge;
    
   public:
    PmlContinuous_PmlContinuous(const unsigned int edge, PmlContinuous *first, PmlContinuous *second);
    ~PmlContinuous_PmlContinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV(const unsigned int i) const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getVEdge() const;
    
    PmlContinuous *firstBoundary() const;
    PmlContinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  
  
  
  class HABC_HABC_HABC : public Corner
  {
   protected:
    std::vector< std::vector< std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > > > _uHABC_C;
    std::vector< std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > > _vC[3];
    
   public:
    HABC_HABC_HABC(const unsigned int corner, HABC_HABC *first, HABC_HABC *second, HABC_HABC *third);
    ~HABC_HABC_HABC();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *getUHABC(const unsigned int m, const unsigned int n, const unsigned int o) const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *getV(const unsigned int m, const unsigned int n, const unsigned int i) const;
    
    HABC_HABC *firstEdge() const;
    HABC_HABC *secondEdge() const;
    HABC_HABC *thirdEdge() const;
      
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuous_PmlContinuous_PmlContinuous : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPmlCorner;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vC[3];
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vEdge[3];
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vCorner;
    
   public:
    PmlContinuous_PmlContinuous_PmlContinuous(const unsigned int corner, PmlContinuous_PmlContinuous *first, PmlContinuous_PmlContinuous *second, PmlContinuous_PmlContinuous *third);
    ~PmlContinuous_PmlContinuous_PmlContinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV(const unsigned int i) const;
    
    PmlContinuous_PmlContinuous *firstEdge() const;
    PmlContinuous_PmlContinuous *secondEdge() const;
    PmlContinuous_PmlContinuous *thirdEdge() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuous_PmlContinuous_PmlContinuous_simplify : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vC[3];
    
   public:
    PmlContinuous_PmlContinuous_PmlContinuous_simplify(const unsigned int corner, PmlContinuous_PmlContinuous *first, PmlContinuous_PmlContinuous *second, PmlContinuous_PmlContinuous *third);
    ~PmlContinuous_PmlContinuous_PmlContinuous_simplify();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV(const unsigned int i) const;
    
    PmlContinuous_PmlContinuous *firstEdge() const;
    PmlContinuous_PmlContinuous *secondEdge() const;
    PmlContinuous_PmlContinuous *thirdEdge() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

}

#endif // H_SUBPROBLEM3D
