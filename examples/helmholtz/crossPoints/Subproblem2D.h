#ifndef H_SUBPROBLEM2D
#define H_SUBPROBLEM2D

#include "SubproblemDomains.h"
#include "SubproblemParameters.h"

#include <gmshfem/Formulation.h>

namespace D2 {


  class Boundary
  {
   protected:
    const unsigned int _boundary;
    
   public:
    Boundary(const unsigned int boundary);
    virtual ~Boundary();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };

  class Corner
  {
   protected:
    const unsigned int _corner;
    Boundary *_bnd[2];
    
   public:
    Corner(const unsigned int corner, Boundary *first, Boundary *second);
    virtual ~Corner();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };

  class Subproblem
  {
   private:
    const SubproblemDomains _domains;
    const SubproblemParameters _parameters;
    gmshfem::problem::Formulation< std::complex< double > > &_formulation;
    const std::string _fieldName;
    std::vector< Boundary * > _boundary;
    std::vector< Corner * > _corner;
    
    void _parseParameters(std::string &method, std::vector< std::string > &parameters, const std::string &str) const;
    
   public:
    Subproblem(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters, const std::string &E, const std::string &N, const std::string &W, const std::string &S, const std::vector< unsigned int > activatedCrossPoints);
    ~Subproblem();
    
    void writeFormulation();
    
    Boundary *getBoundary(const unsigned int b) const;
    Corner *getCorner(const unsigned int c) const;
  };




  class Sommerfeld : public Boundary
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _v;
    
   public:
    Sommerfeld(const unsigned int boundary);
    ~Sommerfeld();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class HABC : public Boundary
  {
   protected:
    const unsigned int _N;
    const double _theta;
    std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > _uHABC;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _v;
    
   public:
    HABC(const unsigned int boundary, const unsigned int N, const double theta);
    ~HABC();
    
    unsigned int getN() const;
    double getTheta() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUHABC(const unsigned int m) const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class Pml : public Boundary
  {
   protected:
    const double _size;
    const std::string _type;
    
   public:
    Pml(const unsigned int boundary, const double pmlSize, const std::string type);
    virtual ~Pml();
    
    virtual double getSize() const;
    virtual gmshfem::function::ScalarFunction< std::complex< double > >  pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double > > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E, gmshfem::domain::Domain &bnd, const gmshfem::function::ScalarFunction< std::complex< double > > &kappa) const;
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
  };

  class PmlContinuous : public Pml // with Lagrange multiplier
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPml;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _v;
    
   public:
    PmlContinuous(const unsigned int boundary, const double pmlSize, const std::string type);
    ~PmlContinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class PmlDiscontinuous : public Pml // with Lagrange multiplier
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPml;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 > * _v;
    
   public:
    PmlDiscontinuous(const unsigned int boundary, const double pmlSize, const std::string type);
    ~PmlDiscontinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlWithoutMultiplier : public Pml // without Lagrange multiplier
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPml;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _v;
    
   public:
    PmlWithoutMultiplier(const unsigned int boundary, const double pmlSize, const std::string type);
    ~PmlWithoutMultiplier();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };




  class HABC_HABC : public Corner
  {
   protected:
    std::vector< std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > > _uHABC_C;
    std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > _vC[2];
    
   public:
    HABC_HABC(const unsigned int corner, HABC *first, HABC *second);
    ~HABC_HABC();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *getV(const unsigned int m, const unsigned int i) const;
    
    HABC *firstBoundary() const;
    HABC *secondBoundary() const;
      
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class PmlContinuous_PmlContinuous : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPmlCorner;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vC[2];
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vCorner;
    
   public:
    PmlContinuous_PmlContinuous(const unsigned int corner, PmlContinuous *first, PmlContinuous *second);
    ~PmlContinuous_PmlContinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV(const unsigned int i) const;
    
    PmlContinuous *firstBoundary() const;
    PmlContinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class PmlDiscontinuous_PmlDiscontinuous : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPmlCorner;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 > * _vC[2];
    
   public:
    PmlDiscontinuous_PmlDiscontinuous(const unsigned int corner, PmlDiscontinuous *first, PmlDiscontinuous *second);
    ~PmlDiscontinuous_PmlDiscontinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >* getV(const unsigned int i) const;
    
    PmlDiscontinuous *firstBoundary() const;
    PmlDiscontinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlWithoutMultiplier_PmlWithoutMultiplier : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uPmlCorner;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vC[2];
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vCorner;
    
   public:
    PmlWithoutMultiplier_PmlWithoutMultiplier(const unsigned int corner, PmlWithoutMultiplier *first, PmlWithoutMultiplier *second);
    ~PmlWithoutMultiplier_PmlWithoutMultiplier();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV(const unsigned int i) const;
    
    PmlWithoutMultiplier *firstBoundary() const;
    PmlWithoutMultiplier *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class PmlContinuous_Sommerfeld : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vC;
    
   public:
    PmlContinuous_Sommerfeld(const unsigned int corner, PmlContinuous *first, Sommerfeld *second);
    ~PmlContinuous_Sommerfeld();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    PmlContinuous *firstBoundary() const;
    Sommerfeld *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class Sommerfeld_PmlContinuous : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _vC;
    
   public:
    Sommerfeld_PmlContinuous(const unsigned int corner, Sommerfeld *first, PmlContinuous *second);
    ~Sommerfeld_PmlContinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getV() const;
    
    Sommerfeld *firstBoundary() const;
    PmlContinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlDiscontinuous_Sommerfeld : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 > * _vC;
    
   public:
    PmlDiscontinuous_Sommerfeld(const unsigned int corner, PmlDiscontinuous *first, Sommerfeld *second);
    ~PmlDiscontinuous_Sommerfeld();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >* getV() const;
    
    PmlDiscontinuous *firstBoundary() const;
    Sommerfeld *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class Sommerfeld_PmlDiscontinuous : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 > * _vC;
    
   public:
    Sommerfeld_PmlDiscontinuous(const unsigned int corner, Sommerfeld *first, PmlDiscontinuous *second);
    ~Sommerfeld_PmlDiscontinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >* getV() const;
    
    Sommerfeld *firstBoundary() const;
    PmlDiscontinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class HABC_Sommerfeld : public Corner
  {
   protected:
    std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > _vC;
    
   public:
    HABC_Sommerfeld(const unsigned int corner, HABC *first, Sommerfeld *second);
    ~HABC_Sommerfeld();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *getV(const unsigned int m) const;
    
    HABC *firstBoundary() const;
    Sommerfeld *secondBoundary() const;
      
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class Sommerfeld_HABC : public Corner
  {
   protected:
    std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > _vC;
    
   public:
    Sommerfeld_HABC(const unsigned int corner, Sommerfeld *first, HABC *second);
    ~Sommerfeld_HABC();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *getV(const unsigned int m) const;
    
    Sommerfeld *firstBoundary() const;
    HABC *secondBoundary() const;
      
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };


}

#endif // H_SUBPROBLEM2D
