#include <gmsh.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshfem/Message.h>

using gmshfem::equation::dof;
using gmshfem::equation::tf;
using gmshfem::function::operator-;
using gmshfem::function::operator*;

void checkerboard(const unsigned int nDomX, const unsigned int nDomY, const double xSize, const double ySize, const double lc, const int order, const unsigned int sourcePosX, const unsigned int sourcePosY)
{
  //    ***********
  // j  *         *
  // ^  *         *
  // |  *         *
  // |  ***********
  // |
  // -------> i
  std::vector< std::vector< int > > omega(nDomX);
  for(unsigned int i = 0; i < nDomX; ++i) {
    omega[i].resize(nDomY);
  }

  // ******************
  // borders
  // ******************

  //  ****(1)****
  //  *         *
  // (2)       (0)
  //  *         *
  //  ****(3)****
  std::vector< std::vector< std::vector< int > > > borders(nDomX);
  for(unsigned int i = 0; i < nDomX; ++i) {
    borders[i].resize(nDomY);
    for(unsigned int j = 0; j < nDomY; ++j) {
      borders[i][j].resize(4);
    }
  }

  std::vector< int > left(nDomY);
  int bottom = 0;
  for(unsigned int i = 0; i < nDomX; ++i) {
    for(unsigned int j = 0; j < nDomY; ++j) {
      // (p3)*******(p2)
      //  *          *
      //  *          *
      //  *          *
      // (p0)*******(p1)
      int p[4];
      p[0] = gmsh::model::geo::addPoint(i * xSize, j * ySize, 0., lc);
      p[1] = gmsh::model::geo::addPoint((i + 1) * xSize, j * ySize, 0., lc);
      p[2] = gmsh::model::geo::addPoint((i + 1) * xSize, (j + 1) * ySize, 0., lc);
      p[3] = gmsh::model::geo::addPoint(i * xSize, (j + 1) * ySize, 0., lc);

      int l[4];
      borders[i][j][0] = l[0] = gmsh::model::geo::addLine(p[1], p[2]);
      borders[i][j][1] = l[1] = gmsh::model::geo::addLine(p[2], p[3]);
      borders[i][j][2] = l[2] = gmsh::model::geo::addLine(p[3], p[0]);
      borders[i][j][3] = l[3] = gmsh::model::geo::addLine(p[0], p[1]);

      if(i == 0) {
        left[j] = l[0];
      }
      else {
        borders[i][j][2] = -left[j];
        left[j] = l[0];
      }

      if(j == 0) {
        bottom = l[1];
      }
      else {
        borders[i][j][3] = -bottom;
        bottom = l[1];
      }

      int ll = gmsh::model::geo::addCurveLoop({l[0], l[1], l[2], l[3]});

      int surface = gmsh::model::geo::addPlaneSurface({ll});
      omega[i][j] = surface;

      if(i == sourcePosX && j == sourcePosY) {
        // source
        int pS = gmsh::model::geo::addPoint(sourcePosX * i + xSize / 2., sourcePosY * j + ySize / 2., 0., lc);

        gmsh::model::geo::synchronize();
        gmsh::model::mesh::embed(0, {pS}, 2, surface);

        gmsh::model::addPhysicalGroup(0, {pS}, 1);
        gmsh::model::setPhysicalName(0, 1, "source");
      }
    }
  }
  gmsh::model::geo::removeAllDuplicates();

  gmsh::model::geo::synchronize();

  // ******************
  // physicals
  // ******************

  for(unsigned int i = 0; i < nDomX; ++i) {
    for(unsigned int j = 0; j < nDomY; ++j) {
      gmsh::model::addPhysicalGroup(2, {omega[i][j]}, i * nDomY + j + 1);
      gmsh::model::setPhysicalName(2, i * nDomY + j + 1, "omega_" + std::to_string(i) + "_" + std::to_string(j));

      gmsh::model::addPhysicalGroup(1, {borders[i][j][0]}, 10000 + i * nDomY + j + 1);
      gmsh::model::setPhysicalName(1, 10000 + i * nDomY + j + 1, "sigmaE_" + std::to_string(i) + "_" + std::to_string(j));

      gmsh::model::addPhysicalGroup(1, {borders[i][j][1]}, 20000 + i * nDomY + j + 1);
      gmsh::model::setPhysicalName(1, 20000 + i * nDomY + j + 1, "sigmaN_" + std::to_string(i) + "_" + std::to_string(j));

      gmsh::model::addPhysicalGroup(1, {borders[i][j][2]}, 30000 + i * nDomY + j + 1);
      gmsh::model::setPhysicalName(1, 30000 + i * nDomY + j + 1, "sigmaW_" + std::to_string(i) + "_" + std::to_string(j));

      gmsh::model::addPhysicalGroup(1, {borders[i][j][3]}, 40000 + i * nDomY + j + 1);
      gmsh::model::setPhysicalName(1, 40000 + i * nDomY + j + 1, "sigmaS_" + std::to_string(i) + "_" + std::to_string(j));
    }
  }

  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate();
  gmsh::model::mesh::setOrder(order);
}

int main(int argc, char **argv)
{
  gmshddm::common::GmshDdm gmshDdm(argc, argv);

  int nDomX = 10, nDomY = 10;
  gmshDdm.userDefinedParameter(nDomX, "nDomX");
  gmshDdm.userDefinedParameter(nDomY, "nDomY");
  double xSize = 0.1, ySize = 0.1;
  gmshDdm.userDefinedParameter(xSize, "xSize");
  gmshDdm.userDefinedParameter(ySize, "ySize");
  double lc = 0.1;
  gmshDdm.userDefinedParameter(lc, "lc");
  int meshOrder = 1;
  gmshDdm.userDefinedParameter(meshOrder, "meshOrder");
  int sourcePosX = 0, sourcePosY = 0;
  gmshDdm.userDefinedParameter(sourcePosX, "sourcePosX");
  gmshDdm.userDefinedParameter(sourcePosY, "sourcePosY");

  // Build geometry and mesh using gmsh API
  checkerboard(nDomX, nDomY, xSize, ySize, lc, meshOrder, sourcePosX, sourcePosY);

  const double pi = 3.14159265359;
  const std::complex< double > im(0., 1.);
  double k = 2. * pi;
  gmshDdm.userDefinedParameter(k, "k");
  std::string interface = "sommerfeld";
  gmshDdm.userDefinedParameter(interface, "interface");
  std::string gauss = "Gauss4";
  gmshDdm.userDefinedParameter(gauss, "gauss");

  unsigned int nDom = nDomX * nDomY;
  // Define domain
  gmshddm::domain::Subdomain omega(nDom);
  gmshfem::domain::Domain gamma = gmshfem::domain::Domain(0, 1);

  std::vector< gmshddm::domain::Subdomain > sigma(4, nDom);
  std::vector< gmshddm::domain::Interface > sigmaInterface(4, nDom);

  // Define topology
  std::vector< std::vector< unsigned int > > topology(nDom);
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDomX); ++i) {
    for(unsigned int j = 0; j < static_cast< unsigned int >(nDomY); ++j) {
      unsigned int index = i * nDomY + j;

      omega(index) = gmshfem::domain::Domain(2, index + 1);

      for(unsigned int f = 0; f < 4; ++f) {
        sigma[f](index) = gmshfem::domain::Domain(1, (f + 1) * 10000 + i * nDomY + j + 1);
      }

      if(i != static_cast< unsigned int >(nDomX) - 1) { // E
        sigmaInterface[0](index, (i + 1) * nDomY + j) = sigma[0](index);
        topology[index].push_back((i + 1) * nDomY + j);
      }

      if(j != static_cast< unsigned int >(nDomY) - 1) { // N
        sigmaInterface[1](index, i * nDomY + (j + 1)) = sigma[1](index);
        topology[index].push_back(i * nDomY + (j + 1));
      }

      if(i != 0) { // W
        sigmaInterface[2](index, (i - 1) * nDomY + j) = sigma[2](index);
        topology[index].push_back((i - 1) * nDomY + j);
      }

      if(j != 0) { // S
        sigmaInterface[3](index, i * nDomY + (j - 1)) = sigma[3](index);
        topology[index].push_back(i * nDomY + (j - 1));
      }
    }
  }

  gmshddm::problem::Formulation< std::complex< double > > formulation("HelmholtzDDM", topology);

  // Fields definition
  gmshddm::field::SubdomainField< std::complex< double >, gmshfem::field::Form::Form0 > u("u", omega | sigma[0] | sigma[1] | sigma[2] | sigma[3], gmshfem::field::FunctionSpaceTypeForm0::Lagrange);
  u(sourcePosX * nDomY + sourcePosY).domain(u(sourcePosX * nDomY + sourcePosY).domain() | gamma);

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > lambda("lambda", gamma, gmshfem::field::FunctionSpaceTypeForm0::Lagrange);

  const std::vector< std::string > orientation{"E", "N", "W", "S"};
  std::vector< gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > > g;
  g.reserve(4);
  for(unsigned int f = 0; f < 4; ++f) {
    g.push_back(gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 >("g_" + orientation[f], sigmaInterface[f], gmshfem::field::FunctionSpaceTypeForm0::Lagrange));
    formulation.addInterfaceField(g[f]);
  }

  std::vector< gmshddm::field::SubdomainField< std::complex< double >, gmshfem::field::Form::Form0 > > v;
  v.reserve(4);
  for(unsigned int f = 0; f < 4; ++f) {
    v.push_back(gmshddm::field::SubdomainField< std::complex< double >, gmshfem::field::Form::Form0 >("v_" + orientation[f], sigma[f], gmshfem::field::FunctionSpaceTypeForm0::Lagrange));
  }

  for(unsigned int i = 0; i < nDomX * nDomY; ++i) {
    formulation(i).integral(-grad(dof(u(i))), grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral(k * k * dof(u(i)), tf(u(i)), omega(i), gauss);

    if(i == sourcePosX * nDomY + sourcePosY) {
      formulation(i).integral(dof(lambda), tf(u(i)), gamma, gauss);
      formulation(i).integral(dof(u(i)), tf(lambda), gamma, gauss);
      formulation(i).integral(formulation.physicalSource(-1.), tf(lambda), gamma, gauss);
    }

    // boundary
    if(interface == "sommerfeld") {
      for(unsigned int f = 0; f < 4; ++f) {
        formulation(i).integral(-dof(v[f](i)), tf(u(i)), sigma[f](i), gauss);
        formulation(i).integral(dof(v[f](i)), tf(v[f](i)), sigma[f](i), gauss);
        formulation(i).integral(im * k * dof(u(i)), tf(v[f](i)), sigma[f](i), gauss);
      }
    }

    // coupling
    for(unsigned int f = 0; f < 4; ++f) {
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        if(!sigmaInterface[f](i, j).isEmpty()) {
          formulation(i).integral(formulation.artificialSource(-g[(f + 2) % 4](j, i)), tf(u(i)), sigmaInterface[f](i, j), gauss);
        }
      }
    }

    // interface
    for(unsigned int f = 0; f < 4; ++f) {
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        if(!sigmaInterface[f](i, j).isEmpty()) {
          formulation(i, j).integral(dof(g[f](i, j)), tf(g[f](i, j)), sigmaInterface[f](i, j), gauss);
          formulation(i, j).integral(formulation.artificialSource(g[(f + 2) % 4](j, i)), tf(g[f](i, j)), sigmaInterface[f](i, j), gauss);
          formulation(i, j).integral(2. * v[f](i), tf(g[f](i, j)), sigmaInterface[f](i, j), gauss);
        }
      }
    }
  }

  double tol = 1e-4;
  gmshDdm.userDefinedParameter(tol, "tol");
  std::string solver = "bcgs";
  gmshDdm.userDefinedParameter(solver, "solver");

  formulation.pre();
  formulation.solve(solver, tol, 1000);

  for(unsigned int i = 0; i < nDom; ++i) {
    gmshfem::post::save(u(i), omega(i), "u_" + std::to_string(i), "msh");
  }

  return 0;
}
