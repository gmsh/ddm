cd build
cmake .. && make

for omega in 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100 105 110 115 120
do
  echo "********* omega = $omega *******************"
  ./example -Transmission Taylor0 -w $omega -alpha 0 > test.txt
  grep "Iteration" test.txt | wc -l >> Niter1.txt
  ./example -Transmission Taylor2Y -w $omega > test.txt
  grep "Iteration" test.txt | wc -l >> Niter2.txt
  ./example -Transmission PadeY -w $omega  > test.txt
  grep "Iteration" test.txt | wc -l >> Niter3.txt
  echo $omega >> omegas.txt
done 

paste omegas.txt Niter1.txt Niter2.txt Niter3.txt >> Results.txt
rm omegas.txt Niter1.txt Niter2.txt Niter3.txt