#include <gmshddm/GmshDdm.h>
#include <gmshddm/Formulation.h>
#include <gmshfem/Message.h>
#include <gmshddm/MPIInterface.h>
#include "gmsh.h"
#include <gmshfem/AnalyticalFunction.h>
#include "Bessel.h"
#include "mpi.h"
#include <iostream>
#include <cstdio>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::analytics;
using namespace gmshfem::post;

std::complex<double> EvalAiryArg(double, double, double, double, double, std::complex<double>);
std::complex<double> airy(std::complex<double>, bool);
ScalarFunction< std::complex< double > > GetepsilonOpt(double, double, double, ScalarFunction< std::complex< double > >, double);
double GetepsilonOpt(double, double, double, double, double);

void waveguide(const unsigned int nDom, const double l, const double L, const double lc, const int MeshOrder)
{
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2." << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("waveguide");

  const double domainSize = L / nDom;

  std::vector< std::vector< int > > borders(nDom);
  std::vector< int > omegas(nDom);
  std::vector< int > sigmas(nDom - 1);

  int p[2];

  p[0] = gmsh::model::geo::addPoint(0., l, 0., lc);
  p[1] = gmsh::model::geo::addPoint(0., 0., 0., lc);

  int line = gmsh::model::geo::addLine(p[0], p[1]);
  gmsh::model::addPhysicalGroup(1, {line}, 1);
  gmsh::model::setPhysicalName(1, 1, "gammaDir");

  for(unsigned int i = 0; i < nDom; ++i) {
    std::vector< std::pair< int, int > > extrudeEntities;
    gmsh::model::geo::extrude({std::make_pair(1, line)}, domainSize, 0., 0., extrudeEntities, {static_cast< int >((domainSize) / lc)}, std::vector< double >(), true);
    line = extrudeEntities[0].second;
    omegas[i] = extrudeEntities[1].second;
    borders[i].push_back(extrudeEntities[2].second);
    borders[i].push_back(extrudeEntities[3].second);
    sigmas[i] = line;
  }

  gmsh::model::addPhysicalGroup(1, {line}, 2);
  gmsh::model::setPhysicalName(1, 2, "gammaInf");

  for(unsigned int i = 0; i < borders.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, borders[i], 100 + i);
    gmsh::model::setPhysicalName(1, 100 + i, "border_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < omegas.size(); ++i) {
    gmsh::model::addPhysicalGroup(2, {omegas[i]}, i + 1);
    gmsh::model::setPhysicalName(2, i + 1, "omega_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < sigmas.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, {sigmas[i]}, 200 + i);
    gmsh::model::setPhysicalName(1, 200 + i, "sigma_" + std::to_string(i) + "_" + std::to_string(i + 1));
  }

  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate();
  gmsh::model::mesh::setOrder(MeshOrder);
  gmsh::write("m.msh");
}

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);
  const unsigned int MPI_Rank = getMPIRank();

  double pi = 3.14159265358979323846264338327950288;
  const std::complex< double > im(0., 1.);
  std::complex< double > exp_T = std::complex<double>(std::cos(2*pi/3.),-std::sin(2*pi/3.));
  int nDom = 4;
  gmshDdm.userDefinedParameter(nDom, "nDom");

  // duct geometry parameters
  double L = 1;
  double H = 0.5;

  // numerical parameters
  double MinPtsByWl = 12;
  gmshDdm.userDefinedParameter(MinPtsByWl, "MinPtsByWl");
  int FEMorder = 4;
  gmshDdm.userDefinedParameter(FEMorder, "FEMorder");
  std::string gauss = "Gauss"+std::to_string(2*FEMorder+1);

  // physical parameters
  double w = 30;
  gmshDdm.userDefinedParameter(w, "w");
  int mode = 5;
  double ky = mode*pi/H;

  // speed of sound c_0(x)^(-2)=nbar*x+ni, linear profile
  double nbar = 5.0;
  double ni = 0.1;
  ScalarFunction< std::complex< double > > c0_m2 = nbar*x< std::complex< double > >() + ni;
  ScalarFunction< std::complex< double > > c0 = sqrt(1/c0_m2);
  ScalarFunction< std::complex< double > > dx_c0_m2 = nbar; // x-derivative
  ScalarFunction< std::complex< double > > k0 = w*sqrt(c0_m2); // wavenumber

  // meshing
  double lc = 2*pi * FEMorder / ( w*sqrt(nbar*L+ni) ) / MinPtsByWl;
  waveguide(nDom, H, L, lc, FEMorder);
  // upper wall boundary condition
  std::string wallType = "Neumann";
  gmshDdm.userDefinedParameter(wallType, "wallType");

  double sq = w*w*ni-ky*ky; // radicand of the principal symbol at x=0
  if (sq >= 0) {
    msg::info << " Input propagative mode " << msg::endl;
  }
  else {
    msg::info << " Input evanescent mode " << msg::endl;
  }
  double Xt = ky*ky/(nbar*w*w) - ni/nbar; // evalaute turning point location
  if ( (Xt >= 0) && (Xt <= L) ) {
    msg::info << " Turning point within the duct at Xt = " << Xt << msg::endl;
  }
  else {
    msg::info << " Turning point outside of the physical domain at Xt = " << Xt << msg::endl;
  }
  // use optimal epsilon for cut-on/cut-off transistion
  ScalarFunction< std::complex< double > > eps = GetepsilonOpt(w, ni, nbar, x< std::complex<double> > (), pi);

  // transmission conditions
  std::string Transmission = "Pade-var"; // Taylor0, Taylor2, Pade (padeOrder,alpha), Pade-var (padeOrder,alpha)
  gmshDdm.userDefinedParameter(Transmission, "Transmission");
  if (Transmission == "Pade-var") {
    for (int i=1; i < nDom; i++) {
      msg::info << " Use optimal epsilon " << GetepsilonOpt(w, ni, nbar, (double)i*L/(double)nDom, pi) << " for interface at x = " << (double)i*L/(double)nDom << msg::endl;
    }
  }
  // init transmission operators
  ScalarFunction< std::complex< double > > Si, Sj, dS;
  double alpha=-pi/2.; // rotation branch-cut, in [0, -pi]
  gmshDdm.userDefinedParameter(alpha, "alpha");
  if ( MPI_Rank == 0 ) {
    msg::info << " use " << Transmission << " as transmission condition with alpha = " << alpha << msg::endl;
  }

  // Define domain
  Subdomain omega(nDom);
  Subdomain gammaInf(nDom);
  Subdomain gammaDir(nDom);
  Subdomain border(nDom);
  Interface sigma(nDom);

  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    omega(i) = Domain(2, i + 1);
    if(i == (static_cast< unsigned int >(nDom-1)) ) {
      gammaInf(i) = Domain(1, 2); // outgoing boundary
    }

    if(i == 0) {
      gammaDir(i) = Domain(1, 1); // ingoing boundary
    }
    border(i) = Domain(1, 100 + i); // upper and lower walls

    if(i != 0) {
      sigma(i, i - 1) = Domain(1, 200 + i - 1);
    }
    if(i != (static_cast< unsigned int >(nDom-1)) ) {
      sigma(i, i + 1) = Domain(1, 200 + i);
    }
  }

  // Define topology
  std::vector< std::vector< unsigned int > > topology(nDom);
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    if(i != 0) {
      topology[i].push_back(i - 1);
    }
    if(i != (static_cast< unsigned int >(nDom-1)) ) {
      topology[i].push_back(i + 1);
    }
  }

  // Create DDM formulation
  std::vector< FieldInterface< std::complex< double > > * > fieldBucket;
  gmshddm::problem::Formulation< std::complex< double > > formulation("Helmholtz-c0x", topology);

  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega | gammaDir | gammaInf | border | sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  // Tell to the formulation that g is field that have to be exchanged between subdomains
  formulation.addInterfaceField(g);
  // analytical solution
  Function< std::complex< double >, Degree::Degree0 > *solution = nullptr;

  msg::info << "********************************" << msg::endl;
  msg::info << " - heterogeneous Helmholtz duct problem, variable speed of sound along the wavefront " << msg::endl;
  msg::info << " - Input wavenumber = " << w << msg::endl;
  msg::info << " - Input mode = " << mode << msg::endl;
  msg::info << " - FEM basis order = " << FEMorder << "" << msg::endl;
  msg::info << " - Mesh size = " << lc << "" << msg::endl;
  msg::info << "********************************" << msg::endl;

  if ( wallType == "Dirichlet" ) { // wall constraints for soft walls
    solution = new AnalyticalFunction< helmholtz2D::DuctModeSolutionAiry< std::complex< double > > >(w, H, nbar, ni, 0., 0., mode, 1);
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDom+1); ++i) {
      u(i).addConstraint(border(i), 0.);
    }
  }
  else {
    solution = new AnalyticalFunction< helmholtz2D::DuctModeSolutionAiry< std::complex< double > > >(w, H, nbar, ni, 0., 0., mode, 0);
  }

  // input boundary condition
  std::complex<double> z0 = EvalAiryArg(0., w, nbar, ni, ky, exp_T);
  std::complex< double > Ampli = exp_T * pow(nbar*w*w,1./3.)*airy(z0,1);
  std::complex<double> zL = EvalAiryArg(L, w, nbar, ni, ky, exp_T);
  std::complex< double > kx = -im * exp_T * pow(nbar*w*w,1./3.) * airy(zL,1)/airy(zL,0);

  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    // weak formulation
    formulation(i).integral(grad(dof(u(i))),grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral(- w*w*c0_m2 * dof(u(i)), tf(u(i)), omega(i), gauss);
    // Analytic outgoing DtN
    formulation(i).integral(im * kx * dof(u(i)), tf(u(i)), gammaInf(i), gauss);

    // input BC
    if(wallType == "Dirichlet"){
      formulation(i).integral(- formulation.physicalSource( Ampli * sin(ky*y< std::complex<double> >()) ) , tf(u(i)), gammaDir(i), gauss);
    }
    else{
      formulation(i).integral(- formulation.physicalSource( Ampli * cos(ky*y< std::complex<double> >() ) ), tf(u(i)), gammaDir(i), gauss);
    }

    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      // Artificial source
      formulation(i).integral(formulation.artificialSource( -g(j,i) ), tf(u(i)), sigma(i,j), gauss);
    }

    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      // Interface terms
      formulation(i,j).integral(dof(g(i,j)), tf(g(i,j)), sigma(i,j), gauss);
      formulation(i,j).integral(formulation.artificialSource( g(j,i) ), tf(g(i,j)), sigma(i,j), gauss);
    }

    // Transmission condition
    if ( Transmission == "Taylor0" ) {
      Si = im*k0*exp(im*alpha/2.);
      Sj = im*k0*exp(im*alpha/2.);
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i,j).integral(- (Si+Sj) * u(i), tf(g(i,j)), sigma(i,j), gauss);
      }
    }
    else if ( Transmission.std::string::find("Taylor2") != std::string::npos ) {
      Si = im*k0*cos(alpha/2.);
      Sj = im*k0*cos(alpha/2.);
      dS = im*exp(-im*alpha/2.0)/(2.0*k0);
      if ( Transmission == "Taylor2-L0" ) {
        Si = Si + 0.25*dx_c0_m2*(c0*c0);
        Sj = Sj + 0.25*dx_c0_m2*(c0*c0);
        dS = dS - dx_c0_m2 / (4.*k0*k0);
      }
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i).integral(- dS * grad( dof(u(i)) ), grad( tf(u(i)) ), sigma(i,j), gauss);
        formulation(i,j).integral(- (Si+Sj) * u(i), tf(g(i,j)), sigma(i,j), gauss);
        formulation(i,j).integral(+ (2.*dS) * grad( u(i) ), grad( tf(g(i,j)) ), sigma(i,j), gauss);
      }
    }
    else if ( Transmission == "DtN" ) {
      std::complex<double> z =  exp_T * (ky*ky - w*w*(nbar * 0.5 +ni)) / pow(w*w*nbar,2./3.);
      std::complex<double> Kx = -im * exp_T * pow(nbar*w*w,1./3.) * airy(z,1)/airy(z,0);
      Si = im * Kx;
      Sj = Si;
      dS = 0;
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i,j).integral(- (Si+Sj) * u(i), tf(g(i,j)), sigma(i,j), gauss);
      }
    }
    else if ( Transmission.std::string::find("Pade") != std::string::npos) {
      int padeOrder = 5;
      gmshDdm.userDefinedParameter(padeOrder, "padeOrder");
      if ( i == 0 && MPI_Rank == 0 ) {
        msg::info << "Use " << padeOrder << " auxiliary fields "<< msg::endl;
      }

      const double Np = 2. * padeOrder + 1.;
      const std::complex< double > exp1 = std::complex<double>(std::cos(alpha),std::sin(alpha));
      const std::complex< double > exp2 = std::complex<double>(std::cos(alpha/2.),std::sin(alpha/2.));
      const std::complex< double > coef = 2./Np;

      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral( im * k0 * exp2 * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        // interface unknowns
        formulation(i,j).integral(- 2. * (im * k0 * exp2) * u(i), tf(g(i,j)), sigma(i,j), gauss);

        // define Pade coefficients
        std::vector< std::complex< double > > c(padeOrder, 0.);
        for(int l = 0; l < padeOrder; ++l) {
          c[l] = std::tan((l + 1) * pi / Np);
          c[l] *= c[l];
        }

        // define the auxiliary fields
        std::vector< Field< std::complex< double >, Form::Form0 >* > phi;
        Field< std::complex< double >, Form::Form0 >* phis;
        for(int l = 0; l < padeOrder; ++l) {
          phis = new Field< std::complex< double >, Form::Form0 >("phi_" + std::to_string(l), sigma(i,j), FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
          if(wallType == "soft") { // impose Dirichlet constraints on the auxiliary fields
            phis->addConstraint(border(i),0.);
            phis->addConstraint(border(i),0.);
          }
          phi.push_back(phis);
          fieldBucket.push_back(phi.back());
        }

        // write the augmented weak form
        for(int l = 0; l < padeOrder; ++l) {
          // boundary integral terms relating the auxiliary fields
          formulation(i).integral(im * k0 * exp2 * coef * c[l] * dof(*phi[l]), tf(u(i)), sigma(i,j), gauss);
          formulation(i).integral(im * k0 * exp2 * coef * c[l] * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
          // coupling of the auxiliary equations
          formulation(i).integral(grad(dof(*phi[l])), grad(tf(*phi[l])), sigma(i,j), gauss);
          formulation(i).integral( -(k0*k0) * (exp1 * c[l] + 1.) * dof(*phi[l]), tf(*phi[l]), sigma(i,j), gauss);
          formulation(i).integral( -(k0*k0) * exp1 * (c[l] + 1.) * dof(u(i)), tf(*phi[l]), sigma(i,j), gauss);
        }

        // interfaces unknowns
        for(int l = 0; l < padeOrder; ++l) {
          formulation(i,j).integral(- 2. * (im * k0 * exp2) * coef * c[l] * (*phi[l]), tf(g(i,j)), sigma(i,j), gauss);
          formulation(i,j).integral(- 2. * (im * k0 * exp2) * coef * c[l] * u(i), tf(g(i,j)), sigma(i,j), gauss);
        }

        if ( Transmission == "Pade-var" ) {
          // define the auxiliary fields
          phis = new Field< std::complex< double >, Form::Form0 >("psi", sigma(i,j), FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
          if(wallType == "soft") { // impose Dirichlet constraints on the auxiliary fields
            phis->addConstraint(border(i),0.);
            phis->addConstraint(border(i),0.);
          }
          phi.push_back(phis);
          fieldBucket.push_back(phi.back());

          if ( i == 0 && MPI_Rank == 0 ) {
            msg::info << "Apply Pade transmission condition for heterogenous media !" << msg::endl;
          }
          // boundary integral terms relating the auxiliary fields
          formulation(i).integral( dx_c0_m2*(c0*c0) / 4.0 * dof(*phi[padeOrder]), tf(u(i)), sigma(i,j), gauss);
          // interface unknowns
          formulation(i,j).integral(- 2. * ( dx_c0_m2*(c0*c0) / 4.0 ) * (*phi[padeOrder]), tf(g(i,j)), sigma(i,j), gauss);
          // coupling of the auxiliary equations
          formulation(i).integral( grad( dof(*phi[padeOrder]) ), grad( tf(*phi[padeOrder]) ), sigma(i,j), gauss);
          formulation(i).integral(- (w - im*eps)*(w - im*eps)/(c0*c0) * dof(*phi[padeOrder]), tf(*phi[padeOrder]), sigma(i,j), gauss);
          formulation(i).integral( (w - im*eps)*(w - im*eps)/(c0*c0) * dof(u(i)), tf(*phi[padeOrder]), sigma(i,j), gauss);
        }
      }
    }
    else {
      msg::error << "Transmission condition not available !" << msg::endl;
      exit(0);
    }
  }

  // Solve the DDM formulation
  formulation.pre();
  std::string solver = "gmres"; // gmres, jacobi
  double tol = 1e-6;
  int maxIt = 200;
  gmshDdm.userDefinedParameter(tol,"tol");
  gmshDdm.userDefinedParameter(solver,"solver");
  gmshDdm.userDefinedParameter(maxIt,"maxIt");
  formulation.solve(solver,tol, maxIt, true);

  // FEM monodomain solution
  Domain omegaMono, borderMono;
  Domain gammaDirMono = Domain(1, 1);
  Domain gammaInfMono = Domain(1, 2);
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    omegaMono |= omega(i);
    borderMono |= border(i);
  }

  gmshfem::field::Field< std::complex< double >, Form::Form0 > uMono("uMono", omegaMono | borderMono | gammaDirMono, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  gmshfem::problem::Formulation< std::complex< double > > monodomain("HelmholtzMono");
  if(wallType == "soft") {
    uMono.addConstraint(borderMono, 0.);
  }
  monodomain.integral(grad(dof(uMono)),grad(tf(uMono)), omegaMono, gauss);
  monodomain.integral(- w*w*c0_m2 * dof(uMono), tf(uMono), omegaMono, gauss);
  monodomain.integral(im * kx * dof(uMono), tf(uMono), gammaInfMono, gauss);

  // input BC
  if(wallType == "soft"){
    monodomain.integral(- Ampli * sin(ky*y< std::complex<double> >() ), tf(uMono), gammaDirMono, gauss);
  }
  else{
    monodomain.integral(- Ampli * cos(ky*y< std::complex<double> >() ), tf(uMono), gammaDirMono, gauss);
  }

  monodomain.pre();
  monodomain.assemble();
  monodomain.solve();

  // Analytic L2 error
  double numa = 0., dena = 0.;
  double average=0;
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    if ( gmshddm::mpi::isItMySubdomain(i) ) {
      save(+u(i),omega(i),"u_"+std::to_string(i));
      std::complex< double > denLocala = integrate(pow(abs( *solution ), 2), omega(i), gauss);
      std::complex< double > numLocala = integrate(pow(abs(*solution - u(i)), 2), omega(i), gauss);
      numa = numLocala.real();
      dena = denLocala.real();
      msg::info << "Local analytic-L2 error = " << sqrt(numa / dena) << " on subdomain " << i << msg::endl;
      average += sqrt(numa / dena);
    }
  }
  msg::info << "Average analytic-L2 error = " << average/(double)nDom << msg::endl;

  // Monodomain L2 error
  double num = 0., den = 0.;
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    if ( gmshddm::mpi::isItMySubdomain(i) ) {
      std::complex< double > denLocal = integrate(pow(abs( uMono ), 2), omega(i), gauss);
      std::complex< double > numLocal = integrate(pow(abs(uMono - u(i)), 2), omega(i), gauss);
      num = numLocal.real();
      den = denLocal.real();
      msg::info << "Local monodomain-L2 error = " << sqrt(num/den) << " on subdomain " << i << msg::endl;
    }
  }

  bool save_eigs = false;
  gmshDdm.userDefinedParameter(save_eigs,"save_eigs");
  if (save_eigs) {
    gmshfem::algebra::MatrixCCS<std::complex<double>> mat;
    mat = formulation.computeMatrix();
    msg::info << "nnz = " << mat.numberOfNonZero() << ", symmetric = " << mat.isSymmetric() << ", hermitian = " << mat.isHermitian() << msg::endl;
    mat.save("Mymatrix_"+Transmission);
  }

  for(unsigned int i = 0; i < fieldBucket.size(); ++i) {
    delete fieldBucket[i];
  }

  return 0;
}

std::complex<double> EvalAiryArg(double x, double w, double a, double b, double ky, std::complex<double> angle)  {
  return angle * (ky*ky - w*w*(a*x+b)) / pow(w*w*a,2./3.);
}

std::complex<double> airy(std::complex<double> z, bool derivative)
{
    double valr, vali;
    int err = AiryComplex(std::real(z), std::imag(z), derivative, &valr, &vali);
    if (err != 0) {
      msg::warning << "issue with complex airy function, error output " << err << msg::endl;
    }
    return std::complex<double> (valr,vali);
}

ScalarFunction< std::complex< double > > GetepsilonOpt(double w, double ni, double nbar, ScalarFunction< std::complex< double > > Xt, double pi)
{
  ScalarFunction< std::complex< double > > ky_t = w*sqrt(nbar*Xt+ni);
  double s = std::sin(2.*pi/3.);

  // optimal epsilon for cut-on/cut-off transition
  ScalarFunction< std::complex< double > > eps_opt;
  eps_opt = 2. * w * s * ( 1 - sqrt( 1 + std::real(airy(std::complex<double>(0,0),0))*pow(nbar*w*w,2./3.) / ( 8.*pow(s*ky_t,2)*std::real(airy(std::complex<double>(0,0),1)) )  ));
  return eps_opt;
}

double GetepsilonOpt(double w, double ni, double nbar, double Xt, double pi)
{
  double ky_t = w*sqrt(nbar*Xt+ni);
  double s = std::sin(2.*pi/3.);

  // optimal epsilon for cut-on/cut-off transition
  std::complex< double > eps_opt;
  eps_opt = 2. * w * s * ( 1 - sqrt( 1 + std::real(airy(std::complex<double>(0,0),0))*pow(nbar*w*w,2./3.) / ( 8.*pow(s*ky_t,2)*std::real(airy(std::complex<double>(0,0),1)) )  ));
  return std::real(eps_opt);
}
