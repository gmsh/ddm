#include <gmshddm/GmshDdm.h>
#include <gmshddm/Formulation.h>
#include <gmshfem/Message.h>
#include <gmshddm/MPIInterface.h>
#include "gmsh.h"
#include <iostream>
#include <cstdio>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;

void waveguide(const unsigned int nDom, const double l, const double L, const double Lpml, const double lc, const int MeshOrder)
{
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2." << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("waveguide");

  const double domainSize = L / nDom;

  std::vector< std::vector< int > > borders(nDom);
  std::vector< int > omegas(nDom);
  std::vector< int > sigmas(nDom - 1);

  int p[2];

  p[0] = gmsh::model::geo::addPoint(0., l, 0., lc);
  p[1] = gmsh::model::geo::addPoint(0., 0., 0., lc);

  int line = gmsh::model::geo::addLine(p[0], p[1]);
  gmsh::model::addPhysicalGroup(1, {line}, 1);
  gmsh::model::setPhysicalName(1, 1, "gammaDir");

  for(unsigned int i = 0; i < nDom; ++i) {
    std::vector< std::pair< int, int > > extrudeEntities;
    gmsh::model::geo::extrude({std::make_pair(1, line)}, domainSize, 0., 0., extrudeEntities, {static_cast< int >((domainSize) / lc)}, std::vector< double >(), true);
    line = extrudeEntities[0].second;
    omegas[i] = extrudeEntities[1].second;
    borders[i].push_back(extrudeEntities[2].second);
    borders[i].push_back(extrudeEntities[3].second);
    sigmas[i] = line;
  }

  gmsh::model::addPhysicalGroup(1, {line}, 2);
  gmsh::model::setPhysicalName(1, 2, "gammaInf");

  for(unsigned int i = 0; i < borders.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, borders[i], 100 + i);
    gmsh::model::setPhysicalName(1, 100 + i, "border_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < omegas.size(); ++i) {
    gmsh::model::addPhysicalGroup(2, {omegas[i]}, i + 1);
    gmsh::model::setPhysicalName(2, i + 1, "omega_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < sigmas.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, {sigmas[i]}, 200 + i);
    gmsh::model::setPhysicalName(1, 200 + i, "sigma_" + std::to_string(i) + "_" + std::to_string(i + 1));
  }

  if(Lpml != 0) {
    std::vector< std::pair< int, int > > PmlEntities;
    gmsh::model::geo::extrude({std::make_pair(1, line)}, Lpml, 0., 0., PmlEntities, {static_cast< int >((Lpml) / lc)}, std::vector< double >(), true);

    gmsh::model::addPhysicalGroup(1, {PmlEntities[0].second}, 3);
    gmsh::model::setPhysicalName(1, 3, "gammaPml");
    gmsh::model::addPhysicalGroup(2, {PmlEntities[1].second}, omegas.size() + 1);
    gmsh::model::setPhysicalName(2, omegas.size() + 1, "omega_" + std::to_string(omegas.size()) );

    gmsh::model::addPhysicalGroup(1, {PmlEntities[2].second,PmlEntities[3].second}, 100 + borders.size());
    gmsh::model::setPhysicalName(1, 100 + borders.size(), "borderPml");
  }

  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate();
  gmsh::model::mesh::setOrder(MeshOrder);
  gmsh::write("m.msh");
}

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);
  double pi = 3.14159265358979323846264338327950288;
  const std::complex< double > im(0., 1.);
  int nDom = 8;
  gmshDdm.userDefinedParameter(nDom, "nDom");

  // duct geometry parameters
  double L = 1;
  double H = 0.5;

  // numerical parameters
  double MinPtsByWl = 12;
  gmshDdm.userDefinedParameter(MinPtsByWl, "MinPtsByWl");
  int FEMorder = 4;
  gmshDdm.userDefinedParameter(FEMorder, "FEMorder");
  std::string gauss = "Gauss"+std::to_string(2*FEMorder+1);

  // physical parameters
  double w = 30;
  gmshDdm.userDefinedParameter(w, "w");
  int mode = 2;
  std::string wallType = "Neumann";
  gmshDdm.userDefinedParameter(wallType, "wallType");
  ScalarFunction< std::complex< double > > Input_neumann, Input_dirichlet;
  Input_neumann = cos( (mode*pi/H) * y< std::complex<double> >() );
  Input_dirichlet = sin( (mode*pi/H) * y< std::complex<double> >() );

  // transmission conditions
  std::string Transmission = "Taylor2"; // Taylor0, Taylor2, Pade (padeOrder,alpha)
  gmshDdm.userDefinedParameter(Transmission, "Transmission");
  // init transmission operators
  ScalarFunction< std::complex< double > > Si, dS;
  double alpha=-pi/4.; // rotation branch-cut, in [0, -pi]
  gmshDdm.userDefinedParameter(alpha, "alpha");
  msg::info << " use " << Transmission << " as transmission condition with alpha = " << alpha << msg::endl;

  ScalarFunction< std::complex< double > > c0, rho0;
  msg::info << " - transverse piecewise speed of sound, constant density " << msg::endl;
  rho0 = 1.25-0.75*heaviside( y< std::complex<double> >() - H/2.);   
  c0 = 0.5+1.5*heaviside( y< std::complex<double> >() - H/2.);
  double minc0 = 0.5;
    
  // meshing
  const int Npml = 40;
  double lc = 2*pi * FEMorder / (w/minc0) / MinPtsByWl;
  const double Lpml = Npml*lc;
  msg::info << " - lc = " << lc << "" << msg::endl;
  msg::info << " - k0 max = " << w << "" << msg::endl;
  msg::info << " - k0 min = " << w << "" << msg::endl;
  waveguide(nDom, H, L, Lpml, lc, FEMorder);
  
  // Define domain
  Subdomain omega(nDom);
  Subdomain gammaInf(nDom);
  Subdomain gammaDir(nDom);
  Subdomain border(nDom+1);
  Interface sigma(nDom);

  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    omega(i) = Domain(2, i + 1);
    if(i == (static_cast< unsigned int >(nDom-1)) ) {
      gammaInf(i) = Domain(1, 2); // outgoing boundary
    }

    if(i == 0) {
      gammaDir(i) = Domain(1, 1); // ingoing boundary
    }
    border(i) = Domain(1, 100 + i);

    if(i != 0) {
      sigma(i, i - 1) = Domain(1, 200 + i - 1);
    }
    if(i != (static_cast< unsigned int >(nDom-1)) ) {
      sigma(i, i + 1) = Domain(1, 200 + i);
    }
  }
  if (Lpml != 0)
    omega(nDom-1) |= Domain(2, nDom+1);

  // Define topology
  std::vector< std::vector< unsigned int > > topology(nDom);
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    if(i != 0) {
      topology[i].push_back(i - 1);
    }
    if(i != (static_cast< unsigned int >(nDom-1)) ) {
      topology[i].push_back(i + 1);
    }
  }

  ScalarFunction< std::complex< double > > k0 = w/c0; // wavenumber
  // Compute reference solution with large PML
  msg::info << "Use a PML with N = " << (Lpml/lc) << " layers" << msg::endl;
  // Bermudez PML function
  double Sigma0 = 40.; // increasing Sigma0 makes DDM local error higher in the PML subdomains
  msg::info << "Sigma0 PML = " << Sigma0 << msg::endl;
  ScalarFunction< std::complex< double > > SigmaX = heaviside( x< std::complex< double > >() - L ) * Sigma0/( (L+Lpml) - x< std::complex< double > >() );
  ScalarFunction< std::complex< double > > gammaX = 1-(im/w)*SigmaX;
  ScalarFunction< std::complex< double > > gammaXinv = 1. / gammaX;
  // Create DDM formulation
  std::vector< FieldInterface< std::complex< double > > * > fieldBucket;
  gmshddm::problem::Formulation< std::complex< double > > formulation("Helmholtz", topology);

  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega | gammaDir | border | sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);

  // Tell to the formulation that g is field that have to be exchanged between subdomains
  formulation.addInterfaceField(g);
  if ( wallType == "Dirichlet" ) { // wall constraints for soft walls
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
      u(i).addConstraint(border(i), 0.);
    }
  }

  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    // weak formulation in the pml domain
    formulation(i).integral( (1./rho0) * vector< std::complex< double > >(gammaXinv, 0., 0.) * grad(dof(u(i))), vector< std::complex< double > >(1., 0., 0.) * grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral( (1./rho0) * vector< std::complex< double > >(0., gammaX, 0.) * grad(dof(u(i))), vector< std::complex< double > >(0., 1., 0.) * grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral(- (1./rho0) * gammaX * k0*k0 * dof(u(i)), tf(u(i)), omega(i), gauss);

    // input BC
      // input BC
    if(wallType == "Dirichlet"){
      formulation(i).integral(- formulation.physicalSource( Input_dirichlet ), tf(u(i)), gammaDir(i), gauss);
    }
    else{
      formulation(i).integral(- formulation.physicalSource( Input_neumann ), tf(u(i)), gammaDir(i), gauss);
    }

    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      // Artificial source
      formulation(i).integral(formulation.artificialSource( -g(j,i) ), tf(u(i)), sigma(i,j), gauss);
    }

    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      // Interface terms
      formulation(i,j).integral(dof(g(i,j)), tf(g(i,j)), sigma(i,j), gauss);
      formulation(i,j).integral(formulation.artificialSource( g(j,i) ), tf(g(i,j)), sigma(i,j), gauss);
    }

    // Transmission condition
    if ( Transmission == "Taylor0" ) {
      Si = im*k0*exp(im*alpha/2.);
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral((1./rho0) * Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i,j).integral(- (1./rho0) * (2.*Si) * u(i), tf(g(i,j)), sigma(i,j), gauss);
      }
    }
    else if ( Transmission == "Taylor2" ) {
      Si = im*k0*cos(im*alpha/2.);
      dS = im*exp(-im*alpha/2.0)/(2.0*k0);
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral((1./rho0) * Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i).integral(- (1./rho0) * dS * grad( dof(u(i)) ), grad( tf(u(i)) ), sigma(i,j), gauss);
        formulation(i,j).integral(- (1./rho0) * (2.*Si) * u(i), tf(g(i,j)), sigma(i,j), gauss);
        formulation(i,j).integral(+ (1./rho0) * (2.*dS) * grad( u(i) ), grad( tf(g(i,j)) ), sigma(i,j), gauss);
      }
    }
    else if ( Transmission == "Taylor2Y" ) {
        Si = im*w*cos(im*alpha/2.) + im*w*exp(-im*alpha/2.0)*( (1./(c0*c0)) - 1.) / (2.0);
        dS = im*exp(-im*alpha/2.0)/(2.0*w);
        for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
          const unsigned int j = topology[i][jj];
          formulation(i).integral((1./rho0) * Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
          formulation(i).integral(- (1./rho0) * dS * grad( dof(u(i)) ), grad( tf(u(i)) ), sigma(i,j), gauss);
          formulation(i,j).integral(- (1./rho0) * (2.*Si) * u(i), tf(g(i,j)), sigma(i,j), gauss);
          formulation(i,j).integral(+ (1./rho0) * (2.*dS) * grad( u(i) ), grad( tf(g(i,j)) ), sigma(i,j), gauss);
        }
    }
    else if ( Transmission.std::string::find("Pade") != std::string::npos) {
      int padeOrder = 8;
      gmshDdm.userDefinedParameter(padeOrder, "padeOrder");
      if ( i == 0 ) {
        msg::info << "Use " << padeOrder << " auxiliary fields "<< msg::endl;
      }

      ScalarFunction< std::complex< double > > k0P = k0;
      if ( Transmission == "PadeY" ) {
        k0P = w;
      }
      
      const double Np = 2. * padeOrder + 1.;
      const std::complex< double > exp1 = std::complex<double>(std::cos(alpha),std::sin(alpha));
      const std::complex< double > exp2 = std::complex<double>(std::cos(alpha/2.),std::sin(alpha/2.));
      const std::complex< double > coef = 2./Np;

      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral( (1./rho0) * im * k0P * exp2 * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        // interface unknowns
        formulation(i,j).integral(- 2. * (1./rho0) * (im * k0P * exp2) * u(i), tf(g(i,j)), sigma(i,j), gauss);

        // define Pade coefficients
        std::vector< std::complex< double > > c(padeOrder, 0.);
        for(int l = 0; l < padeOrder; ++l) {
          c[l] = std::tan((l + 1) * pi / Np);
          c[l] *= c[l];
        }

        // define the auxiliary fields
        std::vector< Field< std::complex< double >, Form::Form0 >* > phi;
        Field< std::complex< double >, Form::Form0 >* phis;
        for(int l = 0; l < padeOrder; ++l) {
          phis = new Field< std::complex< double >, Form::Form0 >("phi_" + std::to_string(l), sigma(i,j), FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
          if(wallType == "Dirichlet") { // impose Dirichlet constraints on the auxiliary fields
            phis->addConstraint(border(i),0.);
            phis->addConstraint(border(i),0.);
          }
          phi.push_back(phis);
          fieldBucket.push_back(phi.back());
        }

        // write the augmented weak form
        for(int l = 0; l < padeOrder; ++l) {
          // boundary integral terms relating the auxiliary fields
          formulation(i).integral((1./rho0) * im * k0P * exp2 * coef * c[l] * dof(*phi[l]), tf(u(i)), sigma(i,j), gauss);
          formulation(i).integral((1./rho0) * im * k0P * exp2 * coef * c[l] * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
          // coupling of the auxiliary equations
          formulation(i).integral((1./rho0) * grad(dof(*phi[l])), grad(tf(*phi[l])), sigma(i,j), gauss);
          if ( Transmission == "PadeY" ) {
            formulation(i).integral( (1./rho0) * (w*w)*( 1. - 1./(c0*c0) ) * dof(*phi[l]), tf(*phi[l]), sigma(i,j), gauss);
          }
          formulation(i).integral( -(1./rho0) * (k0P*k0P) * (exp1 * c[l] + 1.) * dof(*phi[l]), tf(*phi[l]), sigma(i,j), gauss);
          formulation(i).integral( -(1./rho0) * (k0P*k0P) * exp1 * (c[l] + 1.) * dof(u(i)), tf(*phi[l]), sigma(i,j), gauss);
        }

        // interfaces unknowns
        for(int l = 0; l < padeOrder; ++l) {
          formulation(i,j).integral(- 2. * (1./rho0) * (im * k0P * exp2) * coef * c[l] * (*phi[l]), tf(g(i,j)), sigma(i,j), gauss);
          formulation(i,j).integral(- 2. * (1./rho0) * (im * k0P * exp2) * coef * c[l] * u(i), tf(g(i,j)), sigma(i,j), gauss);
        }
      }
    }
    else {
      msg::error << "Transmission condition not available !" << msg::endl;
      exit(0);
    }
  }

  // Solve the DDM formulation
  formulation.pre();
  std::string solver = "gmres";
  double tol = 1e-6;
  int maxIt = 300;
  gmshDdm.userDefinedParameter(solver, "solver");
  gmshDdm.userDefinedParameter(tol, "tol");
  gmshDdm.userDefinedParameter(maxIt, "maxIt");
  formulation.solve(solver,tol,maxIt,true);

  // Compute FEM monodomain solution
  Domain omegaMono, borderMono;
  Domain gammaDirMono = Domain(1, 1);
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    omegaMono |= omega(i);
    borderMono |= border(i);
  }

  gmshfem::field::Field< std::complex< double >, Form::Form0 > uMono("uMono", omegaMono | borderMono | gammaDirMono, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  gmshfem::problem::Formulation< std::complex< double > > monodomain("HelmholtzMono");
  if(wallType == "Dirichlet") {
    uMono.addConstraint(borderMono, 0.);
  }

  monodomain.integral( (1./rho0) * vector< std::complex< double > >(gammaXinv, 0., 0.) * grad(dof(uMono)), vector< std::complex< double > >(1., 0., 0.) * grad(tf(uMono)), omegaMono, gauss);
  monodomain.integral( (1./rho0) * vector< std::complex< double > >(0., gammaX, 0.) * grad(dof(uMono)), vector< std::complex< double > >(0., 1., 0.) * grad(tf(uMono)), omegaMono, gauss);
  monodomain.integral(- (1./rho0) * gammaX * k0*k0 * dof(uMono), tf(uMono), omegaMono, gauss);
  // input BC
  if(wallType == "Dirichlet"){
    monodomain.integral(-Input_dirichlet, tf(uMono), gammaDirMono, gauss);
  }
  else{
    monodomain.integral(-Input_neumann, tf(uMono), gammaDirMono, gauss);
  }
  // solve
  monodomain.pre();
  monodomain.assemble();
  monodomain.solve();
  save(uMono,omegaMono,"uMono");
  //save(c0,omegaMono,"c0");
  //save(rho0,omegaMono,"rho0");

  // Monodomain L2 error
  double local_err;
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    save(+u(i),omega(i),"u_"+std::to_string(i));
    std::complex< double > denLocal = integrate(pow(abs( uMono ), 2), omega(i), gauss);
    std::complex< double > numLocal = integrate(pow(abs(uMono - u(i)), 2), omega(i), gauss);
    local_err = sqrt(numLocal.real() / denLocal.real());
    msg::info << "Local L2 error = " << local_err << " on subdomain " << i << msg::endl;
  }

  for(unsigned int i = 0; i < fieldBucket.size(); ++i) {
    delete fieldBucket[i];
  }

  return 0;
}
