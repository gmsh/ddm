# Simple domain decomposition in a straight waveguide for a variable coefficients Helmholtz problem 

Dependency(ies):
* GmshDDM : v 1.0.0
* GmshFEM : v 1.0.0
* Gmsh

## Problem description

The routine `main_layered.cpp` solves a variable coefficients Helmholtz propagation problem in a straight waveguide, governed by the operator
```math
\mathcal{H} = \nabla \cdot \left( \frac{1}{\rho_0} \nabla \right) + \frac{\omega^2}{\rho_0 c_0^2} 
```
where $`\rho_0(x,y)`$ and $`c_0(x,y)`$ may represent respectively the density and speed of sound of a mean flow in a flow acoustic context, or any useful spatially varying quantity that is governed by a such a PDE.   
The propagation occurs in the `x`-direction, while a homogenoeus Neumann or Dirichlet condition is imposed on the upper and lower walls.
For testing we use a layered profile oriented along the `y`-direction for $`\rho_0(y)`$ and $`c_0(y)`$. 
Different transmission conditions are tested and are compared to the mono-domain solution, that is the solution obtained without domain decomposition. A thick PML is set as outgoing boundary condition in order to damp all possible modes, including grazing modes close to the cut-on frequency.
By default a cosine function is used as input boundary condition.

## Installation and usage 
Simply run 

```
  mkdir build && cd build
  cmake ..
  make
  ./example [PARAM]
```
with `[PARAM]`:
* `-nDom [x]` where `[x]` is the number of subdomains. 8 subdomains are used by default.
* `-wallType [x]`, where `[x]` can be `Neumann` or `Dirichlet`
* `-FEMorder [x]` is the polynomial order of the finite element basis functions. Order 4 is the default.
* `-MinPtsByWl [x]` is the minimum number of dofs per wavelength. 12 is the default.
* `-w [x]` is the angular frequency, $`\omega > 0`$. $`\omega = 30`$ is used by default.
* `-Transmission [x]` is the choice of transmission condition, The available choices are `Taylor0`, `Taylor2`, `Taylor2Y`, `Pade` and `PadeY`. `Taylor2` is the default.
* `-alpha [x]` is the branch rotation angle for the transmission condition, $`\alpha \in [0, -\pi]`$. $`\alpha=-\pi/4`$ is the default.
* `-padeOrder [x]` specify the number of auxiliary function for the `Pade` condition.
* `-solver [x]` selects the type of iterative solver. `gmres` is the default, `jacobi` can also be used.
* `-tol [x]` specifies the tolerence of the iterative solver, `1e-6` is used by default.
* `-maxIt [x]` is the maximum number of iterations allowed, 200 iterations are allowed by default. 

For the transmission condition, `Taylor2` and `Pade` are based on the approximation of the usual square-root operator for Helmholtz problems $`\mathcal{S}_{k_0} = k_0 \sqrt{1+\Delta_{\Gamma}/k_0^2}, \; k_0 = \omega/c_0`$, while the conditions `Taylor2Y` and `PadeY` are based on the modified choice 
```math
\mathcal{S}_{\omega} = \omega \sqrt{1+\mathcal{Z}}, \mathcal{Z} = (c_0^{-2} - 1) + \nabla_{\Gamma}(\rho_0^{-1} \nabla_{\Gamma})/\omega^2
```

## Reference
> P. Marchner, H. Bériot, S. Le Bras, X. Antoine and C. Geuzaine. A domain decomposition solver for large scale time-harmonic flow acoustics problems. (2023), Preprint [HAL-04254633](https://hal.science/hal-04254633).

## Results reproducibility
The data from Figure 3.2, Section 3.3 can be reproduced thanks to the script `runTests.sh`.

## Note
Other kind of profiles are available for testing in the routines `main.cpp` and `main_Airy.cpp`. They refer to the PhD
> Marchner, Philippe. "Non-reflecting boundary conditions and domain decomposition methods for industrial flow acoustics." PhD thesis, Universités de Lorraine et Liège, 2022.
