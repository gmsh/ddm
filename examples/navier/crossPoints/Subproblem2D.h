#ifndef H_SUBPROBLEM2D
#define H_SUBPROBLEM2D

#include "SubproblemDomains.h"
#include "SubproblemParameters.h"

#include <gmshfem/Formulation.h>

namespace D2 {


  class Boundary
  {
   protected:
    const unsigned int _boundary;
    
   public:
    Boundary(const unsigned int boundary);
    virtual ~Boundary();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };

  class Corner
  {
   protected:
    const unsigned int _corner;
    Boundary *_bnd[2];
    
   public:
    Corner(const unsigned int corner, Boundary *first, Boundary *second);
    virtual ~Corner();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };

  class Subproblem
  {
   private:
    const SubproblemDomains _domains;
    const SubproblemParameters _parameters;
    gmshfem::problem::Formulation< std::complex< double > > &_formulation;
    const std::string _fieldName;
    std::vector< Boundary * > _boundary;
    std::vector< Corner * > _corner;
    
    void _parseParameters(std::string &method, std::vector< std::string > &parameters, const std::string &str) const;
    
   public:
    Subproblem(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters, const std::string &E, const std::string &N, const std::string &W, const std::string &S, const std::vector< unsigned int > activatedCrossPoints);
    ~Subproblem();
    
    void writeFormulation();
    
    Boundary *getBoundary(const unsigned int b) const;
    Corner *getCorner(const unsigned int c) const;
  };




  class Sommerfeld : public Boundary
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _v;
    
   public:
    Sommerfeld(const unsigned int boundary);
    ~Sommerfeld();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class Pml : public Boundary
  {
   protected:
    const double _size;
    const std::string _type;
    
   public:
    Pml(const unsigned int boundary, const double pmlSize, const std::string &type);
    virtual ~Pml();
    
    virtual double getSize() const;
    virtual gmshfem::function::TensorFunction< std::complex< double >, 4 > pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double >, 4 > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E, gmshfem::domain::Domain &bnd, const gmshfem::function::TensorFunction< std::complex< double >, 4 > &C, gmshfem::function::ScalarFunction< std::complex< double > > &kappaP, gmshfem::function::ScalarFunction< std::complex< double > > &kappaS) const;
    virtual void pmlCoefficientsDecoupled(gmshfem::function::TensorFunction< std::complex< double > > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E, gmshfem::domain::Domain &bnd, gmshfem::function::ScalarFunction< std::complex< double > > &kappaP, gmshfem::function::ScalarFunction< std::complex< double > > &kappaS) const;
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
  };

  class PmlContinuous : public Pml // with Lagrange multiplier
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _uPml;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _v;
    
   public:
    PmlContinuous(const unsigned int boundary, const double pmlSize, const std::string &type);
    ~PmlContinuous();
            
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousSymmetric : public Pml // with Lagrange multiplier
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _uPml;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _uSymPml;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _v;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vSym;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > _symX, _symY;
    
   public:
    PmlContinuousSymmetric(const unsigned int boundary, const double pmlSize, const std::string &type);
    ~PmlContinuousSymmetric();
            
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getUSymPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getVSym() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlDecoupled : public Pml
  {
   public:
    PmlDecoupled(const unsigned int boundary, const double pmlSize, const std::string type);
    virtual ~PmlDecoupled();

    virtual void pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double > > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E, gmshfem::domain::Domain &bnd, gmshfem::function::ScalarFunction< std::complex< double > > &kappaP, gmshfem::function::ScalarFunction< std::complex< double > > &kappaS) const;
  };
  
  class PmlContinuousDecoupled : public PmlDecoupled // with Lagrange multiplier
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uxPml;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uyPml;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _v;
    
   public:
    PmlContinuousDecoupled(const unsigned int boundary, const double pmlSize, const std::string &type);
    ~PmlContinuousDecoupled();
          
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUxPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUyPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  



  class PmlContinuous_PmlContinuous : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _uPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vCorner;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > _sym;
    
   public:
    PmlContinuous_PmlContinuous(const unsigned int corner, PmlContinuous *first, PmlContinuous *second);
    ~PmlContinuous_PmlContinuous();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV(const unsigned int i) const;
    
    PmlContinuous *firstBoundary() const;
    PmlContinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuous_Sommerfeld : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC;
    
   public:
    PmlContinuous_Sommerfeld(const unsigned int corner, PmlContinuous *first, Sommerfeld *second);
    ~PmlContinuous_Sommerfeld();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV() const;
    
    PmlContinuous *firstBoundary() const;
    Sommerfeld *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class Sommerfeld_PmlContinuous : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC;
    
   public:
    Sommerfeld_PmlContinuous(const unsigned int corner, Sommerfeld *first, PmlContinuous *second);
    ~Sommerfeld_PmlContinuous();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV() const;
    
    Sommerfeld *firstBoundary() const;
    PmlContinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousSymmetric_PmlContinuousSymmetric : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _uPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _uSymPmlCorner[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _uSymSymPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vSymC[2][2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vSymSymC[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vSymCorner[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vSymSymCorner;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > _symX, _symY;
    
   public:
    PmlContinuousSymmetric_PmlContinuousSymmetric(const unsigned int corner, PmlContinuousSymmetric *first, PmlContinuousSymmetric *second);
    ~PmlContinuousSymmetric_PmlContinuousSymmetric();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV(const unsigned int i) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getVSym(const unsigned int i, const unsigned int j) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getVSymSym(const unsigned int i) const;
    
    PmlContinuousSymmetric *firstBoundary() const;
    PmlContinuousSymmetric *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousSymmetric_Sommerfeld : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vSymC;
    
   public:
    PmlContinuousSymmetric_Sommerfeld(const unsigned int corner, PmlContinuousSymmetric *first, Sommerfeld *second);
    ~PmlContinuousSymmetric_Sommerfeld();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getVSym() const;
    
    PmlContinuousSymmetric *firstBoundary() const;
    Sommerfeld *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };

  class Sommerfeld_PmlContinuousSymmetric : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vSymC;
    
   public:
    Sommerfeld_PmlContinuousSymmetric(const unsigned int corner, Sommerfeld *first, PmlContinuousSymmetric *second);
    ~Sommerfeld_PmlContinuousSymmetric();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getVSym() const;
    
    Sommerfeld *firstBoundary() const;
    PmlContinuousSymmetric *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousDecoupled_PmlContinuousDecoupled : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uxPmlCorner;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uyPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vCorner;
    
   public:
    PmlContinuousDecoupled_PmlContinuousDecoupled(const unsigned int corner, PmlContinuousDecoupled *first, PmlContinuousDecoupled *second);
    ~PmlContinuousDecoupled_PmlContinuousDecoupled();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUxPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUyPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV(const unsigned int i) const;
    
    PmlContinuousDecoupled *firstBoundary() const;
    PmlContinuousDecoupled *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuous_PmlContinuousDecoupled : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uxPmlCorner;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uyPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vCorner;
    
   public:
    PmlContinuous_PmlContinuousDecoupled(const unsigned int corner, PmlContinuous *first, PmlContinuousDecoupled *second);
    ~PmlContinuous_PmlContinuousDecoupled();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUxPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUyPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV(const unsigned int i) const;
    
    PmlContinuous *firstBoundary() const;
    PmlContinuousDecoupled *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousDecoupled_PmlContinuous : public Corner
  {
   protected:
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uxPmlCorner;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * _uyPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vC[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * _vCorner;
    
   public:
    PmlContinuousDecoupled_PmlContinuous(const unsigned int corner, PmlContinuousDecoupled *first, PmlContinuous *second);
    ~PmlContinuousDecoupled_PmlContinuous();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUxPml() const;
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* getUyPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* getV(const unsigned int i) const;
    
    PmlContinuousDecoupled *firstBoundary() const;
    PmlContinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };


}

#endif // H_SUBPROBLEM2D
