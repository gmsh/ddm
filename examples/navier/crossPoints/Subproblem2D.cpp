#include "Subproblem2D.h"

using gmshfem::equation::dof;
using gmshfem::equation::tf;
using gmshfem::function::operator-;
using gmshfem::function::operator+;
using gmshfem::function::operator*;

namespace D2 {


  // **********************************
  // Boundary
  // **********************************

  Boundary::Boundary(const unsigned int boundary) : _boundary(boundary)
  {
  }

  Boundary::~Boundary()
  {
  }

  std::string Boundary::orientation() const
  {
    switch (_boundary) {
      case 0:
        return "E";
        break;
      case 1:
        return "N";
        break;
      case 2:
        return "W";
        break;
      case 3:
        return "S";
        break;
      default:
        break;
    }
    return "null";
  }

  // **********************************
  // Corner
  // **********************************

  Corner::Corner(const unsigned int corner, Boundary *first, Boundary *second) : _corner(corner), _bnd {first, second}
  {
  }

  Corner::~Corner()
  {
  }

  std::string Corner::orientation() const
  {
    switch (_corner) {
      case 0:
        return "EN";
        break;
      case 1:
        return "NW";
        break;
      case 2:
        return "WS";
        break;
      case 3:
        return "SE";
        break;
      default:
        break;
    }
    return "null";
  }

  // **********************************
  // Subproblem
  // **********************************

  void Subproblem::_parseParameters(std::string &method, std::vector< std::string > &parameters, const std::string &str) const
  {
    method.clear();
    parameters.clear();
    std::string *current = &method;
    for(unsigned int i = 0; i < str.size(); ++i) {
      if(str[i] == '_') {
        parameters.push_back(std::string());
        current = &parameters.back();
      }
      else {
        (*current) += str[i];
      }
    }
  }

  Subproblem::Subproblem(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters, const std::string &E, const std::string &N, const std::string &W, const std::string &S, const std::vector< unsigned int > activatedCrossPoints) : _domains(domains), _parameters(parameters), _formulation(formulation), _fieldName(fieldName), _boundary(4, nullptr), _corner(4, nullptr)
  {
    std::string type[4] = {E, N, W, S};
    std::string method[4];
    std::vector< std::string > bndParameters[4];
    for(unsigned int i = 0; i < 4; ++i) {
      _parseParameters(method[i], bndParameters[i], type[i]);
    }
    
    for(unsigned int b = 0; b < 4; ++b) {
      if(method[b] == "sommerfeld") { // str: sommerfeld
        _boundary[b] = new Sommerfeld(b);
      }
      else if(method[b] == "pmlContinuous") { // str: pmlContinuous_[N_pml]_[Type]
        const double N_pml = std::stof(bndParameters[b][0]);
        const std::string type  = bndParameters[b][1];
        _boundary[b] = new PmlContinuous(b, N_pml, type);
      }
      else if(method[b] == "pmlContinuousSymmetric") { // str: pmlContinuousSymmetric_[N_pml]_[Type]
        const double N_pml  = std::stof(bndParameters[b][0]);
        const std::string type  = bndParameters[b][1];
        _boundary[b] = new PmlContinuousSymmetric(b, N_pml, type);
      }
      else if(method[b] == "pmlContinuousDecoupled") { // str: pmlContinuousDecoupled_[N_pml]_[Type]
        const double N_pml  = std::stof(bndParameters[b][0]);
        const std::string type  = bndParameters[b][1];
        _boundary[b] = new PmlContinuousDecoupled(b, N_pml, type);
      }
      else {
        gmshfem::msg::error << "Unknown method '" << method[b] << "'" << gmshfem::msg::endl;
      }
    }
    
    for(unsigned int c = 0; c < 4; ++c) {
      auto it = std::find(activatedCrossPoints.begin(), activatedCrossPoints.end(), c);
      if(it == activatedCrossPoints.end()) {
        continue;
      }
      
      if(method[c] == "sommerfeld" && method[(c+1)%4] == "sommerfeld") {
      }
      else if(method[c] == "pmlContinuous" && method[(c+1)%4] == "pmlContinuous") {
        _corner[c] = new PmlContinuous_PmlContinuous(c, static_cast< PmlContinuous * >(_boundary[c]), static_cast< PmlContinuous * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlContinuousSymmetric" && method[(c+1)%4] == "pmlContinuousSymmetric") {
        _corner[c] = new PmlContinuousSymmetric_PmlContinuousSymmetric(c, static_cast< PmlContinuousSymmetric * >(_boundary[c]), static_cast< PmlContinuousSymmetric * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlContinuousDecoupled" && method[(c+1)%4] == "pmlContinuousDecoupled") {
        _corner[c] = new PmlContinuousDecoupled_PmlContinuousDecoupled(c, static_cast< PmlContinuousDecoupled * >(_boundary[c]), static_cast< PmlContinuousDecoupled * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlContinuous" && method[(c+1)%4] == "sommerfeld") {
        _corner[c] = new PmlContinuous_Sommerfeld(c, static_cast< PmlContinuous * >(_boundary[c]), static_cast< Sommerfeld * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "sommerfeld" && method[(c+1)%4] == "pmlContinuous") {
        _corner[c] = new Sommerfeld_PmlContinuous(c, static_cast< Sommerfeld * >(_boundary[c]), static_cast< PmlContinuous * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlContinuousSymmetric" && method[(c+1)%4] == "sommerfeld") {
        _corner[c] = new PmlContinuousSymmetric_Sommerfeld(c, static_cast< PmlContinuousSymmetric * >(_boundary[c]), static_cast< Sommerfeld * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "sommerfeld" && method[(c+1)%4] == "pmlContinuousSymmetric") {
        _corner[c] = new Sommerfeld_PmlContinuousSymmetric(c, static_cast< Sommerfeld * >(_boundary[c]), static_cast< PmlContinuousSymmetric * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlContinuous" && method[(c+1)%4] == "pmlContinuousDecoupled") {
        _corner[c] = new PmlContinuous_PmlContinuousDecoupled(c, static_cast< PmlContinuous * >(_boundary[c]), static_cast< PmlContinuousDecoupled * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlContinuousDecoupled" && method[(c+1)%4] == "pmlContinuous") {
        _corner[c] = new PmlContinuousDecoupled_PmlContinuous(c, static_cast< PmlContinuousDecoupled * >(_boundary[c]), static_cast< PmlContinuous * >(_boundary[(c+1)%4]));
      }
      else {
        gmshfem::msg::warning << "Any corner treatment between '" << method[c] << "' and '" << method[(c+1)%4] << "'" << gmshfem::msg::endl;
      }
    }
  }

  Subproblem::~Subproblem()
  {
    for(unsigned int c = 0; c < 4; ++c) {
      if(_corner[c] != nullptr) {
        delete _corner[c];
      }
    }
    for(unsigned int b = 0; b < 4; ++b) {
      if(_boundary[b] != nullptr) {
        delete _boundary[b];
      }
    }
  }

  void Subproblem::writeFormulation()
  {
    for(unsigned int b = 0; b < 4; ++b) {
      _boundary[b]->writeFormulation(_formulation, _fieldName, _domains, _parameters);
    }
    
    for(unsigned int c = 0; c < 4; ++c) {
      if(_corner[c] != nullptr) {
        _corner[c]->writeFormulation(_formulation, _domains, _parameters);
      }
    }
  }

  Boundary *Subproblem::getBoundary(const unsigned int b) const
  {
    return _boundary[b];
  }

  Corner *Subproblem::getCorner(const unsigned int c) const
  {
    return _corner[c];
  }

  // **********************************
  // Sommerfeld
  // **********************************

  Sommerfeld::Sommerfeld(const unsigned int boundary) : Boundary(boundary)
  {
  }

  Sommerfeld::~Sommerfeld()
  {
    delete _v;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* Sommerfeld::getV() const
  {
    return _v;
  }

  void Sommerfeld::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    
    _v = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *u = static_cast< gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * >(formulation.getField(fieldName));
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;
      
    const std::complex< double > im(0., 1.);
    
    formulation.integral(- dof(*_v), tf(*u), sigma, gauss);

    formulation.integral(dof(*_v), tf(*_v), sigma, gauss);
    formulation.integral((im/kappaP) * In * dof(*u), tf(*_v), sigma, gauss);
    formulation.integral((im/kappaS) * It * dof(*u), tf(*_v), sigma, gauss);
  }
  
  // **********************************
  // PML
  // **********************************

  Pml::Pml(const unsigned int boundary, const double pmlSize, const std::string &type) : Boundary(boundary), _size(pmlSize), _type(type)
  {
  }

  Pml::~Pml()
  {
  }

  double Pml::getSize() const
  {
    return _size;
  }

  gmshfem::function::TensorFunction< std::complex< double >, 4 > Pml::pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double >, 4 > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E,  gmshfem::domain::Domain &bnd, const gmshfem::function::TensorFunction< std::complex< double >, 4 > &C, gmshfem::function::ScalarFunction< std::complex< double > > &kappaP, gmshfem::function::ScalarFunction< std::complex< double > > &kappaS) const
  {
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod;
    const std::complex< double > im(0., 1.);

    gmshfem::function::ScalarFunction< std::complex< double > > distSigma;
    gmshfem::function::ScalarFunction< std::complex< double > > sigma;
    if(orientation() == "E" || orientation() == "W") {
      distSigma = abs(gmshfem::function::x< std::complex< double > >() - gmshfem::function::x< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
      }
    }
    else if(orientation() == "N" || orientation() == "S") {
      distSigma = abs(gmshfem::function::y< std::complex< double > >() - gmshfem::function::y< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
      }
    }
    
    if(_type == "hs") {
      sigma = 1. / (_size - distSigma) - 1./ (_size);
    }
    else if(_type == "h") {
      sigma = 1. / (_size - distSigma);
    }
    else if(_type == "q") {
      const double sigmaStar = 10.;
      sigma = sigmaStar * distSigma * distSigma / (_size * _size);
    }

    gmshfem::function::ScalarFunction< std::complex< double > > gamma = 1. + im * sigma;
    gmshfem::function::ScalarFunction< std::complex< double > > invGamma = 1./gamma;
    if(orientation() == "E" || orientation() == "W") {
      gmshfem::function::TensorFunction< std::complex< double > > C_1 = gmshfem::function::tensor< std::complex< double > >(invGamma, invGamma, invGamma,
                                                                                                                            1., 1., 1.,
                                                                                                                            1., 1., 1.);
      gmshfem::function::TensorFunction< std::complex< double > > C_2 = gmshfem::function::tensor< std::complex< double > >(1., 1., 1.,
                                                                                                                            gamma, gamma, gamma,
                                                                                                                            gamma, gamma, gamma);
      D = gmshfem::function::tensor< std::complex< double >, 4 >(C_1, C_1, C_1,
                                                                 C_2, C_2, C_2,
                                                                 C_2, C_2, C_2);
      E = gamma;
    }
    else if(orientation() == "N" || orientation() == "S") {
      gmshfem::function::TensorFunction< std::complex< double > > C_1 = gmshfem::function::tensor< std::complex< double > >(1., 1., 1.,
                                                                                                                            invGamma, invGamma, invGamma,
                                                                                                                            1., 1., 1.);
      gmshfem::function::TensorFunction< std::complex< double > > C_2 = gmshfem::function::tensor< std::complex< double > >(gamma, gamma, gamma,
                                                                                                                            1., 1., 1.,
                                                                                                                            gamma, gamma, gamma);

      D = gmshfem::function::tensor< std::complex< double >, 4 >(C_2, C_2, C_2,
                                                                 C_1, C_1, C_1,
                                                                 C_2, C_2, C_2);
      E = gamma;
    }
    
    return CMod;
  }
  
  void Pml::pmlCoefficientsDecoupled(gmshfem::function::TensorFunction< std::complex< double > > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E, gmshfem::domain::Domain &bnd, gmshfem::function::ScalarFunction< std::complex< double > > &kappaP, gmshfem::function::ScalarFunction< std::complex< double > > &kappaS) const
  {
    const std::complex< double > im(0., 1.);

    gmshfem::function::ScalarFunction< std::complex< double > > distSigma;
    gmshfem::function::ScalarFunction< std::complex< double > > sigma;
    if(orientation() == "E" || orientation() == "W") {
      distSigma = abs(gmshfem::function::x< std::complex< double > >() - gmshfem::function::x< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
      }
    }
    else if(orientation() == "N" || orientation() == "S") {
      distSigma = abs(gmshfem::function::y< std::complex< double > >() - gmshfem::function::y< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
      }
    }
    
    if(_type == "hs") {
      sigma = 1. / (_size - distSigma) - 1./ _size;
    }
    else if(_type == "h") {
      sigma = 1. / (_size - distSigma);
    }
    else if(_type == "q") {
      const double sigmaStar = 86.435; // N = 6 : 86.435; N = 1 : 186
      sigma = sigmaStar * distSigma * distSigma / (_size * _size);
    }
    
    gmshfem::function::ScalarFunction< std::complex< double > > kMod = 1. + im * sigma / kappaP;
    if(orientation() == "E" || orientation() == "W") {
      D = gmshfem::function::tensorDiag< std::complex< double > >(1./kMod, kMod, kMod);
      E = kMod;
    }
    else if(orientation() == "N" || orientation() == "S") {
      D = gmshfem::function::tensorDiag< std::complex< double > >(kMod, 1./kMod, kMod);
      E = kMod;
    }
  }

  // **********************************
  // PML continuous
  // **********************************

  PmlContinuous::PmlContinuous(const unsigned int boundary, const double pmlSize, const std::string &type) : Pml(boundary, pmlSize, type)
  {
  }

  PmlContinuous::~PmlContinuous()
  {
    delete _v;
    delete _uPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * PmlContinuous::getUPml() const
  {
    return _uPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * PmlContinuous::getV() const
  {
    return _v;
  }

  void PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    gmshfem::domain::Domain pml = domains.getPml(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    const double stab = parameters.getStab();
    
    _uPml = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("uPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _v = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *u = static_cast< gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * >(formulation.getField(fieldName));
    
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = pmlCoefficients(D, E, sigma, C, kappaP, kappaS);
            
    formulation.integral(- gmshfem::function::hadamard(CMod, D) * grad(dof(*_uPml)), grad(tf(*_uPml)), pml, gauss);
    formulation.integral(E * dof(*_uPml), tf(*_uPml), pml, gauss);
    
    formulation.integral(dof(*_v), tf(*_uPml), sigma, gauss);
    formulation.integral(- dof(*_v), tf(*u), sigma, gauss);

    formulation.integral(dof(*_uPml), tf(*_v), sigma, gauss);
    formulation.integral(- dof(*u), tf(*_v), sigma, gauss);
    
    // penalization
    // formulation.integral(1.e10 * stab * dof(*_v), tf(*_v), sigma, gauss);
  }
  
  // **********************************
  // PML continuous symmetric
  // **********************************
  
  PmlContinuousSymmetric::PmlContinuousSymmetric(const unsigned int boundary, const double pmlSize, const std::string &type) : Pml(boundary, pmlSize, type)
  {
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree4 >::Object tmpX, tmpY;
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(i == k && j == l) {
              tmpX(i,j)(k,l) = 1.;
              tmpY(i,j)(k,l) = 1.;
            }
            else {
              tmpX(i,j)(k,l) = 0.;
              tmpY(i,j)(k,l) = 0.;
            }
          }
        }
      }
    }
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(l == 0) {
              tmpX(i,j)(k,l) *= -1.; // symmetry of grad_x
            }
            else if(l == 1) {
              tmpY(i,j)(k,l) *= -1.; // symmetry of grad_y
            }
          }
        }
      }
    }
    _symX = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpX);
    _symY = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpY);
  }

  PmlContinuousSymmetric::~PmlContinuousSymmetric()
  {
    delete _v;
    delete _vSym;
    delete _uPml;
    delete _uSymPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * PmlContinuousSymmetric::getUPml() const
  {
    return _uPml;
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * PmlContinuousSymmetric::getUSymPml() const
  {
    return _uSymPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * PmlContinuousSymmetric::getV() const
  {
    return _v;
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * PmlContinuousSymmetric::getVSym() const
  {
    return _vSym;
  }

  void PmlContinuousSymmetric::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    gmshfem::domain::Domain pml = domains.getPml(_boundary);
    gmshfem::domain::Domain pmlInf = domains.getPmlInf(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    
    _uPml = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("uPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _uSymPml = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("uSymPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _v = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSym = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSym_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *u = static_cast< gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * >(formulation.getField(fieldName));
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;
    
    const std::complex< double > im(0., 1.);
    
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = pmlCoefficients(D, E, sigma, C, kappaP, kappaS);
    
    
    formulation.integral(- gmshfem::function::hadamard(CMod, D) * grad(dof(*_uPml)), grad(tf(*_uPml)), pml, gauss);
    formulation.integral(E * dof(*_uPml), tf(*_uPml), pml, gauss);
    
    formulation.integral(dof(*_v), tf(*_uPml), sigma, gauss);
    formulation.integral(- dof(*_v), tf(*u), sigma, gauss);

    formulation.integral(dof(*_uPml), tf(*_v), sigma, gauss);
    formulation.integral(- dof(*u), tf(*_v), sigma, gauss);
    
    formulation.integral((im/kappaP) * In * dof(*_uPml), tf(*_uPml), pmlInf, gauss);
    formulation.integral((im/kappaS) * It * dof(*_uPml), tf(*_uPml), pmlInf, gauss);
        
    // symmetric
    gmshfem::function::TensorFunction< std::complex< double >, 4 > &sym = ((_boundary == 0 || _boundary == 2) ? _symX : _symY);
    
    formulation.integral(- gmshfem::function::hadamard(CMod, D) * sym * grad(dof(*_uSymPml)), sym * grad(tf(*_uSymPml)), pml, gauss);
    formulation.integral(E * dof(*_uSymPml), tf(*_uSymPml), pml, gauss);
    
    formulation.integral(dof(*_vSym), tf(*_uSymPml), sigma, gauss);

    formulation.integral(dof(*_uSymPml), tf(*_vSym), sigma, gauss);
    formulation.integral(- dof(*u), tf(*_vSym), sigma, gauss);
    
    formulation.integral((im/kappaP) * In * dof(*_uSymPml), tf(*_uSymPml), pmlInf, gauss);
    formulation.integral((im/kappaS) * It * dof(*_uSymPml), tf(*_uSymPml), pmlInf, gauss);
  }
  
  // **********************************
  // PML decoupled
  // **********************************

  PmlDecoupled::PmlDecoupled(const unsigned int boundary, const double pmlSize, const std::string type) : Pml(boundary, pmlSize, type)
  {
  }

  PmlDecoupled::~PmlDecoupled()
  {
  }

  void PmlDecoupled::pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double > > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E, gmshfem::domain::Domain &bnd, gmshfem::function::ScalarFunction< std::complex< double > > &kappaP, gmshfem::function::ScalarFunction< std::complex< double > > &kappaS) const
  {
    pmlCoefficientsDecoupled(D, E, bnd, kappaP, kappaS);
  }
  
  // **********************************
  // PML continuous decoupled
  // **********************************

  PmlContinuousDecoupled::PmlContinuousDecoupled(const unsigned int boundary, const double pmlSize, const std::string &type) : PmlDecoupled(boundary, pmlSize, type)
  {
  }

  PmlContinuousDecoupled::~PmlContinuousDecoupled()
  {
    delete _v;
    delete _uxPml;
    delete _uyPml;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * PmlContinuousDecoupled::getUxPml() const
  {
    return _uxPml;
  }
  
  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * PmlContinuousDecoupled::getUyPml() const
  {
    return _uyPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * PmlContinuousDecoupled::getV() const
  {
    return _v;
  }

  void PmlContinuousDecoupled::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    gmshfem::domain::Domain pml = domains.getPml(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    const double stab = parameters.getStab();
    
    _uxPml = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uxPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _uyPml = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uyPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _v = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *u = static_cast< gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * >(formulation.getField(fieldName));
    
    gmshfem::function::TensorFunction< std::complex< double > > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    pmlCoefficients(D, E, sigma, kappaP, kappaS);
    
    // x wave
    if(_boundary == 0 || _boundary == 2) {
      formulation.integral(- 1./(kappaP * kappaP) * D * grad(dof(*_uxPml)), grad(tf(*_uxPml)), pml, gauss);
      formulation.integral(E * dof(*_uxPml), tf(*_uxPml), pml, gauss);
    }
    else if(_boundary == 1 || _boundary == 3) {
      formulation.integral(- 1./(kappaS * kappaS) * D * grad(dof(*_uxPml)), grad(tf(*_uxPml)), pml, gauss);
      formulation.integral(E * dof(*_uxPml), tf(*_uxPml), pml, gauss);
    }

    // y wave
    if(_boundary == 0 || _boundary == 2) {
      formulation.integral(- 1./(kappaS * kappaS) * D * grad(dof(*_uyPml)), grad(tf(*_uyPml)), pml, gauss);
      formulation.integral(E * dof(*_uyPml), tf(*_uyPml), pml, gauss);
    }
    else if(_boundary == 1 || _boundary == 3) {
      formulation.integral(- 1./(kappaP * kappaP) * D * grad(dof(*_uyPml)), grad(tf(*_uyPml)), pml, gauss);
      formulation.integral(E * dof(*_uyPml), tf(*_uyPml), pml, gauss);
    }

    formulation.integral(dof(*_v), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*_uxPml), sigma, gauss);
    formulation.integral(dof(*_v), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*_uyPml), sigma, gauss);
    formulation.integral(- dof(*_v), tf(*u), sigma, gauss);

    formulation.integral(gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*_uxPml), tf(*_v), sigma, gauss);
    formulation.integral(gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*_uyPml), tf(*_v), sigma, gauss);
    formulation.integral(- dof(*u), tf(*_v), sigma, gauss);

    // penalization
    // formulation.integral(stab * dof(*_v), tf(*_v), sigma, gauss);
  }
  
  // **********************************
  // PML continuous _ PML continuous
  // **********************************

  PmlContinuous_PmlContinuous::PmlContinuous_PmlContinuous(const unsigned int corner, PmlContinuous *first, PmlContinuous *second) : Corner(corner, first, second)
  {
  }

  PmlContinuous_PmlContinuous::~PmlContinuous_PmlContinuous()
  {
    delete _uPmlCorner;
    delete _vC[0];
    delete _vC[1];
    delete _vCorner;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuous_PmlContinuous::getUPml() const
  {
    return _uPmlCorner;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuous_PmlContinuous::getV(const unsigned int i) const
  {
    return _vC[i];
  }

  PmlContinuous *PmlContinuous_PmlContinuous::firstBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[0]);
  }

  PmlContinuous *PmlContinuous_PmlContinuous::secondBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[1]);
  }

  void PmlContinuous_PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;
    const double stab = parameters.getStab();

    _uPmlCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("uPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vC[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uPml[2];
    uPml[0] = static_cast< PmlContinuous * >(_bnd[0])->getUPml();
    uPml[1] = static_cast< PmlContinuous * >(_bnd[1])->getUPml();

    gmshfem::function::TensorFunction< std::complex< double >, 4 > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = static_cast< PmlContinuous * >(_bnd[0])->pmlCoefficients(D[0], E[0], corner, C, kappaP, kappaS);
    static_cast< PmlContinuous * >(_bnd[1])->pmlCoefficients(D[1], E[1], corner, C, fake, fake);

    formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * grad(dof(*_uPmlCorner)), grad(tf(*_uPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCorner, gauss);

    formulation.integral(dof(*_vC[0]), tf(*_uPmlCorner), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[0]), tf(*uPml[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- dof(*uPml[0]), tf(*_vC[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC[1]), tf(*_uPmlCorner), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vC[1]), tf(*uPml[1]), pmlBnd.second, gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- dof(*uPml[1]), tf(*_vC[1]), pmlBnd.second, gauss);

    // corner equation
    _vCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    formulation.integral(dof(*_vC[0]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vC[0]), corner, gauss);

    formulation.integral(-dof(*_vC[1]), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*_vC[1]), corner, gauss);

    formulation.integral(dof(*static_cast< PmlContinuous * >(_bnd[0])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[0])->getV()), corner, gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous * >(_bnd[1])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[1])->getV()), corner, gauss);
    
    // penalization
    // formulation.integral(stab * dof(*_vC[0]), tf(*_vC[0]), pmlBnd.first, gauss);
    // formulation.integral(stab * dof(*_vC[1]), tf(*_vC[1]), pmlBnd.second, gauss);
  }
  
  // **********************************
  // PML continuous _ Sommerfeld
  // **********************************

  PmlContinuous_Sommerfeld::PmlContinuous_Sommerfeld(const unsigned int corner, PmlContinuous *first, Sommerfeld *second) : Corner(corner, first, second)
  {
  }

  PmlContinuous_Sommerfeld::~PmlContinuous_Sommerfeld()
  {
    delete _vC;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuous_Sommerfeld::getV() const
  {
    return _vC;
  }

  PmlContinuous *PmlContinuous_Sommerfeld::firstBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[0]);
  }

  Sommerfeld *PmlContinuous_Sommerfeld::secondBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[1]);
  }

  void PmlContinuous_Sommerfeld::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();

    _vC = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uPml;
    uPml = static_cast< PmlContinuous * >(_bnd[0])->getUPml();
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;

    // To get E at the corner
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = static_cast< PmlContinuous * >(_bnd[0])->pmlCoefficients(D, E, corner, C, kappaP, kappaS);

    const std::complex< double > im(0., 1.);

    formulation.integral(- dof(*_vC), tf(*uPml), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC), tf(*_vC), pmlBnd.first, gauss);
    formulation.integral((im/(kappaP)) * In * E * dof(*uPml), tf(*_vC), pmlBnd.first, gauss);
    formulation.integral((im/(kappaS)) * It * E * dof(*uPml), tf(*_vC), pmlBnd.first, gauss);
  }

  // **********************************
  // Sommerfeld _ PML continuous
  // **********************************

  Sommerfeld_PmlContinuous::Sommerfeld_PmlContinuous(const unsigned int corner, Sommerfeld *first, PmlContinuous *second) : Corner(corner, first, second)
  {
  }

  Sommerfeld_PmlContinuous::~Sommerfeld_PmlContinuous()
  {
    delete _vC;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* Sommerfeld_PmlContinuous::getV() const
  {
    return _vC;
  }

  Sommerfeld *Sommerfeld_PmlContinuous::firstBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[0]);
  }

  PmlContinuous *Sommerfeld_PmlContinuous::secondBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[1]);
  }

  void Sommerfeld_PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();

    _vC = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uPml;
    uPml = static_cast< PmlContinuous * >(_bnd[1])->getUPml();
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;

    // To get E at the corner
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = static_cast< PmlContinuous * >(_bnd[1])->pmlCoefficients(D, E, corner, C, kappaP, kappaS);

    const std::complex< double > im(0., 1.);
    
    formulation.integral(- dof(*_vC), tf(*uPml), pmlBnd.second, gauss);
    
    formulation.integral(dof(*_vC), tf(*_vC), pmlBnd.second, gauss);
    formulation.integral((im/(kappaP)) * In * E * dof(*uPml), tf(*_vC), pmlBnd.second, gauss);
    formulation.integral((im/(kappaS)) * It * E * dof(*uPml), tf(*_vC), pmlBnd.second, gauss);
  }
  
  // **********************************
  // PML continuous symmetric _ PML continuous symmetric
  // **********************************

  PmlContinuousSymmetric_PmlContinuousSymmetric::PmlContinuousSymmetric_PmlContinuousSymmetric(const unsigned int corner, PmlContinuousSymmetric *first, PmlContinuousSymmetric *second) : Corner(corner, first, second)
  {
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree4 >::Object tmpX, tmpY;
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(i == k && j == l) {
              tmpX(i,j)(k,l) = 1.;
              tmpY(i,j)(k,l) = 1.;
            }
            else {
              tmpX(i,j)(k,l) = 0.;
              tmpY(i,j)(k,l) = 0.;
            }
          }
        }
      }
    }
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(l == 0) {
              tmpX(i,j)(k,l) *= -1.; // symmetry of grad_x
            }
            else if(l == 1) {
              tmpY(i,j)(k,l) *= -1.; // symmetry of grad_y
            }
          }
        }
      }
    }
    _symX = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpX);
    _symY = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpY);
  }

  PmlContinuousSymmetric_PmlContinuousSymmetric::~PmlContinuousSymmetric_PmlContinuousSymmetric()
  {
    delete _uPmlCorner;
    delete _uSymPmlCorner[0];
    delete _uSymPmlCorner[1];
    delete _uSymSymPmlCorner;
    delete _vC[0];
    delete _vC[1];
    delete _vSymC[0][0];
    delete _vSymC[0][1];
    delete _vSymC[1][0];
    delete _vSymC[1][1];
    delete _vSymSymC[0];
    delete _vSymSymC[1];
    delete _vCorner;
    delete _vSymCorner[0];
    delete _vSymCorner[1];
    delete _vSymSymCorner;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getUPml() const
  {
    return _uPmlCorner;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getV(const unsigned int i) const
  {
    return _vC[i];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getVSym(const unsigned int i, const unsigned int j) const
  {
    return _vSymC[i][j];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getVSymSym(const unsigned int i) const
  {
    return _vSymSymC[i];
  }

  PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric::firstBoundary() const
  {
    return static_cast< PmlContinuousSymmetric * >(_bnd[0]);
  }

  PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric::secondBoundary() const
  {
    return static_cast< PmlContinuousSymmetric * >(_bnd[1]);
  }

  void PmlContinuousSymmetric_PmlContinuousSymmetric::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    gmshfem::domain::Domain pmlCornerInf = domains.getPmlCornerInf(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;
    const double stab = parameters.getStab();

    _uPmlCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("uPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    
    _uSymPmlCorner[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("uSymPmlCorner_" + orientation() + "_first", pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _uSymPmlCorner[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("uSymPmlCorner_" + orientation() + "_second", pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    
    _uSymSymPmlCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("uSymSymPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vC[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    _vSymC[0][0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymC_first_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSymC[0][1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymC_first_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    _vSymC[1][0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymC_second_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSymC[1][1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymC_second_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    _vSymSymC[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymSymC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSymSymC[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymSymC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uPml[2];
    uPml[0] = static_cast< PmlContinuousSymmetric * >(_bnd[0])->getUPml();
    uPml[1] = static_cast< PmlContinuousSymmetric * >(_bnd[1])->getUPml();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uSymPml[2];
    uSymPml[0] = static_cast< PmlContinuousSymmetric * >(_bnd[0])->getUSymPml();
    uSymPml[1] = static_cast< PmlContinuousSymmetric * >(_bnd[1])->getUSymPml();
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;
    
    const std::complex< double > im(0., 1.);

    gmshfem::function::TensorFunction< std::complex< double >, 4 > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = static_cast< PmlContinuousSymmetric * >(_bnd[0])->pmlCoefficients(D[0], E[0], corner, C, kappaP, kappaS);
    static_cast< PmlContinuousSymmetric * >(_bnd[1])->pmlCoefficients(D[1], E[1], corner, C, fake, fake);
    

    formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * grad(dof(*_uPmlCorner)), grad(tf(*_uPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCorner, gauss);

    formulation.integral(dof(*_vC[0]), tf(*_uPmlCorner), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[0]), tf(*uPml[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- dof(*uPml[0]), tf(*_vC[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC[1]), tf(*_uPmlCorner), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vC[1]), tf(*uPml[1]), pmlBnd.second, gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- dof(*uPml[1]), tf(*_vC[1]), pmlBnd.second, gauss);
    
    formulation.integral((im/kappaP) * In * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCornerInf, gauss);
    formulation.integral((im/kappaS) * It * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCornerInf, gauss);

    // corner equation
    _vCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vC[0]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vC[0]), corner, gauss);

    formulation.integral(-dof(*_vC[1]), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*_vC[1]), corner, gauss);

    formulation.integral(dof(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getV()), corner, gauss);

    formulation.integral(-dof(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getV()), corner, gauss);
    
    
    // symmetric bnd 0
    {
      gmshfem::function::TensorFunction< std::complex< double >, 4 > &sym = ((_corner == 1 || _corner == 3) ? _symX : _symY);
      
      formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * sym * grad(dof(*_uSymPmlCorner[0])), sym * grad(tf(*_uSymPmlCorner[0])), pmlCorner, gauss);
      formulation.integral(E[0] * E[1] * dof(*_uSymPmlCorner[0]), tf(*_uSymPmlCorner[0]), pmlCorner, gauss);

      formulation.integral(dof(*_vSymC[0][0]), tf(*_uSymPmlCorner[0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_uSymPmlCorner[0]), tf(*_vSymC[0][0]), pmlBnd.first, gauss);
      formulation.integral(- dof(*uPml[0]), tf(*_vSymC[0][0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_vSymC[0][1]), tf(*_uSymPmlCorner[0]), pmlBnd.second, gauss);
      formulation.integral(- dof(*_vSymC[0][1]), tf(*uSymPml[1]), pmlBnd.second, gauss);

      formulation.integral(dof(*_uSymPmlCorner[0]), tf(*_vSymC[0][1]), pmlBnd.second, gauss);
      formulation.integral(- dof(*uSymPml[1]), tf(*_vSymC[0][1]), pmlBnd.second, gauss);
      
      formulation.integral((im/kappaP) * In * dof(*_uSymPmlCorner[0]), tf(*_uSymPmlCorner[0]), pmlCornerInf, gauss);
      formulation.integral((im/kappaS) * It * dof(*_uSymPmlCorner[0]), tf(*_uSymPmlCorner[0]), pmlCornerInf, gauss);
      
      // corner equation
      _vSymCorner[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymCorner_" + orientation() + "_first", corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

      formulation.integral(dof(*_vSymC[0][0]), tf(*_vSymCorner[0]), corner, gauss);
      formulation.integral(dof(*_vSymCorner[0]), tf(*_vSymC[0][0]), corner, gauss);

      formulation.integral(-dof(*_vSymC[0][1]), tf(*_vSymCorner[0]), corner, gauss);
      formulation.integral(-dof(*_vSymCorner[0]), tf(*_vSymC[0][1]), corner, gauss);

      formulation.integral(dof(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getV()), tf(*_vSymCorner[0]), corner, gauss);
      formulation.integral(dof(*_vSymCorner[0]), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getV()), corner, gauss);

      formulation.integral(-dof(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getVSym()), tf(*_vSymCorner[0]), corner, gauss);
      formulation.integral(-dof(*_vSymCorner[0]), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getVSym()), corner, gauss);
    }
    
    
    // symmetric bnd 1
    {
      gmshfem::function::TensorFunction< std::complex< double >, 4 > &sym = ((_corner == 1 || _corner == 3) ? _symY : _symX);
      
      formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * sym * grad(dof(*_uSymPmlCorner[1])), sym * grad(tf(*_uSymPmlCorner[1])), pmlCorner, gauss);
      formulation.integral(E[0] * E[1] * dof(*_uSymPmlCorner[1]), tf(*_uSymPmlCorner[1]), pmlCorner, gauss);

      formulation.integral(dof(*_vSymC[1][0]), tf(*_uSymPmlCorner[1]), pmlBnd.first, gauss);
      formulation.integral(- dof(*_vSymC[1][0]), tf(*uSymPml[0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_uSymPmlCorner[1]), tf(*_vSymC[1][0]), pmlBnd.first, gauss);
      formulation.integral(- dof(*uSymPml[0]), tf(*_vSymC[1][0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_vSymC[1][1]), tf(*_uSymPmlCorner[1]), pmlBnd.second, gauss);

      formulation.integral(dof(*_uSymPmlCorner[1]), tf(*_vSymC[1][1]), pmlBnd.second, gauss);
      formulation.integral(- dof(*uPml[1]), tf(*_vSymC[1][1]), pmlBnd.second, gauss);
      
      formulation.integral((im/kappaP) * In * dof(*_uSymPmlCorner[1]), tf(*_uSymPmlCorner[1]), pmlCornerInf, gauss);
      formulation.integral((im/kappaS) * It * dof(*_uSymPmlCorner[1]), tf(*_uSymPmlCorner[1]), pmlCornerInf, gauss);
      
      // corner equation
      _vSymCorner[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymCorner_" + orientation() + "_second", corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

      formulation.integral(dof(*_vSymC[1][0]), tf(*_vSymCorner[1]), corner, gauss);
      formulation.integral(dof(*_vSymCorner[1]), tf(*_vSymC[1][0]), corner, gauss);

      formulation.integral(-dof(*_vSymC[1][1]), tf(*_vSymCorner[1]), corner, gauss);
      formulation.integral(-dof(*_vSymCorner[1]), tf(*_vSymC[1][1]), corner, gauss);

      formulation.integral(dof(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getVSym()), tf(*_vSymCorner[1]), corner, gauss);
      formulation.integral(dof(*_vSymCorner[1]), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getVSym()), corner, gauss);

      formulation.integral(-dof(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getV()), tf(*_vSymCorner[1]), corner, gauss);
      formulation.integral(-dof(*_vSymCorner[1]), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getV()), corner, gauss);
    }
    
    
    // symmetric bnd 0 & 1
    {
      formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * gmshfem::function::hadamard(_symX, _symY) * grad(dof(*_uSymSymPmlCorner)), gmshfem::function::hadamard(_symX, _symY) * grad(tf(*_uSymSymPmlCorner)), pmlCorner, gauss);
      formulation.integral(E[0] * E[1] * dof(*_uSymSymPmlCorner), tf(*_uSymSymPmlCorner), pmlCorner, gauss);
      
      formulation.integral(dof(*_vSymSymC[0]), tf(*_uSymSymPmlCorner), pmlBnd.first, gauss);

      formulation.integral(dof(*_uSymSymPmlCorner), tf(*_vSymSymC[0]), pmlBnd.first, gauss);
      formulation.integral(- dof(*uSymPml[0]), tf(*_vSymSymC[0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_vSymSymC[1]), tf(*_uSymSymPmlCorner), pmlBnd.second, gauss);
  
      formulation.integral(dof(*_uSymSymPmlCorner), tf(*_vSymSymC[1]), pmlBnd.second, gauss);
      formulation.integral(- dof(*uSymPml[1]), tf(*_vSymSymC[1]), pmlBnd.second, gauss);
      
      formulation.integral((im/kappaP) * In * dof(*_uSymSymPmlCorner), tf(*_uSymSymPmlCorner), pmlCornerInf, gauss);
      formulation.integral((im/kappaS) * It * dof(*_uSymSymPmlCorner), tf(*_uSymSymPmlCorner), pmlCornerInf, gauss);

      // corner equation
      _vSymSymCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymSymCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

      formulation.integral(dof(*_vSymSymC[0]), tf(*_vSymSymCorner), corner, gauss);
      formulation.integral(dof(*_vSymSymCorner), tf(*_vSymSymC[0]), corner, gauss);

      formulation.integral(-dof(*_vSymSymC[1]), tf(*_vSymSymCorner), corner, gauss);
      formulation.integral(-dof(*_vSymSymCorner), tf(*_vSymSymC[1]), corner, gauss);

      formulation.integral(dof(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getVSym()), tf(*_vSymSymCorner), corner, gauss);
      formulation.integral(dof(*_vSymSymCorner), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getVSym()), corner, gauss);

      formulation.integral(-dof(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getVSym()), tf(*_vSymSymCorner), corner, gauss);
      formulation.integral(-dof(*_vSymSymCorner), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getVSym()), corner, gauss);
    }
  }
  
  // **********************************
  // PML continuous symmetric _ Sommerfeld
  // **********************************

  PmlContinuousSymmetric_Sommerfeld::PmlContinuousSymmetric_Sommerfeld(const unsigned int corner, PmlContinuousSymmetric *first, Sommerfeld *second) : Corner(corner, first, second)
  {
  }

  PmlContinuousSymmetric_Sommerfeld::~PmlContinuousSymmetric_Sommerfeld()
  {
    delete _vC;
    delete _vSymC;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuousSymmetric_Sommerfeld::getV() const
  {
    return _vC;
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuousSymmetric_Sommerfeld::getVSym() const
  {
    return _vSymC;
  }

  PmlContinuousSymmetric *PmlContinuousSymmetric_Sommerfeld::firstBoundary() const
  {
    return static_cast< PmlContinuousSymmetric * >(_bnd[0]);
  }

  Sommerfeld *PmlContinuousSymmetric_Sommerfeld::secondBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[1]);
  }

  void PmlContinuousSymmetric_Sommerfeld::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();

    _vC = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSymC = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uPml;
    uPml = static_cast< PmlContinuousSymmetric * >(_bnd[0])->getUPml();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uSymPml;
    uSymPml = static_cast< PmlContinuousSymmetric * >(_bnd[0])->getUSymPml();
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;

    // To get E at the corner
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = static_cast< PmlContinuousSymmetric * >(_bnd[0])->pmlCoefficients(D, E, corner, C, kappaP, kappaS);

    const std::complex< double > im(0., 1.);

    formulation.integral(- dof(*_vC), tf(*uPml), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC), tf(*_vC), pmlBnd.first, gauss);
    formulation.integral((im/(kappaP)) * In * E * dof(*uPml), tf(*_vC), pmlBnd.first, gauss);
    formulation.integral((im/(kappaS)) * It * E * dof(*uPml), tf(*_vC), pmlBnd.first, gauss);
    
    // symmetric
    formulation.integral(- dof(*_vSymC), tf(*uSymPml), pmlBnd.first, gauss);

    formulation.integral(dof(*_vSymC), tf(*_vSymC), pmlBnd.first, gauss);
    formulation.integral((im/(kappaP)) * In * E * dof(*uSymPml), tf(*_vSymC), pmlBnd.first, gauss);
    formulation.integral((im/(kappaS)) * It * E * dof(*uSymPml), tf(*_vSymC), pmlBnd.first, gauss);
  }

  // **********************************
  // Sommerfeld _ PML continuous symmetric
  // **********************************

  Sommerfeld_PmlContinuousSymmetric::Sommerfeld_PmlContinuousSymmetric(const unsigned int corner, Sommerfeld *first, PmlContinuousSymmetric *second) : Corner(corner, first, second)
  {
  }

  Sommerfeld_PmlContinuousSymmetric::~Sommerfeld_PmlContinuousSymmetric()
  {
    delete _vC;
    delete _vSymC;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* Sommerfeld_PmlContinuousSymmetric::getV() const
  {
    return _vC;
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* Sommerfeld_PmlContinuousSymmetric::getVSym() const
  {
    return _vSymC;
  }

  Sommerfeld *Sommerfeld_PmlContinuousSymmetric::firstBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[0]);
  }

  PmlContinuousSymmetric *Sommerfeld_PmlContinuousSymmetric::secondBoundary() const
  {
    return static_cast< PmlContinuousSymmetric * >(_bnd[1]);
  }

  void Sommerfeld_PmlContinuousSymmetric::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();

    _vC = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSymC = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vSymC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uPml;
    uPml = static_cast< PmlContinuousSymmetric * >(_bnd[1])->getUPml();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uSymPml;
    uSymPml = static_cast< PmlContinuousSymmetric * >(_bnd[1])->getUSymPml();
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;

    // To get E at the corner
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = static_cast< PmlContinuousSymmetric * >(_bnd[1])->pmlCoefficients(D, E, corner, C, kappaP, kappaS);

    const std::complex< double > im(0., 1.);
    
    formulation.integral(- dof(*_vC), tf(*uPml), pmlBnd.second, gauss);
    
    formulation.integral(dof(*_vC), tf(*_vC), pmlBnd.second, gauss);
    formulation.integral((im/(kappaP)) * In * E * dof(*uPml), tf(*_vC), pmlBnd.second, gauss);
    formulation.integral((im/(kappaS)) * It * E * dof(*uPml), tf(*_vC), pmlBnd.second, gauss);
    
    // symmetric
    formulation.integral(- dof(*_vSymC), tf(*uSymPml), pmlBnd.second, gauss);

    formulation.integral(dof(*_vSymC), tf(*_vSymC), pmlBnd.second, gauss);
    formulation.integral((im/(kappaP)) * In * E * dof(*uSymPml), tf(*_vSymC), pmlBnd.second, gauss);
    formulation.integral((im/(kappaS)) * It * E * dof(*uSymPml), tf(*_vSymC), pmlBnd.second, gauss);
  }
  
  // **********************************
  // PML continuous decoupled _ PML continuous decoupled
  // **********************************

  PmlContinuousDecoupled_PmlContinuousDecoupled::PmlContinuousDecoupled_PmlContinuousDecoupled(const unsigned int corner, PmlContinuousDecoupled *first, PmlContinuousDecoupled *second) : Corner(corner, first, second)
  {
  }

  PmlContinuousDecoupled_PmlContinuousDecoupled::~PmlContinuousDecoupled_PmlContinuousDecoupled()
  {
    delete _uxPmlCorner;
    delete _uyPmlCorner;
    delete _vC[0];
    delete _vC[1];
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuousDecoupled_PmlContinuousDecoupled::getUxPml() const
  {
    return _uxPmlCorner;
  }
  
  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuousDecoupled_PmlContinuousDecoupled::getUyPml() const
  {
    return _uyPmlCorner;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuousDecoupled_PmlContinuousDecoupled::getV(const unsigned int i) const
  {
    return _vC[i];
  }

  PmlContinuousDecoupled *PmlContinuousDecoupled_PmlContinuousDecoupled::firstBoundary() const
  {
    return static_cast< PmlContinuousDecoupled * >(_bnd[0]);
  }

  PmlContinuousDecoupled *PmlContinuousDecoupled_PmlContinuousDecoupled::secondBoundary() const
  {
    return static_cast< PmlContinuousDecoupled * >(_bnd[1]);
  }

  void PmlContinuousDecoupled_PmlContinuousDecoupled::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;
    const double stab = parameters.getStab();
    
    _uxPmlCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uxPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _uyPmlCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uyPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vC[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uxPml[2];
    uxPml[0] = static_cast< PmlContinuousDecoupled * >(_bnd[0])->getUxPml();
    uxPml[1] = static_cast< PmlContinuousDecoupled * >(_bnd[1])->getUxPml();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uyPml[2];
    uyPml[0] = static_cast< PmlContinuousDecoupled * >(_bnd[0])->getUyPml();
    uyPml[1] = static_cast< PmlContinuousDecoupled * >(_bnd[1])->getUyPml();

    gmshfem::function::TensorFunction< std::complex< double > > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    static_cast< PmlContinuousDecoupled * >(_bnd[0])->pmlCoefficients(D[0], E[0], corner, kappaP, kappaS);
    static_cast< PmlContinuousDecoupled * >(_bnd[1])->pmlCoefficients(D[1], E[1], corner, fake, fake);
    
    // x wave
    formulation.integral(- 1./(kappaS * kappaS) * D[0] * D[1] * grad(dof(*_uxPmlCorner)), grad(tf(*_uxPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uxPmlCorner), tf(*_uxPmlCorner), pmlCorner, gauss);

    // y wave
    formulation.integral(- 1./(kappaS * kappaS) * D[0] * D[1] * grad(dof(*_uyPmlCorner)), grad(tf(*_uyPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uyPmlCorner), tf(*_uyPmlCorner), pmlCorner, gauss);

    formulation.integral(dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*_uxPmlCorner), pmlBnd.first, gauss);
    formulation.integral(dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*_uyPmlCorner), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*uxPml[0]), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*uyPml[0]), pmlBnd.first, gauss);

    formulation.integral(gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*_uxPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*_uyPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*uxPml[0]), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*uyPml[0]), tf(*_vC[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*_uxPmlCorner), pmlBnd.second, gauss);
    formulation.integral(dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*_uyPmlCorner), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*uxPml[1]), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*uyPml[1]), pmlBnd.second, gauss);

    formulation.integral(gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*_uxPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*_uyPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*uxPml[1]), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*uyPml[1]), tf(*_vC[1]), pmlBnd.second, gauss);

    // corner equation
    _vCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vC[0]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vC[0]), corner, gauss);

    formulation.integral(-dof(*_vC[1]), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*_vC[1]), corner, gauss);

    formulation.integral(dof(*static_cast< PmlContinuous * >(_bnd[0])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[0])->getV()), corner, gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous * >(_bnd[1])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[1])->getV()), corner, gauss);

    // penalization
    // formulation.integral(stab * dof(*_vC[0]), tf(*_vC[0]), pmlBnd.first, gauss);
    // formulation.integral(stab * dof(*_vC[1]), tf(*_vC[1]), pmlBnd.second, gauss);
  }
  
  // **********************************
  // PML continuous _ PML continuous decoupled
  // **********************************

  PmlContinuous_PmlContinuousDecoupled::PmlContinuous_PmlContinuousDecoupled(const unsigned int corner, PmlContinuous *first, PmlContinuousDecoupled *second) : Corner(corner, first, second)
  {
  }

  PmlContinuous_PmlContinuousDecoupled::~PmlContinuous_PmlContinuousDecoupled()
  {
    delete _uxPmlCorner;
    delete _uyPmlCorner;
    delete _vC[0];
    delete _vC[1];
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuous_PmlContinuousDecoupled::getUxPml() const
  {
    return _uxPmlCorner;
  }
  
  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuous_PmlContinuousDecoupled::getUyPml() const
  {
    return _uyPmlCorner;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuous_PmlContinuousDecoupled::getV(const unsigned int i) const
  {
    return _vC[i];
  }

  PmlContinuous *PmlContinuous_PmlContinuousDecoupled::firstBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[0]);
  }

  PmlContinuousDecoupled *PmlContinuous_PmlContinuousDecoupled::secondBoundary() const
  {
    return static_cast< PmlContinuousDecoupled * >(_bnd[1]);
  }

  void PmlContinuous_PmlContinuousDecoupled::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;
    const double stab = parameters.getStab();
    
    _uxPmlCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uxPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _uyPmlCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uyPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vC[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uPml;
    uPml = static_cast< PmlContinuous * >(_bnd[0])->getUPml();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uxPml;
    uxPml = static_cast< PmlContinuousDecoupled * >(_bnd[1])->getUxPml();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uyPml;
    uyPml = static_cast< PmlContinuousDecoupled * >(_bnd[1])->getUyPml();

    gmshfem::function::TensorFunction< std::complex< double > > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    static_cast< PmlContinuous * >(_bnd[0])->pmlCoefficientsDecoupled(D[0], E[0], corner, kappaP, kappaS);
    static_cast< PmlContinuousDecoupled * >(_bnd[1])->pmlCoefficients(D[1], E[1], corner, fake, fake);
    
    // x wave
    formulation.integral(- 1./(kappaS * kappaS) * D[0] * D[1] * grad(dof(*_uxPmlCorner)), grad(tf(*_uxPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uxPmlCorner), tf(*_uxPmlCorner), pmlCorner, gauss);

    // y wave
    formulation.integral(- 1./(kappaS * kappaS) * D[0] * D[1] * grad(dof(*_uyPmlCorner)), grad(tf(*_uyPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uyPmlCorner), tf(*_uyPmlCorner), pmlCorner, gauss);

    formulation.integral(dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*_uxPmlCorner), pmlBnd.first, gauss);
    formulation.integral(dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*_uyPmlCorner), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[0]), tf(*uPml), pmlBnd.first, gauss);

    formulation.integral(gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*_uxPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*_uyPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- dof(*uPml), tf(*_vC[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*_uxPmlCorner), pmlBnd.second, gauss);
    formulation.integral(dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*_uyPmlCorner), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*uxPml), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*uyPml), pmlBnd.second, gauss);

    formulation.integral(gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*_uxPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*_uyPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*uxPml), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*uyPml), tf(*_vC[1]), pmlBnd.second, gauss);

    // corner equation
    _vCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vC[0]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vC[0]), corner, gauss);

    formulation.integral(-dof(*_vC[1]), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*_vC[1]), corner, gauss);

    formulation.integral(dof(*static_cast< PmlContinuous * >(_bnd[0])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[0])->getV()), corner, gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous * >(_bnd[1])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[1])->getV()), corner, gauss);

    // penalization
    // formulation.integral(stab * dof(*_vC[0]), tf(*_vC[0]), pmlBnd.first, gauss);
    // formulation.integral(stab * dof(*_vC[1]), tf(*_vC[1]), pmlBnd.second, gauss);
  }
  
  // **********************************
  // PML continuous decoupled _ PML continuous
  // **********************************

  PmlContinuousDecoupled_PmlContinuous::PmlContinuousDecoupled_PmlContinuous(const unsigned int corner, PmlContinuousDecoupled *first, PmlContinuous *second) : Corner(corner, first, second)
  {
  }

  PmlContinuousDecoupled_PmlContinuous::~PmlContinuousDecoupled_PmlContinuous()
  {
    delete _uxPmlCorner;
    delete _uyPmlCorner;
    delete _vC[0];
    delete _vC[1];
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuousDecoupled_PmlContinuous::getUxPml() const
  {
    return _uxPmlCorner;
  }
  
  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuousDecoupled_PmlContinuous::getUyPml() const
  {
    return _uyPmlCorner;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >* PmlContinuousDecoupled_PmlContinuous::getV(const unsigned int i) const
  {
    return _vC[i];
  }

  PmlContinuousDecoupled *PmlContinuousDecoupled_PmlContinuous::firstBoundary() const
  {
    return static_cast< PmlContinuousDecoupled * >(_bnd[0]);
  }

  PmlContinuous *PmlContinuousDecoupled_PmlContinuous::secondBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[1]);
  }

  void PmlContinuousDecoupled_PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;
    const double stab = parameters.getStab();
    
    _uxPmlCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uxPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _uyPmlCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uyPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vC[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *uPml;
    uPml = static_cast< PmlContinuous * >(_bnd[1])->getUPml();

    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uxPml;
    uxPml = static_cast< PmlContinuousDecoupled * >(_bnd[0])->getUxPml();
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uyPml;
    uyPml = static_cast< PmlContinuousDecoupled * >(_bnd[0])->getUyPml();

    gmshfem::function::TensorFunction< std::complex< double > > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    static_cast< PmlContinuousDecoupled * >(_bnd[0])->pmlCoefficients(D[0], E[0], corner, kappaP, kappaS);
    static_cast< PmlContinuous * >(_bnd[1])->pmlCoefficientsDecoupled(D[1], E[1], corner, fake, fake);
    
    // x wave
    formulation.integral(- 1./(kappaS * kappaS) * D[0] * D[1] * grad(dof(*_uxPmlCorner)), grad(tf(*_uxPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uxPmlCorner), tf(*_uxPmlCorner), pmlCorner, gauss);

    // y wave
    formulation.integral(- 1./(kappaS * kappaS) * D[0] * D[1] * grad(dof(*_uyPmlCorner)), grad(tf(*_uyPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uyPmlCorner), tf(*_uyPmlCorner), pmlCorner, gauss);

    formulation.integral(dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*_uxPmlCorner), pmlBnd.first, gauss);
    formulation.integral(dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*_uyPmlCorner), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*uxPml), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[0]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*uyPml), pmlBnd.first, gauss);

    formulation.integral(gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*_uxPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*_uyPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*uxPml), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*uyPml), tf(*_vC[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * tf(*_uxPmlCorner), pmlBnd.second, gauss);
    formulation.integral(dof(*_vC[1]), gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * tf(*_uyPmlCorner), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vC[1]), tf(*uPml), pmlBnd.second, gauss);

    formulation.integral(gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * dof(*_uxPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * dof(*_uyPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- dof(*uPml), tf(*_vC[1]), pmlBnd.second, gauss);

    // corner equation
    _vCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vC[0]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vC[0]), corner, gauss);

    formulation.integral(-dof(*_vC[1]), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*_vC[1]), corner, gauss);

    formulation.integral(dof(*static_cast< PmlContinuous * >(_bnd[0])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[0])->getV()), corner, gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous * >(_bnd[1])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[1])->getV()), corner, gauss);

    // penalization
    // formulation.integral(stab * dof(*_vC[0]), tf(*_vC[0]), pmlBnd.first, gauss);
    // formulation.integral(stab * dof(*_vC[1]), tf(*_vC[1]), pmlBnd.second, gauss);
  }


}
