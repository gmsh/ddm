#ifndef H_SUBPROBLEMPARAMETERS
#define H_SUBPROBLEMPARAMETERS

#include <gmshfem/Function.h>

class SubproblemParameters
{
 private:
  std::string _gauss;
  gmshfem::function::ScalarFunction< std::complex< double > > _kappaP;
  gmshfem::function::ScalarFunction< std::complex< double > > _kappaS;
  gmshfem::function::TensorFunction< std::complex< double >, 4 > _C;
  unsigned int _neumannOrder;
  unsigned int _fieldOrder;
  double _stab;
  
 public:
  SubproblemParameters();
  ~SubproblemParameters();
  
  void setGauss(const std::string &gauss);
  std::string getGauss() const;
  
  void setKappaP(const gmshfem::function::ScalarFunction< std::complex< double > > &kappa);
  gmshfem::function::ScalarFunction< std::complex< double > > getKappaP() const;
  
  void setKappaS(const gmshfem::function::ScalarFunction< std::complex< double > > &kappa);
  gmshfem::function::ScalarFunction< std::complex< double > > getKappaS() const;
  
  void setC(const gmshfem::function::TensorFunction< std::complex< double >, 4 > &C);
  gmshfem::function::TensorFunction< std::complex< double >, 4 > getC() const;
  
  void setNeumannOrder(const unsigned int neumannOrder);
  unsigned int getNeumannOrder() const;
  
  void setFieldOrder(const unsigned int fieldOrder);
  unsigned int getFieldOrder() const;
  
  void setStab(const double stab);
  double getStab() const;
};

#endif // H_SUBPROBLEMPARAMETERS
