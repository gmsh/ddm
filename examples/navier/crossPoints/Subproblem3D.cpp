#include "Subproblem3D.h"

using gmshfem::equation::dof;
using gmshfem::equation::tf;
using gmshfem::function::operator-;
using gmshfem::function::operator+;
using gmshfem::function::operator*;

namespace D3 {


  // **********************************
  // Boundary
  // **********************************

  Boundary::Boundary(const unsigned int boundary) : _boundary(boundary)
  {
  }

  Boundary::~Boundary()
  {
  }

  std::string Boundary::orientation() const
  {
    switch (_boundary) {
      case 0:
        return "E";
        break;
      case 1:
        return "N";
        break;
      case 2:
        return "W";
        break;
      case 3:
        return "S";
        break;
      case 4:
        return "D";
        break;
      case 5:
        return "U";
        break;
      default:
        break;
    }
    return "null";
  }
  
  // **********************************
  // Edge
  // **********************************

  Edge::Edge(const unsigned int edge, Boundary *first, Boundary *second) : _edge(edge), _bnd {first, second}
  {
  }

  Edge::~Edge()
  {
  }

  std::string Edge::orientation() const
  {
    switch (_edge) {
      case 0:
        return "ED";
        break;
      case 1:
        return "ND";
        break;
      case 2:
        return "WD";
        break;
      case 3:
        return "SD";
        break;
        
      case 4:
        return "EN";
        break;
      case 5:
        return "NW";
        break;
      case 6:
        return "WS";
        break;
      case 7:
        return "SE";
        break;
        
      case 8:
        return "EU";
        break;
      case 9:
        return "NU";
        break;
      case 10:
        return "WU";
        break;
      case 11:
        return "SU";
        break;
      default:
        break;
    }
    return "null";
  }

  // **********************************
  // Corner
  // **********************************

  Corner::Corner(const unsigned int corner, Edge *first, Edge *second, Edge *third) : _corner(corner), _bnd {first, second, third}
  {
  }

  Corner::~Corner()
  {
  }

  std::string Corner::orientation() const
  {
    switch (_corner) {
      case 0:
        return "END";
        break;
      case 1:
        return "NWD";
        break;
      case 2:
        return "WSD";
        break;
      case 3:
        return "SED";
        break;
        
      case 4:
        return "ENU";
        break;
      case 5:
        return "NWU";
        break;
      case 6:
        return "WSU";
        break;
      case 7:
        return "SEU";
        break;
      default:
        break;
    }
    return "null";
  }

  // **********************************
  // Subproblem
  // **********************************

  void Subproblem::_parseParameters(std::string &method, std::vector< std::string > &parameters, const std::string &str) const
  {
    method.clear();
    parameters.clear();
    std::string *current = &method;
    for(unsigned int i = 0; i < str.size(); ++i) {
      if(str[i] == '_') {
        parameters.push_back(std::string());
        current = &parameters.back();
      }
      else {
        (*current) += str[i];
      }
    }
  }

  Subproblem::Subproblem(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters, const std::string &E, const std::string &N, const std::string &W, const std::string &S, const std::string &D, const std::string &U) : _domains(domains), _parameters(parameters), _formulation(formulation), _fieldName(fieldName), _boundary(6, nullptr), _edge(12, nullptr), _corner(8, nullptr)
  {
    std::string type[6] = {E, N, W, S, D, U};
    std::string method[6];
    std::vector< std::string > bndParameters[6];
    for(unsigned int i = 0; i < 6; ++i) {
      _parseParameters(method[i], bndParameters[i], type[i]);
    }
    
    for(unsigned int b = 0; b < 6; ++b) {
      if(method[b] == "sommerfeld") { // str: sommerfeld
        _boundary[b] = new Sommerfeld(b);
      }
      else if(method[b] == "pmlContinuous") { // str: pmlContinuous_[N_pml]_[Type]
        const double N_pml  = std::stof(bndParameters[b][0]);
        const std::string type  = bndParameters[b][1];
        _boundary[b] = new PmlContinuous(b, N_pml, type);
      }
      else if(method[b] == "pmlContinuousSymmetric") { // str: pmlContinuousSymmetric_[N_pml]_[Type]
        const double N_pml  = std::stof(bndParameters[b][0]);
        const std::string type  = bndParameters[b][1];
        _boundary[b] = new PmlContinuousSymmetric(b, N_pml, type);
      }
      else {
        gmshfem::msg::error << "Unknown method '" << method[b] << "'" << gmshfem::msg::endl;
      }
    }
    
    for(unsigned int e = 0; e < 12; ++e) {
      if(e < 4) {
        if(method[e%4] == "pmlContinuous" && method[4] == "pmlContinuous") {
          _edge[e] = new PmlContinuous_PmlContinuous(e, static_cast< PmlContinuous * >(_boundary[e%4]), static_cast< PmlContinuous * >(_boundary[4]));
        }
        else if(method[e%4] == "pmlContinuousSymmetric" && method[4] == "pmlContinuousSymmetric") {
          _edge[e] = new PmlContinuousSymmetric_PmlContinuousSymmetric(e, static_cast< PmlContinuousSymmetric * >(_boundary[e%4]), static_cast< PmlContinuousSymmetric * >(_boundary[4]));
        }
      }
      else if(e >= 4 && e < 8) {
        if(method[e%4] == "pmlContinuous" && method[(e+1)%4] == "pmlContinuous") {
          _edge[e] = new PmlContinuous_PmlContinuous(e, static_cast< PmlContinuous * >(_boundary[e%4]), static_cast< PmlContinuous * >(_boundary[(e+1)%4]));
        }
        else if(method[e%4] == "pmlContinuousSymmetric" && method[(e+1)%4] == "pmlContinuousSymmetric") {
          _edge[e] = new PmlContinuousSymmetric_PmlContinuousSymmetric(e, static_cast< PmlContinuousSymmetric * >(_boundary[e%4]), static_cast< PmlContinuousSymmetric * >(_boundary[(e+1)%4]));
        }
      }
      else {
        if(method[e%4] == "pmlContinuous" && method[5] == "pmlContinuous") {
          _edge[e] = new PmlContinuous_PmlContinuous(e, static_cast< PmlContinuous * >(_boundary[e%4]), static_cast< PmlContinuous * >(_boundary[5]));
        }
        else if(method[e%4] == "pmlContinuousSymmetric" && method[5] == "pmlContinuousSymmetric") {
          _edge[e] = new PmlContinuousSymmetric_PmlContinuousSymmetric(e, static_cast< PmlContinuousSymmetric * >(_boundary[e%4]), static_cast< PmlContinuousSymmetric * >(_boundary[5]));
        }
      }
    }
    
    for(unsigned int c = 0; c < 8; ++c) {
      if(c < 4) {
        if(method[c%4] == "pmlContinuous" && method[(c+1)%4] == "pmlContinuous" && method[4] == "pmlContinuous") {
          _corner[c] = new PmlContinuous_PmlContinuous_PmlContinuous(c, static_cast< PmlContinuous_PmlContinuous * >(_edge[c%4]), static_cast< PmlContinuous_PmlContinuous * >(_edge[(c+1)%4]), static_cast< PmlContinuous_PmlContinuous * >(_edge[c+4]));
        }
        else if(method[c%4] == "pmlContinuousSymmetric" && method[(c+1)%4] == "pmlContinuousSymmetric" && method[4] == "pmlContinuousSymmetric") {
//          _corner[c] = new PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify(c, static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[c%4]), static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[(c+1)%4]), static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[c+4]));
          _corner[c] = new PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric(c, static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[c%4]), static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[(c+1)%4]), static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[c+4]));
        }
      }
      else {
        if(method[c%4] == "pmlContinuous" && method[(c+1)%4] == "pmlContinuous" && method[5] == "pmlContinuous") {
          _corner[c] = new PmlContinuous_PmlContinuous_PmlContinuous(c, static_cast< PmlContinuous_PmlContinuous * >(_edge[c%4+8]), static_cast< PmlContinuous_PmlContinuous * >(_edge[(c+1)%4+8]), static_cast< PmlContinuous_PmlContinuous * >(_edge[c]));
        }
        else if(method[c%4] == "pmlContinuousSymmetric" && method[(c+1)%4] == "pmlContinuousSymmetric" && method[5] == "pmlContinuousSymmetric") {
//          _corner[c] = new PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify(c, static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[c%4+8]), static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[(c+1)%4+8]), static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[c]));
          _corner[c] = new PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric(c, static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[c%4+8]), static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[(c+1)%4+8]), static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_edge[c]));
        }
      }
    }
  }

  Subproblem::~Subproblem()
  {
    for(unsigned int c = 0; c < 8; ++c) {
      if(_corner[c] != nullptr) {
        delete _corner[c];
      }
    }
    for(unsigned int e = 0; e < 12; ++e) {
      if(_edge[e] != nullptr) {
        delete _edge[e];
      }
    }
    for(unsigned int b = 0; b < 6; ++b) {
      if(_boundary[b] != nullptr) {
        delete _boundary[b];
      }
    }
  }

  void Subproblem::writeFormulation()
  {
    for(unsigned int b = 0; b < 6; ++b) {
      _boundary[b]->writeFormulation(_formulation, _fieldName, _domains, _parameters);
    }
    
    for(unsigned int e = 0; e < 12; ++e) {
      if(_edge[e] != nullptr) {
        _edge[e]->writeFormulation(_formulation, _domains, _parameters);
      }
    }
    
    for(unsigned int c = 0; c < 8; ++c) {
      if(_corner[c] != nullptr) {
        _corner[c]->writeFormulation(_formulation, _domains, _parameters);
      }
    }
  }

  Boundary *Subproblem::getBoundary(const unsigned int b) const
  {
    return _boundary[b];
  }
  
  Edge *Subproblem::getEdge(const unsigned int e) const
  {
    return _edge[e];
  }

  Corner *Subproblem::getCorner(const unsigned int c) const
  {
    return _corner[c];
  }

  // **********************************
  // Sommerfeld
  // **********************************

  Sommerfeld::Sommerfeld(const unsigned int boundary) : Boundary(boundary)
  {
  }

  Sommerfeld::~Sommerfeld()
  {
    delete _v;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* Sommerfeld::getV() const
  {
    return _v;
  }

  void Sommerfeld::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    
    _v = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *u = static_cast< gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * >(formulation.getField(fieldName));
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;
      
    const std::complex< double > im(0., 1.);
    
    formulation.integral(- dof(*_v), tf(*u), sigma, gauss);

    formulation.integral(dof(*_v), tf(*_v), sigma, gauss);
    formulation.integral((im/kappaP) * In * dof(*u), tf(*_v), sigma, gauss);
    formulation.integral((im/kappaS) * It * dof(*u), tf(*_v), sigma, gauss);
  }
  
  // **********************************
  // PML
  // **********************************

  Pml::Pml(const unsigned int boundary, const double pmlSize, const std::string &type) : Boundary(boundary), _size(pmlSize), _type(type)
  {
  }

  Pml::~Pml()
  {
  }

  double Pml::getSize() const
  {
    return _size;
  }
  
  gmshfem::function::TensorFunction< std::complex< double >, 4 > Pml::pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double >, 4 > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E,  gmshfem::domain::Domain &bnd, const gmshfem::function::TensorFunction< std::complex< double >, 4 > &C, gmshfem::function::ScalarFunction< std::complex< double > > &kappaP, gmshfem::function::ScalarFunction< std::complex< double > > &kappaS) const
  {
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod;
    const std::complex< double > im(0., 1.);

    gmshfem::function::ScalarFunction< std::complex< double > > distSigma;
    gmshfem::function::ScalarFunction< std::complex< double > > sigma;
    if(orientation() == "E" || orientation() == "W") {
      distSigma = abs(gmshfem::function::x< std::complex< double > >() - gmshfem::function::x< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
      }
    }
    else if(orientation() == "N" || orientation() == "S") {
      distSigma = abs(gmshfem::function::y< std::complex< double > >() - gmshfem::function::y< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
      }
    }
    else if(orientation() == "D" || orientation() == "U") {
      distSigma = abs(gmshfem::function::z< std::complex< double > >() - gmshfem::function::z< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        CMod = changeOfCoordinates(C, gmshfem::function::x< double >(), gmshfem::function::y< double >(), gmshfem::function::z< double >(bnd));
        kappaP = changeOfCoordinates(kappaP, gmshfem::function::x< double >(), gmshfem::function::y< double >(), gmshfem::function::z< double >(bnd));
        kappaS = changeOfCoordinates(kappaS, gmshfem::function::x< double >(), gmshfem::function::y< double >(), gmshfem::function::z< double >(bnd));
      }
    }
    
    if(_type == "hs") {
      sigma = 1. / (_size - distSigma) - 1./ (_size);
    }
    else if(_type == "h") {
      sigma = 1. / (_size - distSigma);
    }
    else if(_type == "q") {
      const double sigmaStar = 86.435;
      sigma = sigmaStar * distSigma * distSigma / (_size * _size);
    }
    
    gmshfem::function::ScalarFunction< std::complex< double > > gamma = 1. + im * sigma;
    gmshfem::function::ScalarFunction< std::complex< double > > invGamma = 1./gamma;
    if(orientation() == "E" || orientation() == "W") {
      gmshfem::function::TensorFunction< std::complex< double > > C_1 = gmshfem::function::tensor< std::complex< double > >(invGamma, invGamma, invGamma,
                                                                                                                            1., 1., 1.,
                                                                                                                            1., 1., 1.);
      gmshfem::function::TensorFunction< std::complex< double > > C_2 = gmshfem::function::tensor< std::complex< double > >(1., 1., 1.,
                                                                                                                            gamma, gamma, gamma,
                                                                                                                            gamma, gamma, gamma);
      D = gmshfem::function::tensor< std::complex< double >, 4 >(C_1, C_1, C_1,
                                                                 C_2, C_2, C_2,
                                                                 C_2, C_2, C_2);
      E = gamma;
    }
    else if(orientation() == "N" || orientation() == "S") {
      gmshfem::function::TensorFunction< std::complex< double > > C_1 = gmshfem::function::tensor< std::complex< double > >(1., 1., 1.,
                                                                                                                            invGamma, invGamma, invGamma,
                                                                                                                            1., 1., 1.);
      gmshfem::function::TensorFunction< std::complex< double > > C_2 = gmshfem::function::tensor< std::complex< double > >(gamma, gamma, gamma,
                                                                                                                            1., 1., 1.,
                                                                                                                            gamma, gamma, gamma);

      D = gmshfem::function::tensor< std::complex< double >, 4 >(C_2, C_2, C_2,
                                                                 C_1, C_1, C_1,
                                                                 C_2, C_2, C_2);
      E = gamma;
    }
    else if(orientation() == "D" || orientation() == "U") {
      gmshfem::function::TensorFunction< std::complex< double > > C_1 = gmshfem::function::tensor< std::complex< double > >(1., 1., 1.,
                                                                                                                            1., 1., 1.,
                                                                                                                            invGamma, invGamma, invGamma);
      gmshfem::function::TensorFunction< std::complex< double > > C_2 = gmshfem::function::tensor< std::complex< double > >(gamma, gamma, gamma,
                                                                                                                            gamma, gamma, gamma,
                                                                                                                            1., 1., 1.);

      D = gmshfem::function::tensor< std::complex< double >, 4 >(C_2, C_2, C_2,
                                                                 C_2, C_2, C_2,
                                                                 C_1, C_1, C_1);
      E = gamma;
    }
    
    return CMod;
  }
  
  // **********************************
  // PML continuous
  // **********************************

  PmlContinuous::PmlContinuous(const unsigned int boundary, const double pmlSize, const std::string &type) : Pml(boundary, pmlSize, type)
  {
  }

  PmlContinuous::~PmlContinuous()
  {
    delete _v;
    delete _uPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * PmlContinuous::getUPml() const
  {
    return _uPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * PmlContinuous::getV() const
  {
    return _v;
  }

  void PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    gmshfem::domain::Domain pml = domains.getPml(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    const double stab = parameters.getStab();
    
    _uPml = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _v = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *u = static_cast< gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * >(formulation.getField(fieldName));
    
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = pmlCoefficients(D, E, sigma, C, kappaP, kappaS);
            
    formulation.integral(- gmshfem::function::hadamard(CMod, D) * grad(dof(*_uPml)), grad(tf(*_uPml)), pml, gauss);
    formulation.integral(E * dof(*_uPml), tf(*_uPml), pml, gauss);
    
    formulation.integral(dof(*_v), tf(*_uPml), sigma, gauss);
    formulation.integral(- dof(*_v), tf(*u), sigma, gauss);

    formulation.integral(dof(*_uPml), tf(*_v), sigma, gauss);
    formulation.integral(- dof(*u), tf(*_v), sigma, gauss);
    
    // penalization
    // formulation.integral(1.e10 * stab * dof(*_v), tf(*_v), sigma, gauss);
  }
  
  // **********************************
  // PML continuous symmetric
  // **********************************
  
  PmlContinuousSymmetric::PmlContinuousSymmetric(const unsigned int boundary, const double pmlSize, const std::string &type) : Pml(boundary, pmlSize, type)
  {
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree4 >::Object tmpX, tmpY, tmpZ;
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(i == k && j == l) {
              tmpX(i,j)(k,l) = 1.;
              tmpY(i,j)(k,l) = 1.;
              tmpZ(i,j)(k,l) = 1.;
            }
            else {
              tmpX(i,j)(k,l) = 0.;
              tmpY(i,j)(k,l) = 0.;
              tmpZ(i,j)(k,l) = 0.;
            }
          }
        }
      }
    }
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(l == 0) {
              tmpX(i,j)(k,l) *= -1.; // symmetry of grad_x
            }
            else if(l == 1) {
              tmpY(i,j)(k,l) *= -1.; // symmetry of grad_y
            }
            else if(l == 2) {
              tmpZ(i,j)(k,l) *= -1.; // symmetry of grad_z
            }
          }
        }
      }
    }
    _symX = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpX);
    _symY = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpY);
    _symZ = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpZ);
  }

  PmlContinuousSymmetric::~PmlContinuousSymmetric()
  {
    delete _v;
    delete _vSym;
    delete _uPml;
    delete _uSymPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * PmlContinuousSymmetric::getUPml() const
  {
    return _uPml;
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * PmlContinuousSymmetric::getUSymPml() const
  {
    return _uSymPml;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * PmlContinuousSymmetric::getV() const
  {
    return _v;
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * PmlContinuousSymmetric::getVSym() const
  {
    return _vSym;
  }

  void PmlContinuousSymmetric::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    gmshfem::domain::Domain pml = domains.getPml(_boundary);
    gmshfem::domain::Domain pmlInf = domains.getPmlInf(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    
    _uPml = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _uSymPml = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uSymPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _v = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSym = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSym_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *u = static_cast< gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * >(formulation.getField(fieldName));
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;
    
    const std::complex< double > im(0., 1.);
    
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = pmlCoefficients(D, E, sigma, C, kappaP, kappaS);
    
    
    formulation.integral(- gmshfem::function::hadamard(CMod, D) * grad(dof(*_uPml)), grad(tf(*_uPml)), pml, gauss);
    formulation.integral(E * dof(*_uPml), tf(*_uPml), pml, gauss);
    
    formulation.integral(dof(*_v), tf(*_uPml), sigma, gauss);
    formulation.integral(- dof(*_v), tf(*u), sigma, gauss);

    formulation.integral(dof(*_uPml), tf(*_v), sigma, gauss);
    formulation.integral(- dof(*u), tf(*_v), sigma, gauss);
    
    formulation.integral((im/kappaP) * In * dof(*_uPml), tf(*_uPml), pmlInf, gauss);
    formulation.integral((im/kappaS) * It * dof(*_uPml), tf(*_uPml), pmlInf, gauss);
        
    // symmetric
    gmshfem::function::TensorFunction< std::complex< double >, 4 > &sym = ((_boundary == 0 || _boundary == 2) ? _symX : ((_boundary == 1 || _boundary == 3) ? _symY : _symZ));
    
    formulation.integral(- gmshfem::function::hadamard(CMod, D) * sym * grad(dof(*_uSymPml)), sym * grad(tf(*_uSymPml)), pml, gauss);
    formulation.integral(E * dof(*_uSymPml), tf(*_uSymPml), pml, gauss);
    
    formulation.integral(dof(*_vSym), tf(*_uSymPml), sigma, gauss);

    formulation.integral(dof(*_uSymPml), tf(*_vSym), sigma, gauss);
    formulation.integral(- dof(*u), tf(*_vSym), sigma, gauss);
    
    formulation.integral((im/kappaP) * In * dof(*_uSymPml), tf(*_uSymPml), pmlInf, gauss);
    formulation.integral((im/kappaS) * It * dof(*_uSymPml), tf(*_uSymPml), pmlInf, gauss);
//
//
//    gmshfem::domain::Domain pmlBnd1 = domains.getPmlBnd((_boundary+1)%4).first;
//    gmshfem::domain::Domain pmlBnd2 = domains.getPmlBnd((_boundary-1)%4).second;
//    formulation.integral((im/kappaP) * In * dof(*_uPml), tf(*_uPml), pmlBnd1 | pmlBnd2, gauss);
//    formulation.integral((im/kappaS) * It * dof(*_uPml), tf(*_uPml), pmlBnd1 | pmlBnd2, gauss);
//
//    formulation.integral((im/kappaP) * In * dof(*_uSymPml), tf(*_uSymPml), pmlBnd1 | pmlBnd2, gauss);
//    formulation.integral((im/kappaS) * It * dof(*_uSymPml), tf(*_uSymPml), pmlBnd1 | pmlBnd2, gauss);
  }
  
  
  
  // **********************************
  // PML continuous _ PML continuous
  // **********************************

  PmlContinuous_PmlContinuous::PmlContinuous_PmlContinuous(const unsigned int edge, PmlContinuous *first, PmlContinuous *second) : Edge(edge, first, second)
  {
  }

  PmlContinuous_PmlContinuous::~PmlContinuous_PmlContinuous()
  {
    delete _uPmlEdge;
    delete _vE[0];
    delete _vE[1];
    delete _vEdge;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuous_PmlContinuous::getUPml() const
  {
    return _uPmlEdge;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuous_PmlContinuous::getV(const unsigned int i) const
  {
    return _vE[i];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuous_PmlContinuous::getVEdge() const
  {
    return _vEdge;
  }

  PmlContinuous *PmlContinuous_PmlContinuous::firstBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[0]);
  }

  PmlContinuous *PmlContinuous_PmlContinuous::secondBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[1]);
  }

  void PmlContinuous_PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain edge = domains.getEdge(_edge);
    gmshfem::domain::Domain pmlEdge = domains.getPmlEdge(_edge);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_edge);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
        const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;

    _uPmlEdge = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uPmlEdge_" + orientation(), pmlEdge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vE[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vE_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vE[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vE_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uPml[2];
    uPml[0] = static_cast< PmlContinuous * >(_bnd[0])->getUPml();
    uPml[1] = static_cast< PmlContinuous * >(_bnd[1])->getUPml();

    gmshfem::function::TensorFunction< std::complex< double >, 4 > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = static_cast< PmlContinuous * >(_bnd[0])->pmlCoefficients(D[0], E[0], edge, C, kappaP, kappaS);
    static_cast< PmlContinuous * >(_bnd[1])->pmlCoefficients(D[1], E[1], edge, C, fake, fake);

    formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * grad(dof(*_uPmlEdge)), grad(tf(*_uPmlEdge)), pmlEdge, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uPmlEdge), tf(*_uPmlEdge), pmlEdge, gauss);

    formulation.integral(dof(*_vE[0]), tf(*_uPmlEdge), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vE[0]), tf(*uPml[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_uPmlEdge), tf(*_vE[0]), pmlBnd.first, gauss);
    formulation.integral(- dof(*uPml[0]), tf(*_vE[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_vE[1]), tf(*_uPmlEdge), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vE[1]), tf(*uPml[1]), pmlBnd.second, gauss);

    formulation.integral(dof(*_uPmlEdge), tf(*_vE[1]), pmlBnd.second, gauss);
    formulation.integral(- dof(*uPml[1]), tf(*_vE[1]), pmlBnd.second, gauss);
    
    // edge equation
    _vEdge = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vEdge_" + orientation(), edge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    formulation.integral(dof(*_vE[0]), tf(*_vEdge), edge, gauss);
    formulation.integral(dof(*_vEdge), tf(*_vE[0]), edge, gauss);

    formulation.integral(-dof(*_vE[1]), tf(*_vEdge), edge, gauss);
    formulation.integral(-dof(*_vEdge), tf(*_vE[1]), edge, gauss);

    formulation.integral(dof(*static_cast< PmlContinuous * >(_bnd[0])->getV()), tf(*_vEdge), edge, gauss);
    formulation.integral(dof(*_vEdge), tf(*static_cast< PmlContinuous * >(_bnd[0])->getV()), edge, gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous * >(_bnd[1])->getV()), tf(*_vEdge), edge, gauss);
    formulation.integral(-dof(*_vEdge), tf(*static_cast< PmlContinuous * >(_bnd[1])->getV()), edge, gauss);
  }
  
  // **********************************
  // PML continuous symmetric _ PML continuous symmetric
  // **********************************

  PmlContinuousSymmetric_PmlContinuousSymmetric::PmlContinuousSymmetric_PmlContinuousSymmetric(const unsigned int edge, PmlContinuousSymmetric *first, PmlContinuousSymmetric *second) : Edge(edge, first, second)
  {
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree4 >::Object tmpX, tmpY, tmpZ;
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(i == k && j == l) {
              tmpX(i,j)(k,l) = 1.;
              tmpY(i,j)(k,l) = 1.;
              tmpZ(i,j)(k,l) = 1.;
            }
            else {
              tmpX(i,j)(k,l) = 0.;
              tmpY(i,j)(k,l) = 0.;
              tmpZ(i,j)(k,l) = 0.;
            }
          }
        }
      }
    }
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(l == 0) {
              tmpX(i,j)(k,l) *= -1.; // symmetry of grad_x
            }
            else if(l == 1) {
              tmpY(i,j)(k,l) *= -1.; // symmetry of grad_y
            }
            else if(l == 2) {
              tmpZ(i,j)(k,l) *= -1.; // symmetry of grad_z
            }
          }
        }
      }
    }
    _symX = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpX);
    _symY = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpY);
    _symZ = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpZ);
  }

  PmlContinuousSymmetric_PmlContinuousSymmetric::~PmlContinuousSymmetric_PmlContinuousSymmetric()
  {
    delete _uPmlEdge;
    delete _uSymPmlEdge[0];
    delete _uSymPmlEdge[1];
    delete _uSymSymPmlEdge;
    delete _vE[0];
    delete _vE[1];
    delete _vSymE[0][0];
    delete _vSymE[0][1];
    delete _vSymE[1][0];
    delete _vSymE[1][1];
    delete _vSymSymE[0];
    delete _vSymSymE[1];
    delete _vEdge;
    delete _vSymEdge[0];
    delete _vSymEdge[1];
    delete _vSymSymEdge;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getUPml() const
  {
    return _uPmlEdge;
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getUSymPml(const unsigned int i) const
  {
    return _uSymPmlEdge[i];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getUSymSymPml() const
  {
    return _uSymSymPmlEdge;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getV(const unsigned int i) const
  {
    return _vE[i];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getVSym(const unsigned int i, const unsigned int j) const
  {
    return _vSymE[i][j];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getVSymSym(const unsigned int i) const
  {
    return _vSymSymE[i];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getVEdge() const
  {
    return _vEdge;
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getVSymEdge(const unsigned int i) const
  {
    return _vSymEdge[i];
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric::getVSymSymEdge() const
  {
    return _vSymSymEdge;
  }

  PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric::firstBoundary() const
  {
    return static_cast< PmlContinuousSymmetric * >(_bnd[0]);
  }

  PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric::secondBoundary() const
  {
    return static_cast< PmlContinuousSymmetric * >(_bnd[1]);
  }

  void PmlContinuousSymmetric_PmlContinuousSymmetric::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain edge = domains.getEdge(_edge);
    gmshfem::domain::Domain pmlEdge = domains.getPmlEdge(_edge);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_edge);
    gmshfem::domain::Domain pmlEdgeInf = domains.getPmlEdgeInf(_edge);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
        const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;

    _uPmlEdge = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uPmlEdge_" + orientation(), pmlEdge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    
    _uSymPmlEdge[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uSymPmlEdge_" + orientation() + "_first", pmlEdge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _uSymPmlEdge[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uSymPmlEdge_" + orientation() + "_second", pmlEdge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    
    _uSymSymPmlEdge = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uSymSymPmlEdge_" + orientation(), pmlEdge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vE[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vE_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vE[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vE_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    _vSymE[0][0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymE_first_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSymE[0][1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymE_first_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    _vSymE[1][0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymE_second_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSymE[1][1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymE_second_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    _vSymSymE[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymE_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vSymSymE[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymE_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uPml[2];
    uPml[0] = static_cast< PmlContinuousSymmetric * >(_bnd[0])->getUPml();
    uPml[1] = static_cast< PmlContinuousSymmetric * >(_bnd[1])->getUPml();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uSymPml[2];
    uSymPml[0] = static_cast< PmlContinuousSymmetric * >(_bnd[0])->getUSymPml();
    uSymPml[1] = static_cast< PmlContinuousSymmetric * >(_bnd[1])->getUSymPml();
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;
    
    const std::complex< double > im(0., 1.);

    gmshfem::function::TensorFunction< std::complex< double >, 4 > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = static_cast< PmlContinuousSymmetric * >(_bnd[0])->pmlCoefficients(D[0], E[0], edge, C, kappaP, kappaS);
    static_cast< PmlContinuousSymmetric * >(_bnd[1])->pmlCoefficients(D[1], E[1], edge, C, fake, fake);

    formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * grad(dof(*_uPmlEdge)), grad(tf(*_uPmlEdge)), pmlEdge, gauss);
    formulation.integral(E[0] * E[1] * dof(*_uPmlEdge), tf(*_uPmlEdge), pmlEdge, gauss);
    
    formulation.integral(dof(*_vE[0]), tf(*_uPmlEdge), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vE[0]), tf(*uPml[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_uPmlEdge), tf(*_vE[0]), pmlBnd.first, gauss);
    formulation.integral(- dof(*uPml[0]), tf(*_vE[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_vE[1]), tf(*_uPmlEdge), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vE[1]), tf(*uPml[1]), pmlBnd.second, gauss);

    formulation.integral(dof(*_uPmlEdge), tf(*_vE[1]), pmlBnd.second, gauss);
    formulation.integral(- dof(*uPml[1]), tf(*_vE[1]), pmlBnd.second, gauss);
    
    formulation.integral((im/kappaP) * In * dof(*_uPmlEdge), tf(*_uPmlEdge), pmlEdgeInf, gauss);
    formulation.integral((im/kappaS) * It * dof(*_uPmlEdge), tf(*_uPmlEdge), pmlEdgeInf, gauss);
    
    // edge equation
    _vEdge = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vEdge_" + orientation(), edge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    formulation.integral(dof(*_vE[0]), tf(*_vEdge), edge, gauss);
    formulation.integral(dof(*_vEdge), tf(*_vE[0]), edge, gauss);

    formulation.integral(-dof(*_vE[1]), tf(*_vEdge), edge, gauss);
    formulation.integral(-dof(*_vEdge), tf(*_vE[1]), edge, gauss);

    formulation.integral(dof(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getV()), tf(*_vEdge), edge, gauss);
    formulation.integral(dof(*_vEdge), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getV()), edge, gauss);

    formulation.integral(-dof(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getV()), tf(*_vEdge), edge, gauss);
    formulation.integral(-dof(*_vEdge), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getV()), edge, gauss);
    
    
    // symmetric bnd 0
    {
      gmshfem::function::TensorFunction< std::complex< double >, 4 > &sym = (_edge == 0 ? _symZ :
                                                                            (_edge == 1 ? _symZ :
                                                                            (_edge == 2 ? _symZ :
                                                                            (_edge == 3 ? _symZ :
                                                                            (_edge == 4 ? _symY :
                                                                            (_edge == 5 ? _symX :
                                                                            (_edge == 6 ? _symY :
                                                                            (_edge == 7 ? _symX :
                                                                            (_edge == 8 ? _symZ :
                                                                            (_edge == 9 ? _symZ :
                                                                            (_edge == 10 ? _symZ : _symZ)))))))))));
      
      formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * sym * grad(dof(*_uSymPmlEdge[0])), sym * grad(tf(*_uSymPmlEdge[0])), pmlEdge, gauss);
      formulation.integral(E[0] * E[1] * dof(*_uSymPmlEdge[0]), tf(*_uSymPmlEdge[0]), pmlEdge, gauss);

      formulation.integral(dof(*_vSymE[0][0]), tf(*_uSymPmlEdge[0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_uSymPmlEdge[0]), tf(*_vSymE[0][0]), pmlBnd.first, gauss);
      formulation.integral(- dof(*uPml[0]), tf(*_vSymE[0][0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_vSymE[0][1]), tf(*_uSymPmlEdge[0]), pmlBnd.second, gauss);
      formulation.integral(- dof(*_vSymE[0][1]), tf(*uSymPml[1]), pmlBnd.second, gauss);

      formulation.integral(dof(*_uSymPmlEdge[0]), tf(*_vSymE[0][1]), pmlBnd.second, gauss);
      formulation.integral(- dof(*uSymPml[1]), tf(*_vSymE[0][1]), pmlBnd.second, gauss);
      
      formulation.integral((im/kappaP) * In * dof(*_uSymPmlEdge[0]), tf(*_uSymPmlEdge[0]), pmlEdgeInf, gauss);
      formulation.integral((im/kappaS) * It * dof(*_uSymPmlEdge[0]), tf(*_uSymPmlEdge[0]), pmlEdgeInf, gauss);
      
      // edge equation
      _vSymEdge[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymEdge_" + orientation() + "_first", edge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

      formulation.integral(dof(*_vSymE[0][0]), tf(*_vSymEdge[0]), edge, gauss);
      formulation.integral(dof(*_vSymEdge[0]), tf(*_vSymE[0][0]), edge, gauss);

      formulation.integral(-dof(*_vSymE[0][1]), tf(*_vSymEdge[0]), edge, gauss);
      formulation.integral(-dof(*_vSymEdge[0]), tf(*_vSymE[0][1]), edge, gauss);

      formulation.integral(dof(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getV()), tf(*_vSymEdge[0]), edge, gauss);
      formulation.integral(dof(*_vSymEdge[0]), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getV()), edge, gauss);

      formulation.integral(-dof(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getVSym()), tf(*_vSymEdge[0]), edge, gauss);
      formulation.integral(-dof(*_vSymEdge[0]), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getVSym()), edge, gauss);
    }
    
    
    // symmetric bnd 1
    {
      gmshfem::function::TensorFunction< std::complex< double >, 4 > &sym = (_edge == 0 ? _symX :
                                                                            (_edge == 1 ? _symY :
                                                                            (_edge == 2 ? _symX :
                                                                            (_edge == 3 ? _symY :
                                                                            (_edge == 4 ? _symX :
                                                                            (_edge == 5 ? _symY :
                                                                            (_edge == 6 ? _symX :
                                                                            (_edge == 7 ? _symY :
                                                                            (_edge == 8 ? _symX :
                                                                            (_edge == 9 ? _symY :
                                                                            (_edge == 10 ? _symX : _symY)))))))))));
      
      formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * sym * grad(dof(*_uSymPmlEdge[1])), sym * grad(tf(*_uSymPmlEdge[1])), pmlEdge, gauss);
      formulation.integral(E[0] * E[1] * dof(*_uSymPmlEdge[1]), tf(*_uSymPmlEdge[1]), pmlEdge, gauss);

      formulation.integral(dof(*_vSymE[1][0]), tf(*_uSymPmlEdge[1]), pmlBnd.first, gauss);
      formulation.integral(- dof(*_vSymE[1][0]), tf(*uSymPml[0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_uSymPmlEdge[1]), tf(*_vSymE[1][0]), pmlBnd.first, gauss);
      formulation.integral(- dof(*uSymPml[0]), tf(*_vSymE[1][0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_vSymE[1][1]), tf(*_uSymPmlEdge[1]), pmlBnd.second, gauss);

      formulation.integral(dof(*_uSymPmlEdge[1]), tf(*_vSymE[1][1]), pmlBnd.second, gauss);
      formulation.integral(- dof(*uPml[1]), tf(*_vSymE[1][1]), pmlBnd.second, gauss);
      
      formulation.integral((im/kappaP) * In * dof(*_uSymPmlEdge[1]), tf(*_uSymPmlEdge[1]), pmlEdgeInf, gauss);
      formulation.integral((im/kappaS) * It * dof(*_uSymPmlEdge[1]), tf(*_uSymPmlEdge[1]), pmlEdgeInf, gauss);
      
      // edge equation
      _vSymEdge[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymEdge_" + orientation() + "_second", edge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

      formulation.integral(dof(*_vSymE[1][0]), tf(*_vSymEdge[1]), edge, gauss);
      formulation.integral(dof(*_vSymEdge[1]), tf(*_vSymE[1][0]), edge, gauss);

      formulation.integral(-dof(*_vSymE[1][1]), tf(*_vSymEdge[1]), edge, gauss);
      formulation.integral(-dof(*_vSymEdge[1]), tf(*_vSymE[1][1]), edge, gauss);

      formulation.integral(dof(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getVSym()), tf(*_vSymEdge[1]), edge, gauss);
      formulation.integral(dof(*_vSymEdge[1]), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getVSym()), edge, gauss);

      formulation.integral(-dof(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getV()), tf(*_vSymEdge[1]), edge, gauss);
      formulation.integral(-dof(*_vSymEdge[1]), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getV()), edge, gauss);
    }
    
    
    // symmetric bnd 0 & 1
    {
      gmshfem::function::TensorFunction< std::complex< double >, 4 > hxz = gmshfem::function::hadamard(_symX, _symZ);
      gmshfem::function::TensorFunction< std::complex< double >, 4 > hyz = gmshfem::function::hadamard(_symY, _symZ);
      gmshfem::function::TensorFunction< std::complex< double >, 4 > hxy = gmshfem::function::hadamard(_symX, _symY);
      gmshfem::function::TensorFunction< std::complex< double >, 4 > &sym = (_edge == 0 ? hxz :
                                                                            (_edge == 1 ? hyz :
                                                                            (_edge == 2 ? hxz :
                                                                            (_edge == 3 ? hyz :
                                                                            (_edge == 4 ? hxy :
                                                                            (_edge == 5 ? hxy :
                                                                            (_edge == 6 ? hxy :
                                                                            (_edge == 7 ? hxy :
                                                                            (_edge == 8 ? hxz :
                                                                            (_edge == 9 ? hyz :
                                                                            (_edge == 10 ? hxz : hyz)))))))))));
                                                                            
      formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0], D[1])) * sym * grad(dof(*_uSymSymPmlEdge)), sym * grad(tf(*_uSymSymPmlEdge)), pmlEdge, gauss);
      formulation.integral(E[0] * E[1] * dof(*_uSymSymPmlEdge), tf(*_uSymSymPmlEdge), pmlEdge, gauss);
      
      formulation.integral(dof(*_vSymSymE[0]), tf(*_uSymSymPmlEdge), pmlBnd.first, gauss);

      formulation.integral(dof(*_uSymSymPmlEdge), tf(*_vSymSymE[0]), pmlBnd.first, gauss);
      formulation.integral(- dof(*uSymPml[0]), tf(*_vSymSymE[0]), pmlBnd.first, gauss);

      formulation.integral(dof(*_vSymSymE[1]), tf(*_uSymSymPmlEdge), pmlBnd.second, gauss);
  
      formulation.integral(dof(*_uSymSymPmlEdge), tf(*_vSymSymE[1]), pmlBnd.second, gauss);
      formulation.integral(- dof(*uSymPml[1]), tf(*_vSymSymE[1]), pmlBnd.second, gauss);
      
      formulation.integral((im/kappaP) * In * dof(*_uSymSymPmlEdge), tf(*_uSymSymPmlEdge), pmlEdgeInf, gauss);
      formulation.integral((im/kappaS) * It * dof(*_uSymSymPmlEdge), tf(*_uSymSymPmlEdge), pmlEdgeInf, gauss);

      // corner equation
      _vSymSymEdge = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymEdge_" + orientation(), edge, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

      formulation.integral(dof(*_vSymSymE[0]), tf(*_vSymSymEdge), edge, gauss);
      formulation.integral(dof(*_vSymSymEdge), tf(*_vSymSymE[0]), edge, gauss);

      formulation.integral(-dof(*_vSymSymE[1]), tf(*_vSymSymEdge), edge, gauss);
      formulation.integral(-dof(*_vSymSymEdge), tf(*_vSymSymE[1]), edge, gauss);

      formulation.integral(dof(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getVSym()), tf(*_vSymSymEdge), edge, gauss);
      formulation.integral(dof(*_vSymSymEdge), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[0])->getVSym()), edge, gauss);

      formulation.integral(-dof(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getVSym()), tf(*_vSymSymEdge), edge, gauss);
      formulation.integral(-dof(*_vSymSymEdge), tf(*static_cast< PmlContinuousSymmetric * >(_bnd[1])->getVSym()), edge, gauss);
    }
  }
  


  
  // **********************************
  // PML continuous _ PML continuous _ PML continuous
  // **********************************

  PmlContinuous_PmlContinuous_PmlContinuous::PmlContinuous_PmlContinuous_PmlContinuous(const unsigned int corner, PmlContinuous_PmlContinuous *first, PmlContinuous_PmlContinuous *second, PmlContinuous_PmlContinuous *third) : Corner(corner, first, second, third)
  {
  }

  PmlContinuous_PmlContinuous_PmlContinuous::~PmlContinuous_PmlContinuous_PmlContinuous()
  {
    delete _uPmlCorner;
    delete _vC[0];
    delete _vC[1];
    delete _vC[2];
    delete _vEdge[0];
    delete _vEdge[1];
    delete _vEdge[2];
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuous_PmlContinuous_PmlContinuous::getUPml() const
  {
    return _uPmlCorner;
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuous_PmlContinuous_PmlContinuous::getV(const unsigned int i) const
  {
    return _vC[i];
  }

  PmlContinuous_PmlContinuous *PmlContinuous_PmlContinuous_PmlContinuous::firstEdge() const
  {
    return static_cast< PmlContinuous_PmlContinuous * >(_bnd[0]);
  }

  PmlContinuous_PmlContinuous *PmlContinuous_PmlContinuous_PmlContinuous::secondEdge() const
  {
    return static_cast< PmlContinuous_PmlContinuous * >(_bnd[1]);
  }
  
  PmlContinuous_PmlContinuous *PmlContinuous_PmlContinuous_PmlContinuous::thirdEdge() const
  {
    return static_cast< PmlContinuous_PmlContinuous * >(_bnd[2]);
  }

  void PmlContinuous_PmlContinuous_PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlEdgeBnd(_corner);
    std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > cornerEdge = domains.getCornerEdge(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;

    _uPmlCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vC[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vC_" + orientation() + "_first", std::get<0>(pmlBnd), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vC_" + orientation() + "_second", std::get<1>(pmlBnd), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[2] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vC_" + orientation() + "_third", std::get<2>(pmlBnd), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uPml[3];
    uPml[0] = static_cast< PmlContinuous_PmlContinuous * >(_bnd[0])->getUPml();
    uPml[1] = static_cast< PmlContinuous_PmlContinuous * >(_bnd[1])->getUPml();
    uPml[2] = static_cast< PmlContinuous_PmlContinuous * >(_bnd[2])->getUPml();

    std::vector< PmlContinuous * > boundaries(6);
    for(unsigned int i = 0; i < 3; ++i) {
      boundaries[2 * i] = static_cast< PmlContinuous_PmlContinuous * >(_bnd[i])->firstBoundary();
      boundaries[2 * i + 1] = static_cast< PmlContinuous_PmlContinuous * >(_bnd[i])->secondBoundary();
    }
    std::sort(boundaries.begin(), boundaries.end());
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D[3];
    gmshfem::function::ScalarFunction< std::complex< double > > E[3];
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = boundaries[0]->pmlCoefficients(D[0], E[0], corner, C, kappaP, kappaS);
    boundaries[2]->pmlCoefficients(D[1], E[1], corner, C, kappaP, kappaS);
    boundaries[4]->pmlCoefficients(D[2], E[2], corner, C, kappaP, kappaS);
    
    formulation.integral(- gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0],  gmshfem::function::hadamard(D[1], D[2]))) * grad(dof(*_uPmlCorner)), grad(tf(*_uPmlCorner)), pmlCorner, gauss);
    formulation.integral(E[0] * E[1] * E[2] * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCorner, gauss);

    formulation.integral(dof(*_vC[0]), tf(*_uPmlCorner), std::get<0>(pmlBnd), gauss);
    formulation.integral(- dof(*_vC[0]), tf(*uPml[0]), std::get<0>(pmlBnd), gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[0]), std::get<0>(pmlBnd), gauss);
    formulation.integral(- dof(*uPml[0]), tf(*_vC[0]), std::get<0>(pmlBnd), gauss);

    formulation.integral(dof(*_vC[1]), tf(*_uPmlCorner), std::get<1>(pmlBnd), gauss);
    formulation.integral(- dof(*_vC[1]), tf(*uPml[1]), std::get<1>(pmlBnd), gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[1]), std::get<1>(pmlBnd), gauss);
    formulation.integral(- dof(*uPml[1]), tf(*_vC[1]), std::get<1>(pmlBnd), gauss);
    
    formulation.integral(dof(*_vC[2]), tf(*_uPmlCorner), std::get<2>(pmlBnd), gauss);
    formulation.integral(- dof(*_vC[2]), tf(*uPml[2]), std::get<2>(pmlBnd), gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[2]), std::get<2>(pmlBnd), gauss);
    formulation.integral(- dof(*uPml[2]), tf(*_vC[2]), std::get<2>(pmlBnd), gauss);
    
    // corner equation
    _vEdge[0] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vCornerEdge_" + orientation() + "_first", std::get<0>(cornerEdge), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vC[0]), tf(*_vEdge[0]), std::get<0>(cornerEdge), gauss);
    formulation.integral(dof(*_vEdge[0]), tf(*_vC[0]), std::get<0>(cornerEdge), gauss);

    formulation.integral(-dof(*_vC[2]), tf(*_vEdge[0]), std::get<0>(cornerEdge), gauss);
    formulation.integral(-dof(*_vEdge[0]), tf(*_vC[2]), std::get<0>(cornerEdge), gauss);

    formulation.integral(dof(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[0])->getV(0)), tf(*_vEdge[0]), std::get<0>(cornerEdge), gauss);
    formulation.integral(dof(*_vEdge[0]), tf(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[0])->getV(0)), std::get<0>(cornerEdge), gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[2])->getV(0)), tf(*_vEdge[0]), std::get<0>(cornerEdge), gauss);
    formulation.integral(-dof(*_vEdge[0]), tf(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[2])->getV(0)), std::get<0>(cornerEdge), gauss);

    _vEdge[1] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vCornerEdge_" + orientation() + "_second", std::get<1>(cornerEdge), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vC[1]), tf(*_vEdge[1]), std::get<1>(cornerEdge), gauss);
    formulation.integral(dof(*_vEdge[1]), tf(*_vC[1]), std::get<1>(cornerEdge), gauss);

    formulation.integral(-dof(*_vC[2]), tf(*_vEdge[1]), std::get<1>(cornerEdge), gauss);
    formulation.integral(-dof(*_vEdge[1]), tf(*_vC[2]), std::get<1>(cornerEdge), gauss);

    formulation.integral(dof(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[1])->getV(0)), tf(*_vEdge[1]), std::get<1>(cornerEdge), gauss);
    formulation.integral(dof(*_vEdge[1]), tf(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[1])->getV(0)), std::get<1>(cornerEdge), gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[2])->getV(1)), tf(*_vEdge[1]), std::get<1>(cornerEdge), gauss);
    formulation.integral(-dof(*_vEdge[1]), tf(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[2])->getV(1)), std::get<1>(cornerEdge), gauss);

    _vEdge[2] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vCornerEdge_" + orientation() + "_third", std::get<2>(cornerEdge), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vC[0]), tf(*_vEdge[2]), std::get<2>(cornerEdge), gauss);
    formulation.integral(dof(*_vEdge[2]), tf(*_vC[0]), std::get<2>(cornerEdge), gauss);

    formulation.integral(-dof(*_vC[1]), tf(*_vEdge[2]), std::get<2>(cornerEdge), gauss);
    formulation.integral(-dof(*_vEdge[2]), tf(*_vC[1]), std::get<2>(cornerEdge), gauss);

    formulation.integral(dof(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[0])->getV(1)), tf(*_vEdge[2]), std::get<2>(cornerEdge), gauss);
    formulation.integral(dof(*_vEdge[2]), tf(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[0])->getV(1)), std::get<2>(cornerEdge), gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[1])->getV(1)), tf(*_vEdge[2]), std::get<2>(cornerEdge), gauss);
    formulation.integral(-dof(*_vEdge[2]), tf(*static_cast< PmlContinuous_PmlContinuous * >(_bnd[1])->getV(1)), std::get<2>(cornerEdge), gauss);
    

    _vCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vEdge[0]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vEdge[0]), corner, gauss);
    
    formulation.integral(dof(*_vEdge[1]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vEdge[1]), corner, gauss);
    
    formulation.integral(dof(*_vEdge[2]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vEdge[2]), corner, gauss);
  }
  
  // **********************************
  // PML continuous symmetric _ PML continuous symmetric _ PML continuous symmetric _ simplify
  // **********************************

  PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify(const unsigned int corner, PmlContinuousSymmetric_PmlContinuousSymmetric *first, PmlContinuousSymmetric_PmlContinuousSymmetric *second, PmlContinuousSymmetric_PmlContinuousSymmetric *third) : Corner(corner, first, second, third)
  {
  }

  PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::~PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify()
  {
    for(int i = 0; i < 3; ++i) {
      delete _vC[i];
    }
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 2; ++j) {
        delete _vSymC[j][i];
      }
    }
    for(int i = 0; i < 3; ++i) {
      delete _vSymSymC[i];
    }
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::getV(const unsigned int i) const
  {
    return _vC[i];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::getVSym(const unsigned int i, const unsigned int j) const
  {
    return _vSymC[i][j];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::getVSymSym(const unsigned int i) const
  {
    return _vSymSymC[i];
  }

  PmlContinuousSymmetric_PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::firstEdge() const
  {
    return static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[0]);
  }

  PmlContinuousSymmetric_PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::secondEdge() const
  {
    return static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[1]);
  }
  
  PmlContinuousSymmetric_PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::thirdEdge() const
  {
    return static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[2]);
  }

  void PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlEdgeBnd(_corner);
    std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > cornerEdge = domains.getCornerEdge(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;

    std::string IlearnToCount[3] {"first", "second", "third"};
    gmshfem::domain::Domain pmlBnds[3] {std::get<0>(pmlBnd), std::get<1>(pmlBnd), std::get<2>(pmlBnd)};
    for(int i = 0; i < 3; ++i) {
      _vC[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vC_" + orientation() + "_" + IlearnToCount[i], pmlBnds[i], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    }
    
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 2; ++j) {
        _vSymC[j][i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymC_" + IlearnToCount[j] + "_" + orientation() + "_" + IlearnToCount[i], pmlBnds[i], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
      }
    }

    for(int i = 0; i < 3; ++i) {
      _vSymSymC[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymC_" + orientation() + "_" + IlearnToCount[i], pmlBnds[i], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    }


    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uPml[3];
    for(int i = 0; i < 3; ++i) {
      uPml[i] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->getUPml();
    }
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uSymPml[2][3];
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 2; ++j) {
        uSymPml[j][i] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->getUSymPml(j);
      }
    }
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uSymSymPml[3];
    for(int i = 0; i < 3; ++i) {
      uSymSymPml[i] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->getUSymSymPml();
    }
    
    std::vector< PmlContinuousSymmetric * > boundaries(6);
    for(unsigned int i = 0; i < 3; ++i) {
      boundaries[2 * i] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->firstBoundary();
      boundaries[2 * i + 1] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->secondBoundary();
    }
    std::sort(boundaries.begin(), boundaries.end());
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D[3];
    gmshfem::function::ScalarFunction< std::complex< double > > E[3];
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = boundaries[0]->pmlCoefficients(D[0], E[0], corner, C, kappaP, kappaS);
    boundaries[2]->pmlCoefficients(D[1], E[1], corner, C, fake, fake);
    boundaries[4]->pmlCoefficients(D[2], E[2], corner, C, fake, fake);
    gmshfem::function::ScalarFunction< std::complex< double > > Et = E[0] * E[1] * E[2];
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;

    const std::complex< double > im(0., 1.);
        
    for(unsigned int i = 0; i < 3; ++i) {
      formulation.integral(- dof(*_vC[i]), tf(*uPml[i]), pmlBnds[i], gauss);
      
      formulation.integral(dof(*_vC[i]), tf(*_vC[i]), pmlBnds[i], gauss);
      formulation.integral((im/(kappaP)) * In * Et * dof(*uPml[i]), tf(*_vC[i]), pmlBnds[i], gauss);
      formulation.integral((im/(kappaS)) * It * Et * dof(*uPml[i]), tf(*_vC[i]), pmlBnds[i], gauss);
    }
    
    for(unsigned int i = 0; i < 2; ++i) {
      for(unsigned int j = 0; j < 3; ++j) {
        formulation.integral(- dof(*_vSymC[i][j]), tf(*uSymPml[i][j]), pmlBnds[j], gauss);
        
        formulation.integral(dof(*_vSymC[i][j]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
        formulation.integral((im/(kappaP)) * In * Et * dof(*uSymPml[i][j]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
        formulation.integral((im/(kappaS)) * It * Et * dof(*uSymPml[i][j]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
      }
    }
    
    for(unsigned int i = 0; i < 3; ++i) {
      formulation.integral(- dof(*_vSymSymC[i]), tf(*uSymSymPml[i]), pmlBnds[i], gauss);
      
      formulation.integral(dof(*_vSymSymC[i]), tf(*_vSymSymC[i]), pmlBnds[i], gauss);
      formulation.integral((im/(kappaP)) * In * Et * dof(*uSymSymPml[i]), tf(*_vSymSymC[i]), pmlBnds[i], gauss);
      formulation.integral((im/(kappaS)) * It * Et * dof(*uSymSymPml[i]), tf(*_vSymSymC[i]), pmlBnds[i], gauss);
    }
  }
  
  // **********************************
  // PML continuous symmetric _ PML continuous symmetric _ PML continuous symmetric
  // **********************************

  PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric(const unsigned int corner, PmlContinuousSymmetric_PmlContinuousSymmetric *first, PmlContinuousSymmetric_PmlContinuousSymmetric *second, PmlContinuousSymmetric_PmlContinuousSymmetric *third) : Corner(corner, first, second, third)
  {
    typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree4 >::Object tmpX, tmpY, tmpZ;
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(i == k && j == l) {
              tmpX(i,j)(k,l) = 1.;
              tmpY(i,j)(k,l) = 1.;
              tmpZ(i,j)(k,l) = 1.;
            }
            else {
              tmpX(i,j)(k,l) = 0.;
              tmpY(i,j)(k,l) = 0.;
              tmpZ(i,j)(k,l) = 0.;
            }
          }
        }
      }
    }
    for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        for(int k = 0; k < 3; ++k) {
          for(int l = 0; l < 3; ++l) {
            if(l == 0) {
              tmpX(i,j)(k,l) *= -1.; // symmetry of grad_x
            }
            else if(l == 1) {
              tmpY(i,j)(k,l) *= -1.; // symmetry of grad_y
            }
            else if(l == 2) {
              tmpZ(i,j)(k,l) *= -1.; // symmetry of grad_z
            }
          }
        }
      }
    }
    _symX = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpX);
    _symY = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpY);
    _symZ = gmshfem::function::TensorFunction< std::complex< double >, 4 >(tmpZ);
  }

  PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::~PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric()
  {
    delete _uPmlCorner;
    for(int i = 0; i < 3; ++i) {
      delete _uSymPmlCorner[i];
      delete _uSymSymPmlCorner[i];
    }
    delete _uSymSymSymPmlCorner;
    
    for(int i = 0; i < 3; ++i) {
      delete _vC[i];
      for(int j = 0; j < 3; ++j) {
        delete _vSymC[i][j];
        delete _vSymSymC[i][j];
      }
      delete _vSymSymSymC[i];
    }
    
    for(int i = 0; i < 3; ++i) {
      delete _vEdge[i];
      for(int j = 0; j < 3; ++j) {
        delete _vSymEdge[i][j];
        delete _vSymSymEdge[i][j];
      }
    }
    
    delete _vCorner;
    for(int i = 0; i < 3; ++i) {
      delete _vSymCorner[i];
      delete _vSymSymCorner[i];
    }
  }

  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::getV(const unsigned int i) const
  {
    return _vC[i];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::getVSym(const unsigned int i, const unsigned int j) const
  {
    return _vSymC[i][j];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::getVSymSym(const unsigned int i, const unsigned int j) const
  {
    return _vSymSymC[i][j];
  }
  
  gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::getVSymSymSym(const unsigned int i) const
  {
    return _vSymSymSymC[i];
  }

  PmlContinuousSymmetric_PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::firstEdge() const
  {
    return static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[0]);
  }

  PmlContinuousSymmetric_PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::secondEdge() const
  {
    return static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[1]);
  }
  
  PmlContinuousSymmetric_PmlContinuousSymmetric *PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::thirdEdge() const
  {
    return static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[2]);
  }

  void PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlEdgeBnd(_corner);
    std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > cornerEdge = domains.getCornerEdge(_corner);
    gmshfem::domain::Domain pmlCornerInf = domains.getPmlCornerInf(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > C = parameters.getC();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP = parameters.getKappaP();
    gmshfem::function::ScalarFunction< std::complex< double > > kappaS = parameters.getKappaS();
    gmshfem::function::ScalarFunction< std::complex< double > > fake = 1.;

    std::string IlearnToCount [3] {"first", "second", "third"};
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > syms[8][3] {
      {_symY, _symX, _symZ},
      {_symX, _symY, _symZ},
      {_symY, _symX, _symZ},
      {_symX, _symY, _symZ},
      {_symY, _symX, _symZ},
      {_symX, _symY, _symZ},
      {_symY, _symX, _symZ},
      {_symX, _symY, _symZ},
    };
    gmshfem::function::TensorFunction< std::complex< double >, 4 > hxz = gmshfem::function::hadamard(_symX, _symZ);
    gmshfem::function::TensorFunction< std::complex< double >, 4 > hyz = gmshfem::function::hadamard(_symY, _symZ);
    gmshfem::function::TensorFunction< std::complex< double >, 4 > hxy = gmshfem::function::hadamard(_symX, _symY);
    const gmshfem::function::TensorFunction< std::complex< double >, 4 > symsSyms[8][3] {
      {hxz, hyz, hxy},
      {hyz, hxz, hxy},
      {hxz, hyz, hxy},
      {hyz, hxz, hxy},
      {hxz, hyz, hxy},
      {hyz, hxz, hxy},
      {hxz, hyz, hxy},
      {hyz, hxz, hxy},
    };
    gmshfem::function::TensorFunction< std::complex< double >, 4 > hxyz = gmshfem::function::hadamard(_symX, gmshfem::function::hadamard(_symY, _symZ));

    const std::pair< int, int > cornerEdgeLink[3] {std::pair< int, int >(0,2), std::pair< int, int >(1,2), std::pair< int, int >(0,1)};
    const std::pair< int, int > cornerEdgeLinkV[3] {std::pair< int, int >(0,0), std::pair< int, int >(0,1), std::pair< int, int >(1,1)};
    const std::pair< int, int > firstOrderLinkEdge[3] {std::pair< int, int>(0,2), std::pair< int, int>(1,2), std::pair< int, int>(0,1)};
    const std::pair< int, int > firstOrderLinkBnd[3] {std::pair< int, int>(1,1), std::pair< int, int>(1,0), std::pair< int, int>(0,0)};
    const std::pair< int, int > secondOrderLinkEdge[3] {std::pair< int, int>(1,2), std::pair< int, int>(0,2), std::pair< int, int>(0,1)};
    const std::pair< int, int > secondOrderLinkBnd[3] {std::pair< int, int>(0,1), std::pair< int, int>(1,0), std::pair< int, int>(1,1)};
    
    gmshfem::domain::Domain pmlBnds[3] {std::get<0>(pmlBnd), std::get<1>(pmlBnd), std::get<2>(pmlBnd)};
    gmshfem::domain::Domain cornerEdges[3] {std::get<0>(cornerEdge), std::get<1>(cornerEdge), std::get<2>(cornerEdge)};
    
    _uPmlCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    for(int i = 0; i < 3; ++i) {
      _uSymPmlCorner[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uSymPmlCorner_" + IlearnToCount[i] + "_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
      _uSymSymPmlCorner[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uSymSymPmlCorner_" + IlearnToCount[i] + "_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    }
    _uSymSymSymPmlCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("uSymSymSymPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    
    for(int i = 0; i < 3; ++i) {
      _vC[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vC_" + orientation() + "_" + IlearnToCount[i], pmlBnds[i], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
      for(int j = 0; j < 3; ++j) {
        _vSymC[i][j] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymC_" + IlearnToCount[i] + "_" + orientation() + "_" + IlearnToCount[j], pmlBnds[j], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
        _vSymSymC[i][j] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymC_" + IlearnToCount[i] + "_" + orientation() + "_" + IlearnToCount[j], pmlBnds[j], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
      }
      _vSymSymSymC[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymSymC_" + orientation() + "_first", pmlBnds[i], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    }

    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uPml[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uSymPml[2][3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *uSymSymPml[3];
    for(int i = 0; i < 3; ++i) {
      uPml[i] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->getUPml();
      for(int j = 0; j < 2; ++j) {
        uSymPml[j][i] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->getUSymPml(j);
      }
      uSymSymPml[i] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->getUSymSymPml();
    }
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > I = gmshfem::function::identity< std::complex< double > >();
    gmshfem::function::TensorFunction< std::complex< double > > In = gmshfem::function::dyadic(n,n);
    gmshfem::function::TensorFunction< std::complex< double > > It = I - In;
    
    const std::complex< double > im(0., 1.);

    std::vector< PmlContinuousSymmetric * > boundaries(6);
    for(unsigned int i = 0; i < 3; ++i) {
      boundaries[2 * i] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->firstBoundary();
      boundaries[2 * i + 1] = static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[i])->secondBoundary();
    }
    std::sort(boundaries.begin(), boundaries.end());
    gmshfem::function::TensorFunction< std::complex< double >, 4 > D[3];
    gmshfem::function::ScalarFunction< std::complex< double > > E[3];
    gmshfem::function::TensorFunction< std::complex< double >, 4 > CMod = boundaries[0]->pmlCoefficients(D[0], E[0], corner, C, kappaP, kappaS);
    boundaries[2]->pmlCoefficients(D[1], E[1], corner, C, kappaP, kappaS);
    boundaries[4]->pmlCoefficients(D[2], E[2], corner, C, kappaP, kappaS);
    gmshfem::function::TensorFunction< std::complex< double >, 4 > Dt = gmshfem::function::hadamard(CMod, gmshfem::function::hadamard(D[0],  gmshfem::function::hadamard(D[1], D[2])));
    gmshfem::function::ScalarFunction< std::complex< double > > Et = E[0] * E[1] * E[2];
    
    
    
    formulation.integral(- Dt * grad(dof(*_uPmlCorner)), grad(tf(*_uPmlCorner)), pmlCorner, gauss);
    formulation.integral(Et * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCorner, gauss);
    
    formulation.integral((im/kappaP) * In * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCornerInf, gauss);
    formulation.integral((im/kappaS) * It * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCornerInf, gauss);

    for(int i = 0; i < 3; ++i) {
      formulation.integral(dof(*_vC[i]), tf(*_uPmlCorner), pmlBnds[i], gauss);
      formulation.integral(- dof(*_vC[i]), tf(*uPml[i]), pmlBnds[i], gauss);

      formulation.integral(dof(*_uPmlCorner), tf(*_vC[i]), pmlBnds[i], gauss);
      formulation.integral(- dof(*uPml[i]), tf(*_vC[i]), pmlBnds[i], gauss);
    }
    
    // corner equation
    for(int i = 0; i < 3; ++i) {
      _vEdge[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vCornerEdge_" + orientation() + "_" + IlearnToCount[i], cornerEdges[i], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
      formulation.integral(dof(*_vC[cornerEdgeLink[i].first]), tf(*_vEdge[i]), cornerEdges[i], gauss);
      formulation.integral(dof(*_vEdge[i]), tf(*_vC[cornerEdgeLink[i].first]), cornerEdges[i], gauss);

      formulation.integral(-dof(*_vC[cornerEdgeLink[i].second]), tf(*_vEdge[i]), cornerEdges[i], gauss);
      formulation.integral(-dof(*_vEdge[i]), tf(*_vC[cornerEdgeLink[i].second]), cornerEdges[i], gauss);

      formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[i].first])->getV(cornerEdgeLinkV[i].first)), tf(*_vEdge[i]), cornerEdges[i], gauss);
      formulation.integral(dof(*_vEdge[i]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[i].first])->getV(cornerEdgeLinkV[i].first)), cornerEdges[i], gauss);

      formulation.integral(-dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[i].second])->getV(cornerEdgeLinkV[i].second)), tf(*_vEdge[i]), cornerEdges[i], gauss);
      formulation.integral(-dof(*_vEdge[i]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[i].second])->getV(cornerEdgeLinkV[i].second)), cornerEdges[i], gauss);
    }

    _vCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    for(int i = 0; i < 3; ++i) {
      formulation.integral(dof(*_vEdge[i]), tf(*_vCorner), corner, gauss);
      formulation.integral(dof(*_vCorner), tf(*_vEdge[i]), corner, gauss);
    }
    
    
    
    // first order symmetric fields (sym), u)x, u)y, u)z
    for(int i = 0; i < 3; ++i) {
      formulation.integral(- Dt * syms[_corner][i] * grad(dof(*_uSymPmlCorner[i])), syms[_corner][i] * grad(tf(*_uSymPmlCorner[i])), pmlCorner, gauss);
      formulation.integral(Et * dof(*_uSymPmlCorner[i]), tf(*_uSymPmlCorner[i]), pmlCorner, gauss);
      
      formulation.integral((im/kappaP) * In * dof(*_uSymPmlCorner[i]), tf(*_uSymPmlCorner[i]), pmlCornerInf, gauss);
      formulation.integral((im/kappaS) * It * dof(*_uSymPmlCorner[i]), tf(*_uSymPmlCorner[i]), pmlCornerInf, gauss);


      for(int j = 0; j < 3; ++j) {
        if(j == firstOrderLinkEdge[i].first) {
          formulation.integral(dof(*_vSymC[i][j]), tf(*_uSymPmlCorner[i]), pmlBnds[j], gauss);
          formulation.integral(- dof(*_vSymC[i][j]), tf(*uSymPml[firstOrderLinkBnd[i].first][j]), pmlBnds[j], gauss);

          formulation.integral(dof(*_uSymPmlCorner[i]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
          formulation.integral(- dof(*uSymPml[firstOrderLinkBnd[i].first][j]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
        }
        else if(j == firstOrderLinkEdge[i].second) {
          formulation.integral(dof(*_vSymC[i][j]), tf(*_uSymPmlCorner[i]), pmlBnds[j], gauss);
          formulation.integral(- dof(*_vSymC[i][j]), tf(*uSymPml[firstOrderLinkBnd[i].second][j]), pmlBnds[j], gauss);

          formulation.integral(dof(*_uSymPmlCorner[i]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
          formulation.integral(- dof(*uSymPml[firstOrderLinkBnd[i].second][j]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
        }
        else {
          formulation.integral(dof(*_vSymC[i][j]), tf(*_uSymPmlCorner[i]), pmlBnds[j], gauss);

          formulation.integral(dof(*_uSymPmlCorner[i]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
          formulation.integral(- dof(*uPml[j]), tf(*_vSymC[i][j]), pmlBnds[j], gauss);
        }
      }
      
      // corner equation
      for(int j = 0; j < 3; ++j) {
        _vSymEdge[i][j] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymCornerEdge_" + IlearnToCount[i] + "_" + orientation() + "_" + IlearnToCount[j], cornerEdges[j], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
        formulation.integral(dof(*_vSymC[i][cornerEdgeLink[j].first]), tf(*_vSymEdge[i][j]), cornerEdges[j], gauss);
        formulation.integral(dof(*_vSymEdge[i][j]), tf(*_vSymC[i][cornerEdgeLink[j].first]), cornerEdges[j], gauss);

        formulation.integral(-dof(*_vSymC[i][cornerEdgeLink[j].second]), tf(*_vSymEdge[i][j]), cornerEdges[j], gauss);
        formulation.integral(-dof(*_vSymEdge[i][j]), tf(*_vSymC[i][cornerEdgeLink[j].second]), cornerEdges[j], gauss);

        if(cornerEdgeLink[j].first == firstOrderLinkEdge[i].first) {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSym(firstOrderLinkBnd[i].first, cornerEdgeLinkV[j].first)), tf(*_vSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSym(firstOrderLinkBnd[i].first, cornerEdgeLinkV[j].first)), cornerEdges[j], gauss);
        }
        else if(cornerEdgeLink[j].first == firstOrderLinkEdge[i].second) {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSym(firstOrderLinkBnd[i].second, cornerEdgeLinkV[j].first)), tf(*_vSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSym(firstOrderLinkBnd[i].second, cornerEdgeLinkV[j].first)), cornerEdges[j], gauss);
        }
        else {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getV(cornerEdgeLinkV[j].first)), tf(*_vSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getV(cornerEdgeLinkV[j].first)), cornerEdges[j], gauss);
        }
        
        if(cornerEdgeLink[j].second == firstOrderLinkEdge[i].first) {
          formulation.integral(-dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSym(firstOrderLinkBnd[i].first, cornerEdgeLinkV[j].second)), tf(*_vSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(-dof(*_vSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSym(firstOrderLinkBnd[i].first, cornerEdgeLinkV[j].second)), cornerEdges[j], gauss);
        }
        else if(cornerEdgeLink[j].second == firstOrderLinkEdge[i].second) {
          formulation.integral(-dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSym(firstOrderLinkBnd[i].second, cornerEdgeLinkV[j].second)), tf(*_vSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(-dof(*_vSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSym(firstOrderLinkBnd[i].second, cornerEdgeLinkV[j].second)), cornerEdges[j], gauss);
        }
        else {
          formulation.integral(-dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getV(cornerEdgeLinkV[j].second)), tf(*_vSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(-dof(*_vSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getV(cornerEdgeLinkV[j].second)), cornerEdges[j], gauss);
        }
      }

      _vSymCorner[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymCorner_" + IlearnToCount[i] + "_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

      for(int j = 0; j < 3; ++j) {
        formulation.integral(dof(*_vSymEdge[i][j]), tf(*_vSymCorner[i]), corner, gauss);
        formulation.integral(dof(*_vSymCorner[i]), tf(*_vSymEdge[i][j]), corner, gauss);
      }
    }
    
    
    
    // second order symmetric fields (symSym), u)xy, u)xz, u)yz
    for(int i = 0; i < 3; ++i) {
      formulation.integral(- Dt * symsSyms[_corner][i] * grad(dof(*_uSymSymPmlCorner[i])), symsSyms[_corner][i] * grad(tf(*_uSymSymPmlCorner[i])), pmlCorner, gauss);
      formulation.integral(Et * dof(*_uSymSymPmlCorner[i]), tf(*_uSymSymPmlCorner[i]), pmlCorner, gauss);
      
      formulation.integral((im/kappaP) * In * dof(*_uSymSymPmlCorner[i]), tf(*_uSymSymPmlCorner[i]), pmlCornerInf, gauss);
      formulation.integral((im/kappaS) * It * dof(*_uSymSymPmlCorner[i]), tf(*_uSymSymPmlCorner[i]), pmlCornerInf, gauss);


      for(int j = 0; j < 3; ++j) {
        if(j == secondOrderLinkEdge[i].first) {
          formulation.integral(dof(*_vSymSymC[i][j]), tf(*_uSymSymPmlCorner[i]), pmlBnds[j], gauss);

          formulation.integral(dof(*_uSymSymPmlCorner[i]), tf(*_vSymSymC[i][j]), pmlBnds[j], gauss);
          formulation.integral(- dof(*uSymPml[secondOrderLinkBnd[i].first][j]), tf(*_vSymSymC[i][j]), pmlBnds[j], gauss);
        }
        else if(j == secondOrderLinkEdge[i].second) {
          formulation.integral(dof(*_vSymSymC[i][j]), tf(*_uSymSymPmlCorner[i]), pmlBnds[j], gauss);

          formulation.integral(dof(*_uSymSymPmlCorner[i]), tf(*_vSymSymC[i][j]), pmlBnds[j], gauss);
          formulation.integral(- dof(*uSymPml[secondOrderLinkBnd[i].second][j]), tf(*_vSymSymC[i][j]), pmlBnds[j], gauss);
        }
        else { // j == i
          formulation.integral(dof(*_vSymSymC[i][i]), tf(*_uSymSymPmlCorner[i]), pmlBnds[i], gauss);
          formulation.integral(- dof(*_vSymSymC[i][i]), tf(*uSymSymPml[i]), pmlBnds[i], gauss);

          formulation.integral(dof(*_uSymSymPmlCorner[i]), tf(*_vSymSymC[i][i]), pmlBnds[i], gauss);
          formulation.integral(- dof(*uSymSymPml[i]), tf(*_vSymSymC[i][i]), pmlBnds[i], gauss);
        }
      }
      
      // corner equation
      for(int j = 0; j < 3; ++j) {
        _vSymSymEdge[i][j] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymCornerEdge_" + IlearnToCount[i] + "_" + orientation() + "_" + IlearnToCount[j], cornerEdges[j], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
        formulation.integral(dof(*_vSymSymC[i][cornerEdgeLink[j].first]), tf(*_vSymSymEdge[i][j]), cornerEdges[j], gauss);
        formulation.integral(dof(*_vSymSymEdge[i][j]), tf(*_vSymSymC[i][cornerEdgeLink[j].first]), cornerEdges[j], gauss);

        formulation.integral(-dof(*_vSymSymC[i][cornerEdgeLink[j].second]), tf(*_vSymSymEdge[i][j]), cornerEdges[j], gauss);
        formulation.integral(-dof(*_vSymSymEdge[i][j]), tf(*_vSymSymC[i][cornerEdgeLink[j].second]), cornerEdges[j], gauss);
        
        if(cornerEdgeLink[j].first == secondOrderLinkEdge[i].first) {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSym(secondOrderLinkBnd[i].first, cornerEdgeLinkV[j].first)), tf(*_vSymSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSym(secondOrderLinkBnd[i].first, cornerEdgeLinkV[j].first)), cornerEdges[j], gauss);
        }
        else if(cornerEdgeLink[j].first == secondOrderLinkEdge[i].second) {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSym(secondOrderLinkBnd[i].second, cornerEdgeLinkV[j].first)), tf(*_vSymSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSym(secondOrderLinkBnd[i].second, cornerEdgeLinkV[j].first)), cornerEdges[j], gauss);
        }
        else {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSymSym(cornerEdgeLinkV[j].first)), tf(*_vSymSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].first])->getVSymSym(cornerEdgeLinkV[j].first)), cornerEdges[j], gauss);
        }
        
        if(cornerEdgeLink[j].second == secondOrderLinkEdge[i].first) {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSym(secondOrderLinkBnd[i].first, cornerEdgeLinkV[j].second)), tf(*_vSymSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSym(secondOrderLinkBnd[i].first, cornerEdgeLinkV[j].second)), cornerEdges[j], gauss);
        }
        else if(cornerEdgeLink[j].second == secondOrderLinkEdge[i].second) {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSym(secondOrderLinkBnd[i].second, cornerEdgeLinkV[j].second)), tf(*_vSymSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSym(secondOrderLinkBnd[i].second, cornerEdgeLinkV[j].second)), cornerEdges[j], gauss);
        }
        else {
          formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSymSym(cornerEdgeLinkV[j].second)), tf(*_vSymSymEdge[i][j]), cornerEdges[j], gauss);
          formulation.integral(dof(*_vSymSymEdge[i][j]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[j].second])->getVSymSym(cornerEdgeLinkV[j].second)), cornerEdges[j], gauss);
        }
      }
      
      _vSymSymCorner[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymCorner_" + IlearnToCount[i] + "_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

      for(int j = 0; j < 3; ++j) {
        formulation.integral(dof(*_vSymSymEdge[i][j]), tf(*_vSymSymCorner[i]), corner, gauss);
        formulation.integral(dof(*_vSymSymCorner[i]), tf(*_vSymSymEdge[i][j]), corner, gauss);
      }
    }



    // third order symmetric fields (symSymSym), u)xyz
    formulation.integral(- Dt * hxyz * grad(dof(*_uSymSymSymPmlCorner)), hxyz * grad(tf(*_uSymSymSymPmlCorner)), pmlCorner, gauss);
    formulation.integral(Et * dof(*_uSymSymSymPmlCorner), tf(*_uSymSymSymPmlCorner), pmlCorner, gauss);

    formulation.integral((im/kappaP) * In * dof(*_uSymSymSymPmlCorner), tf(*_uSymSymSymPmlCorner), pmlCornerInf, gauss);
    formulation.integral((im/kappaS) * It * dof(*_uSymSymSymPmlCorner), tf(*_uSymSymSymPmlCorner), pmlCornerInf, gauss);

    for(int i = 0; i < 3; ++i) {
      formulation.integral(dof(*_vSymSymSymC[i]), tf(*_uSymSymSymPmlCorner), pmlBnds[i], gauss);

      formulation.integral(dof(*_uSymSymSymPmlCorner), tf(*_vSymSymSymC[i]), pmlBnds[i], gauss);
      formulation.integral(- dof(*uSymSymPml[i]), tf(*_vSymSymSymC[i]), pmlBnds[i], gauss);
    }

    // corner equation
    for(int i = 0; i < 3; ++i) {
      _vSymSymSymEdge[i] = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymSymCornerEdge_" + orientation() + "_" + IlearnToCount[i], cornerEdges[i], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
      formulation.integral(dof(*_vSymSymSymC[cornerEdgeLink[i].first]), tf(*_vSymSymSymEdge[i]), cornerEdges[i], gauss);
      formulation.integral(dof(*_vSymSymSymEdge[i]), tf(*_vSymSymSymC[cornerEdgeLink[i].first]), cornerEdges[i], gauss);

      formulation.integral(-dof(*_vSymSymSymC[cornerEdgeLink[i].second]), tf(*_vSymSymSymEdge[i]), cornerEdges[i], gauss);
      formulation.integral(-dof(*_vSymSymSymEdge[i]), tf(*_vSymSymSymC[cornerEdgeLink[i].second]), cornerEdges[i], gauss);

      formulation.integral(dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[i].first])->getVSymSym(cornerEdgeLinkV[i].first)), tf(*_vSymSymSymEdge[i]), cornerEdges[i], gauss);
      formulation.integral(dof(*_vSymSymSymEdge[i]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[i].first])->getVSymSym(cornerEdgeLinkV[i].first)), cornerEdges[i], gauss);

      formulation.integral(-dof(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[i].second])->getVSymSym(cornerEdgeLinkV[i].second)), tf(*_vSymSymSymEdge[i]), cornerEdges[i], gauss);
      formulation.integral(-dof(*_vSymSymSymEdge[i]), tf(*static_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(_bnd[cornerEdgeLink[i].second])->getVSymSym(cornerEdgeLinkV[i].second)), cornerEdges[i], gauss);
    }

    _vSymSymSymCorner = new gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("vSymSymSymCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    for(int i = 0; i < 3; ++i) {
      formulation.integral(dof(*_vSymSymSymEdge[i]), tf(*_vSymSymSymCorner), corner, gauss);
      formulation.integral(dof(*_vSymSymSymCorner), tf(*_vSymSymSymEdge[i]), corner, gauss);
    }
  }


}
