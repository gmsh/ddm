#include <numeric>
#include "gmsh.h"
#include "gmshddm/Formulation.h"
#include "gmshddm/GmshDdm.h"

using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;

static const double Pi = std::atan(1) * 4;
static const std::complex< double > J(0., 1.);

void waveguide(unsigned int nDom, double l, double L, double lc)
{
  if(nDom < 2)
    throw gmshfem::common::Exception("nDom should be at least equal to 2");

  gmsh::model::add("waveguide");

  const double domainSize = L / nDom;

  std::vector< std::vector< int > > borders(nDom);
  std::vector< int > omegas(nDom);
  std::vector< int > sigmas(nDom - 1);

  int p[2];

  p[0] = gmsh::model::geo::addPoint(0., l, 0., lc);
  p[1] = gmsh::model::geo::addPoint(0., 0., 0., lc);

  int line = gmsh::model::geo::addLine(p[0], p[1]);
  gmsh::model::addPhysicalGroup(1, {line}, 1);
  gmsh::model::setPhysicalName(1, 1, "gammaD");

  for(unsigned int i = 0; i < nDom; ++i) {
    std::vector< std::pair< int, int > > extrudeEntities;
    gmsh::model::geo::extrude({std::make_pair(1, line)},
                              domainSize,
                              0.,
                              0.,
                              extrudeEntities,
                              {static_cast< int >((domainSize) / lc)},
                              std::vector< double >(), false);
    line = extrudeEntities[0].second;
    omegas[i] = extrudeEntities[1].second;
    borders[i].push_back(extrudeEntities[2].second);
    borders[i].push_back(extrudeEntities[3].second);
    if(i < nDom-1)
      sigmas[i] = line;
  }

  gmsh::model::addPhysicalGroup(1, {line}, 2);
  gmsh::model::setPhysicalName(1, 2, "gammaI");

  for(unsigned int i = 0; i < borders.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, borders[i], 100 + i);
    gmsh::model::setPhysicalName(1, 100 + i, "border_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < omegas.size(); ++i) {
    gmsh::model::addPhysicalGroup(2, {omegas[i]}, i + 1);
    gmsh::model::setPhysicalName(2, i + 1, "omega_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < sigmas.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, {sigmas[i]}, 200 + i);
    gmsh::model::setPhysicalName(1, 200 + i, "sigma_" +
                                             std::to_string(i) + "_" +
                                             std::to_string(i + 1));
  }

  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate();
  gmsh::model::mesh::setOrder(1);
  // gmsh::write("m.msh");
}

void subdomain(int nDom,
               Subdomain &omega,
               Subdomain &gammaI,
               Subdomain &gammaD,
               Subdomain &wall,
               Interface &sigma,
               std::vector< std::vector< unsigned int > > &neighbourhood)
{
  // Computational (sub)domain(s)
  for(int i = 0; i < nDom; i++){
    omega(i) = Domain(2, i + 1);
    if(i == nDom-1)
      gammaI(i) = Domain(1, 2);
    if(i == 0)
      gammaD(i) = Domain(1, 1);
    wall(i) = Domain(1, 100 + i);

    if(i != 0)
      sigma(i, i - 1) = Domain(1, 200 + i - 1);
    if(i != nDom-1)
      sigma(i, i + 1) = Domain(1, 200 + i);
  }

  // Neighbourhood
  for(int i = 0; i < nDom; i++){
    if(i != 0)
      neighbourhood[i].push_back(i - 1);
    if(i != nDom-1)
      neighbourhood[i].push_back(i + 1);
  }
}

void helmholtz(int nDom,
               const Subdomain &omega,
               const Subdomain &gammaI,
               const Subdomain &gammaD,
               const Subdomain &wall,
               const Interface &sigma,
               const std::vector< std::vector< unsigned int > > &neighbourhood,
               int p,
               int gauss,
               double k,
               const std::string &tc,
               const std::complex< double > &kInf,
               const ScalarFunction< std::complex< double > > &fSrc,
               SubdomainField< std::complex< double >, Form::Form0 > &u)
{
  // For now, just oo0
  if(tc != "oo0")
    throw gmshfem::common::Exception("Only oo0 for now ;)");

  // Fields
  FunctionSpaceTypeForm0 HGrad = FunctionSpaceTypeForm0::HierarchicalH1;
  u = SubdomainField< std::complex< double >, Form::Form0 >
    ("u", omega|gammaD|gammaI|wall|sigma, HGrad, p);
  InterfaceField< std::complex< double >, Form::Form0 >
    g("g", sigma, HGrad, p);
  SubdomainField< std::complex< double >, Form::Form0 >
    lambda("lambda", gammaD, HGrad, p);

  for(size_t i = 0; i < neighbourhood.size(); i++)
    u(i).addConstraint(wall(i), 0); // Soft walls

  // Formulation
  gmshddm::problem::Formulation< std::complex< double > > form("Helmholtz",
                                                               neighbourhood);
  ScalarFunction< std::complex< double > > fSrcDD = form.physicalSource(fSrc);
  form.addInterfaceField(g);
  std::string quad = "Gauss" + std::to_string(gauss);

  for(size_t i = 0; i < neighbourhood.size(); i++){
    form(i).integral(     d(dof(u(i))), d(tf(u(i))), omega(i), quad);
    form(i).integral(-k*k * dof(u(i)),    tf(u(i)),  omega(i), quad);

    form(i).integral(-J*kInf * dof(u(i)), tf(u(i)), gammaI(i), quad);

    form(i).integral(dof(lambda(i)), tf(u(i)), gammaD(i), quad);
    form(i).integral(dof(u(i)), tf(lambda(i)), gammaD(i), quad);
    form(i).integral(-fSrcDD,   tf(lambda(i)), gammaD(i), quad);

    for(size_t jj = 0; jj < neighbourhood[i].size(); jj++){
      const unsigned int j = neighbourhood[i][jj];
      ScalarFunction< std::complex< double > >
        gInJI = form.artificialSource(g(j, i));

      form(i).integral(-gInJI, tf(u(i)), sigma(i, j), quad);
      form(i).integral(-J*k * dof(u(i)), tf(u(i)), sigma(i, j), quad);
    }

    for(size_t jj = 0; jj < neighbourhood[i].size(); jj++){
      const unsigned int j = neighbourhood[i][jj];
      ScalarFunction< std::complex< double > >
        gInJI = form.artificialSource(g(j, i));

      form(i, j).integral(dof(g(i, j)),   tf(g(i, j)), sigma(i, j), quad);
      form(i, j).integral(gInJI,          tf(g(i, j)), sigma(i, j), quad);
      form(i, j).integral(+2.*J*k * u(i), tf(g(i, j)), sigma(i, j), quad);
    }
  }

  // Solve
  form.pre();
  form.solve("gmres", 1e-9, 100);
}

void analytic(const std::complex< double > &kx,
              const std::complex< double > &ky,
              ScalarFunction< std::complex< double > > &u)
{
  u = sin(ky   * y< std::complex<double> >()) *
      exp(J*kx * x< std::complex<double> >());
}

double l2(int nDom,
          int gauss,
          const SubdomainField< std::complex< double >, Form::Form0 > &uFem,
          const ScalarFunction< std::complex< double > > &uAna,
          const Subdomain &omega)
{
  std::string G = "Gauss" + std::to_string(gauss);
  std::vector< double > error(nDom);
  for(int i = 0; i < nDom; i++){
    double N = std::real(integrate(pow(abs(uAna - uFem(i)), 2), omega(i), G));
    double D = std::real(integrate(pow(abs(uAna),           2), omega(i), G));
    error[i] = std::sqrt(N/D);
  }

  return std::accumulate(error.begin(), error.end(), 0.);
}

int main(int argc, char **argv)
{
  // Init //
  GmshDdm gmshDdm(argc, argv);

  // Parameters: transmission condition, number of subdomains, wavenumber, //
  // mesh refinement levels, FE order refinement level, mesh refinement    //
  // offset, FE order offset and tolerance for hp-convergence              //
  std::string tc = "oo0"; gmshDdm.userDefinedParameter(tc,    "tc");
  int nDom = 2;           gmshDdm.userDefinedParameter(nDom,  "nDom");
  double k = 31;          gmshDdm.userDefinedParameter(k,     "k");
  int lvlH = 4;           gmshDdm.userDefinedParameter(lvlH,  "lvlH");
  int lvlP = 4;           gmshDdm.userDefinedParameter(lvlP,  "lvlP");
  int hOff = 2;           gmshDdm.userDefinedParameter(hOff,  "hOff");
  int pOff = 2;           gmshDdm.userDefinedParameter(pOff,  "pOff");
  double hpTol = 2e-3;    gmshDdm.userDefinedParameter(hpTol, "hpTol");

  // Dimensions //
  double L = 1;
  double H = 0.5;

  // Mode (2D) //
  int mode = 1;
  std::complex<double> ky = mode * Pi / H;
  std::complex<double> kx = std::sqrt(k*k - ky*ky);

  // Source and 0th order ABC //
  std::complex< double > kInf = kx;
  ScalarFunction< std::complex< double > >
    fSrc = sin(ky  * y< std::complex<double> >());

  // Init error matrix: each line is a FE order, each column is a mesh size //
  std::vector< std::vector< double > > error(lvlP);
  for(int pp = 0; pp < lvlP; pp++)
    error[pp].resize(lvlH);

  // Mesh size (in points per wavelength)
  std::vector< double > h(lvlH);
  for(int hh = 0; hh < lvlH; hh++)
    h[hh] = std::pow(2, hh+hOff);

  // Tests //
  msg::info << "# Start test" << msg::endl;
  for(int hh = 0; hh < lvlH; hh++){
    // Info
    msg::info << "  * h: " << h[hh] << msg::endl;

    // Mesh
    double lc = (2*Pi/k) / h[hh];
    waveguide(nDom, H, L, lc);

    // Computational (sub)domain(s)
    Subdomain omega(nDom);
    Subdomain gammaI(nDom);
    Subdomain gammaD(nDom);
    Subdomain wall(nDom);
    Interface sigma(nDom);
    std::vector< std::vector< unsigned int > > neighbourhood(nDom);
    subdomain(nDom, omega, gammaI, gammaD, wall, sigma, neighbourhood);

    // Solve for each p and compute error
    for(int pp = 0; pp < lvlP; pp++){
      // Order
      int p = pp+pOff;

      // Info
      msg::info << "    + p: " << p << msg::endl;

      // FEM+OSM
      SubdomainField< std::complex< double >, Form::Form0 > u;
      helmholtz(nDom, omega, gammaI, gammaD, wall, sigma, neighbourhood,
                p, p*2, k, tc, kInf, fSrc, u);

      // Reference solution
      ScalarFunction< std::complex< double > > uAna;
      analytic(kx, ky, uAna);

      // Error
      error[pp][hh] = l2(nDom, p*4, u, uAna, omega);
    }
  }

  // Slopes //
  std::vector< std::vector< double > > slope(lvlP);
  for(int pp = 0; pp < lvlP; pp++)
    slope[pp].resize(lvlH-1);

  for(int pp = 0; pp < lvlP; pp++)
    for(int hh = 0; hh < lvlH-1; hh++)
      slope[pp][hh] = (std::log(error[pp][hh+1]) - std::log(error[pp][hh])) /
                      (std::log(h[hh+1])         - std::log(h[hh]));

  // Summary //
  msg::info << "# Summary" << msg::endl;
  for(int pp = 0; pp < lvlP; pp++){
    msg::info << "  * Order: " << pp+pOff << msg::endl;
    for(int hh = 0; hh < lvlH; hh++)
      msg::info << msg::fixed
                << "    + h: " << h[hh]
                << msg::scientific
                << " --> " << error[pp][hh] << msg::endl;
    for(int hh = 0; hh < lvlH-1; hh++)
      msg::info << "    + s: " << hh
                << " --> " << slope[pp][hh] << msg::endl;
  }

  // Check slopes and return //
  int status = 0;
  for(int pp = 0; pp < lvlP; pp++){
    for(int hh = 0; hh < lvlH-1; hh++){
      if(-slope[pp][hh] < (pp+1)*(1-pp*hpTol)){
        status = -1;
        msg::error << "Bad slope for (p, dh) = "
                   << "(" << pp+pOff << ", " << hh << ")"
                   << msg::endl;
      }
    }
  }

  return status;
}
