#include "gmsh.h"

#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshfem/Message.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;

void waveguide(const unsigned int nDom, const double a, const double b,
               const double L, const double lc)
{
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2."
                        << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("waveguide");

  const double domainSize = L / nDom;

  std::vector< std::vector< int > > borders(nDom);
  std::vector< int > omegas(nDom);
  std::vector< int > sigmas(nDom - 1);

  int p[4];

  p[0] = gmsh::model::geo::addPoint(0., 0, 0., lc);
  p[1] = gmsh::model::geo::addPoint(0, a, 0., lc);
  p[2] = gmsh::model::geo::addPoint(0, a, b, lc);
  p[3] = gmsh::model::geo::addPoint(0, 0., b, lc);
  int line[4];

  line[0] = gmsh::model::geo::addLine(p[0], p[1]);
  line[1] = gmsh::model::geo::addLine(p[2], p[1]);
  line[2] = gmsh::model::geo::addLine(p[2], p[3]);
  line[3] = gmsh::model::geo::addLine(p[3], p[0]);

  int curveLoop =
    gmsh::model::geo::addCurveLoop({line[3], line[0], -line[1], line[2]});

  int tagSurface = gmsh::model::geo::addPlaneSurface({curveLoop});
  gmsh::model::geo::mesh::setTransfiniteSurface(tagSurface);
  gmsh::model::geo::mesh::setRecombine(2, tagSurface);
  gmsh::model::addPhysicalGroup(2, {tagSurface}, 1);
  gmsh::model::setPhysicalName(2, 1, "gammaDir");

  for(unsigned int i = 0; i < nDom; ++i) {
    std::vector< std::pair< int, int > > extrudeEntities;
    gmsh::model::geo::extrude(
      {std::make_pair(2, tagSurface)}, domainSize, 0., 0., extrudeEntities,
      {static_cast< int >((domainSize) / lc)}, std::vector< double >(), true);
    tagSurface = extrudeEntities[0].second;
    omegas[i] = extrudeEntities[1].second;
    borders[i].push_back(extrudeEntities[2].second);
    borders[i].push_back(extrudeEntities[3].second);
    borders[i].push_back(extrudeEntities[4].second);
    borders[i].push_back(extrudeEntities[5].second);
    sigmas[i] = tagSurface;
  }

  gmsh::model::addPhysicalGroup(2, {tagSurface}, 2);
  gmsh::model::setPhysicalName(2, 2, "gammaInf");

  for(unsigned int i = 0; i < borders.size(); ++i) {
    gmsh::model::addPhysicalGroup(2, borders[i], 100 + i);
    gmsh::model::setPhysicalName(2, 100 + i, "border_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < omegas.size(); ++i) {
    gmsh::model::addPhysicalGroup(3, {omegas[i]}, i + 1);
    gmsh::model::setPhysicalName(3, i + 1, "omega_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < sigmas.size(); ++i) {
    gmsh::model::addPhysicalGroup(2, {sigmas[i]}, 200 + i);
    gmsh::model::setPhysicalName(
      2, 200 + i, "sigma_" + std::to_string(i) + "_" + std::to_string(i + 1));
  }
  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate();
}

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);
  /****
   * input Data
   ****/
  double pi = 3.14159265359;
  int nDom = 2;
  double L = 2;
  double a = 1;
  double b = 1;
  double k = 10;
  double mode_m = 2.;
  double mode_n = 1.;
  int FEorder = 0;
  double lc = 0.5;
  bool ddm = true;
  std::string gauss = "Gauss4";
  gmshDdm.userDefinedParameter(ddm, "ddm");
  gmshDdm.userDefinedParameter(nDom, "nDom");
  gmshDdm.userDefinedParameter(L, "L");
  gmshDdm.userDefinedParameter(a, "a");
  gmshDdm.userDefinedParameter(b, "b");
  gmshDdm.userDefinedParameter(lc, "lc");
  gmshDdm.userDefinedParameter(k, "k");
  gmshDdm.userDefinedParameter(FEorder, "FEorder");
  gmshDdm.userDefinedParameter(gauss, "gauss");
  double ky = mode_m * pi / a;
  double kz = mode_n * pi / b;
  double kc = sqrt(ky * ky + kz * kz);
  std::complex< double > im(0., 1.);
  std::complex< double > beta;
  if(-kc * kc + k * k >= 0) {
    beta = sqrt(-kc * kc + k * k);
  }
  else {
    beta = -im * sqrt(kc * kc - k * k);
  }
  // TM
  VectorFunction< std::complex< double > > Einc = vector< std::complex< double > >(
    sin< std::complex< double > >(ky * y< std::complex< double > >()) *
      sin< std::complex< double > >(kz * z< std::complex< double > >()),
    im * beta * ky / (kc * kc) *
      cos< std::complex< double > >(ky * y< std::complex< double > >()) *
      sin< std::complex< double > >(kz * z< std::complex< double > >()),
    im * beta * kz / (kc * kc) *
      cos< std::complex< double > >(kz * z< std::complex< double > >()) *
      sin< std::complex< double > >(ky * y< std::complex< double > >()));
  /****
     * FEM
     ****/
  if(!ddm) {
    /****
    * Gmsh part
    ****/
    gmsh::model::add("waveguide");
    const double domainSize = L;
    std::vector< std::vector< int > > borders(1);
    std::vector< int > omegas(1);
    int p[4];
    p[0] = gmsh::model::geo::addPoint(0., 0, 0., lc);
    p[1] = gmsh::model::geo::addPoint(0, a, 0., lc);
    p[2] = gmsh::model::geo::addPoint(0, a, b, lc);
    p[3] = gmsh::model::geo::addPoint(0, 0., b, lc);
    int line[4];

    line[0] = gmsh::model::geo::addLine(p[0], p[1]);
    line[1] = gmsh::model::geo::addLine(p[2], p[1]);
    line[2] = gmsh::model::geo::addLine(p[2], p[3]);
    line[3] = gmsh::model::geo::addLine(p[3], p[0]);

    int curveLoop =
      gmsh::model::geo::addCurveLoop({line[3], line[0], -line[1], line[2]});

    int tagSurface = gmsh::model::geo::addPlaneSurface({curveLoop});
    gmsh::model::geo::mesh::setTransfiniteSurface(tagSurface);
    gmsh::model::geo::mesh::setRecombine(2, tagSurface);
    gmsh::model::addPhysicalGroup(2, {tagSurface}, 1);
    gmsh::model::setPhysicalName(2, 1, "gammaDir");
    std::vector< std::pair< int, int > > extrudeEntities;
    gmsh::model::geo::extrude(
      {std::make_pair(2, tagSurface)}, domainSize, 0., 0., extrudeEntities,
      {static_cast< int >((domainSize) / lc)}, std::vector< double >(), true);
    tagSurface = extrudeEntities[0].second;
    omegas[0] = extrudeEntities[1].second;
    borders[0].push_back(extrudeEntities[2].second);
    borders[0].push_back(extrudeEntities[3].second);
    borders[0].push_back(extrudeEntities[4].second);
    borders[0].push_back(extrudeEntities[5].second);

    gmsh::model::addPhysicalGroup(2, {tagSurface}, 2);
    gmsh::model::setPhysicalName(2, 2, "gammaInf");
    gmsh::model::addPhysicalGroup(2, borders[0], 100);
    gmsh::model::setPhysicalName(2, 100, "border_" + std::to_string(0));
    gmsh::model::addPhysicalGroup(3, {omegas[0]}, 1);
    gmsh::model::setPhysicalName(3, 1, "omega_" + std::to_string(0));

    gmsh::model::geo::synchronize();
    gmsh::model::mesh::generate();
    Domain omega(3, 1);
    Domain gammaInf(2, 2);
    Domain gammaDir(2, 1);
    Domain border(2, 100);
    gmshfem::problem::Formulation< std::complex< double > > formulation("maxwell");

    /****
     *  Create fields
     ****/
    Field< std::complex< double >, Form::Form1 > E(
      "E", omega | gammaDir | gammaInf | border,
      FunctionSpaceTypeForm1::HierarchicalHCurl, FEorder);
    Field< std::complex< double >, Form::Form1 > lambda(
      "lambda", gammaDir, FunctionSpaceTypeForm1::HierarchicalHCurl,
      FEorder);
    // VectorFunction<std::complex<double>> n = normal<std::complex<double>>();
    // VOLUME TERMS

    E.addConstraint(border, vector< std::complex< double > >(0., 0., 0.));
    lambda.addConstraint(border, vector< std::complex< double > >(0., 0., 0.));

    formulation.integral(curl(dof(E)), curl(tf(E)), omega, gauss);
    formulation.integral(-k * k * dof(E), tf(E), omega, gauss);
    // Silver Muller
    // formulation.integral(- im * k * n % (dof(E) % n), tf(E), gammaInf,
    // gauss);
    formulation.integral(-im * k * dof(E), tf(E), gammaInf, gauss);
    // Physical source using Lagrange mutiplier
    formulation.integral(dof(lambda), tf(E), gammaDir, gauss);
    formulation.integral(dof(E), tf(lambda), gammaDir, gauss);
    formulation.integral(Einc, tf(lambda), gammaDir, gauss);
    formulation.pre();
    formulation.assemble();
    formulation.solve();
    save(E, omega, "E");
  }
  /****
   * DDM
   ****/
  else {
    /****
     *  Build geometry and mesh using gmsh API
     ****/
    waveguide(nDom, a, b, L, lc);

    /****
     *  Define domains
     ****/
    Subdomain omega(nDom);
    Subdomain gammaInf(nDom);
    Subdomain gammaDir(nDom);
    Subdomain border(nDom);
    Interface sigma(nDom);
    for(unsigned int i = 0; i < unsigned(nDom); ++i) {
      omega(i) = Domain(3, i + 1);

      if(i == unsigned(nDom) - 1) {
        gammaInf(i) = Domain(2, 2);
      }

      if(i == 0) {
        gammaDir(i) = Domain(2, 1);
      }

      border(i) = Domain(2, 100 + i);

      if(i != 0) {
        sigma(i, i - 1) = Domain(2, 200 + i - 1);
      }
      if(i != unsigned(nDom) - 1) {
        sigma(i, i + 1) = Domain(2, 200 + i);
      }
    }
    /****
     *  Define  topology
     ****/
    std::vector< std::vector< unsigned int > > topology(nDom);
    for(unsigned int i = 0; i < unsigned(nDom); ++i) {
      if(i != 0) {
        topology[i].push_back(i - 1);
      }
      if(i != unsigned(nDom) - 1) {
        topology[i].push_back(i + 1);
      }
    }
    // Create DDM formulation
    gmshddm::problem::Formulation< std::complex< double > > formulation("Maxwell",
                                                                        topology);
    // Create fields
    SubdomainField< std::complex< double >, Form::Form1 > E(
      "E", omega | gammaDir | gammaInf | sigma,
      FunctionSpaceTypeForm1::HierarchicalHCurl, FEorder);
    SubdomainField< std::complex< double >, Form::Form1 > lambda(
      "lambda", gammaDir, FunctionSpaceTypeForm1::HierarchicalHCurl,
      FEorder);
    InterfaceField< std::complex< double >, Form::Form1 > g(
      "g", sigma, FunctionSpaceTypeForm1::HierarchicalHCurl, 0);


    // Tell to the formulation that g is field that have to be exchanged between
    // subdomains
    formulation.addInterfaceField(g);
    VectorFunction< std::complex< double > > n = normal< std::complex< double > >();
    VectorFunction< std::complex< double > > functionZero =
      vector< std::complex< double > >(0., 0., 0.);
    for(unsigned int i = 0; i < unsigned(nDom); ++i) {
      E(i).addConstraint(border(i), functionZero);
      lambda(i).addConstraint(border(i), functionZero);
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        g(i, j).addConstraint(border(i), functionZero);
      }
    }

    // Add terms to the formulation
    for(unsigned int i = 0; i < unsigned(nDom); ++i) {
      // VOLUME TERMS
      formulation(i).integral(curl(dof(E(i))), curl(tf(E(i))), omega(i), gauss);
      formulation(i).integral(-k * k * dof(E(i)), tf(E(i)), omega(i), gauss);
      // Silver Muller
      /*  formulation(i).integral(-im * k * n % (dof(E(i)) % n), tf(E(i)),
                                gammaInf(i), gauss);*/
      formulation(i).integral(-im * k * dof(E(i)), tf(E(i)), gammaInf(i),
                              gauss);
      // Physical source using Lagrange mutiplier
      formulation(i).integral(dof(lambda(i)), tf(E(i)), gammaDir(i),
                              gauss);
      formulation(i).integral(dof(E(i)), tf(lambda(i)), gammaDir(i),
                              gauss);
      formulation(i).integral(formulation.physicalSource(Einc), tf(lambda(i)),
                              gammaDir(i), gauss);

      // Artificial source on transmission boundaries
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        /*  formulation(i).integral(-im * k * n % (dof(E(i)) % n), tf(E(i)),
                                  sigma(i, j), gauss);*/
        formulation(i).integral(-im * k * dof(E(i)), tf(E(i)), sigma(i, j),
                                gauss);
        formulation(i).integral(formulation.artificialSource(g(j, i)), tf(E(i)),
                                sigma(i, j), gauss);
      }
      // INTERFACE TERMS
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i, j).integral(dof(g(i, j)), tf(g(i, j)), sigma(i, j),
                                   gauss);
        formulation(i, j).integral(formulation.artificialSource(g(j, i)),
                                   tf(g(i, j)), sigma(i, j), gauss);
        formulation(i, j).integral(-2. * im * k * E(i), tf(g(i, j)),
                                   sigma(i, j), gauss);
        /*formulation(i, j).integral(-2. * im * k * n % (E(i) % n), tf(g(i, j)),
                                   sigma(i, j), gauss);*/
      }
    }
    formulation.pre();
    // Solve the DDM formulation
    formulation.solve("gmres");
    //save field E
    for(int i = 0; i < nDom; ++i) {
      save(E(i), omega(i), "E" + std::to_string(i));
    }
  }

  return 0;
}
