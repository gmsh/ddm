#include "gmsh.h"

#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshddm/MPIInterface.h>
#include <gmshfem/Message.h>


using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;


// Create a [0,2]x[0,1] rectanle splitted horizontally in nDom domains.
void createDomain(const unsigned int nDom, const double lc) {
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2." << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("Poisson");

  const double domainSize = 2.0 / nDom;

  std::vector< std::vector< int > > borders(nDom);
  std::vector< int > omegas(nDom);
  std::vector< int > sigmas(nDom - 1);

  std::vector<int> points(2 * (nDom + 1)); // Bottom point, then top point, from left to right
  
  points[0] = gmsh::model::geo::addPoint(0., 0., 0., lc);
  points[1] = gmsh::model::geo::addPoint(0., 1., 0., lc);
  int prevLine = gmsh::model::geo::addLine(points[0], points[1]); // Oriented upwards
  gmsh::model::geo::synchronize();

  int gammaLeft = gmsh::model::addPhysicalGroup(1, {prevLine}, -1);
  gmsh::model::setPhysicalName(1, gammaLeft, "gamma_left");


  for (unsigned i = 1; i <= nDom; ++i) {
    points[2*i] = gmsh::model::geo::addPoint(domainSize * i, 0., 0., lc);
    points[2*i+1] = gmsh::model::geo::addPoint(domainSize * i, 1., 0., lc);
    int lowerLine = gmsh::model::geo::addLine(points[2*(i-1)], points[2*i]);
    int upperLine = gmsh::model::geo::addLine(points[2*i+1], points[2*i-1]);
    int couplingLine = gmsh::model::geo::addLine(points[2*i], points[2*i+1]);

    gmsh::model::geo::synchronize();
    int couplingLineID = gmsh::model::addPhysicalGroup(1, {couplingLine});
    if (i != nDom) {
      gmsh::model::setPhysicalName(1, couplingLineID, "border_" + std::to_string(i-1));
    }
    else {
      gmsh::model::setPhysicalName(1, couplingLineID, "gamma_right");
    }

    // Create surface
    int curve = gmsh::model::geo::addCurveLoop({-prevLine, lowerLine, couplingLine, upperLine});
    int surface = gmsh::model::geo::addPlaneSurface({curve});
      
    gmsh::model::geo::synchronize();

    prevLine = couplingLine;

    omegas[i-1] = gmsh::model::addPhysicalGroup(2, {surface});
    gmsh::model::setPhysicalName(2, omegas[i-1], "omega_" + std::to_string(i-1));
  }

  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate();


}

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  unsigned int nDom = 4;
  int order = 1;
  double lc = 0.03;
  std::string gauss = "Gauss10";

  gmshDdm.userDefinedParameter(nDom, "nDom");
  gmshDdm.userDefinedParameter(order, "order");
  gmshDdm.userDefinedParameter(lc, "lc");
  gmshDdm.userDefinedParameter(gauss, "gauss");

  createDomain(nDom, lc);

  Domain left("gamma_left");
  Domain right("gamma_right");


  Subdomain omega(nDom);
  Interface sigma(nDom);
  std::vector< std::vector< unsigned int > > topology(nDom);

  for (unsigned i = 0; i < nDom; ++i) {
    omega(i) = Domain("omega_" + std::to_string(i));
    if (i != 0) {
      sigma(i, i-1) = Domain("border_" + std::to_string(i-1));
      topology[i].push_back(i-1);
    }
    if (i != nDom - 1) {
      sigma(i, i+1) = Domain("border_" + std::to_string(i));
      topology[i].push_back(i+1);
    }
    
  }

  gmshddm::problem::Formulation< std::complex< double > > formulation("Laplace", topology);
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega,  FunctionSpaceTypeForm0::HierarchicalH1, order);

  u(0).addConstraint(left, 0);
  u(nDom-1).addConstraint(right, 0);
  auto trueSolution = 0.5 * x<std::complex< double >>() * (2.0 - x<std::complex< double >>());

  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, order);
  formulation.addInterfaceField(g);

  double alpha = 1; // Coupling operator

  for (unsigned i = 0; i < nDom; ++i) {
    std::cout << "Setting up " << i << std::endl;
    // Domain
    formulation(i).integral(grad(dof(u(i))), grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral(formulation.physicalSource(-1.0), tf(u(i)), omega(i), gauss);

    // Artificial sources
    for(int j : topology[i]) {
      // du/dn + a*u = g => du/dn = g - a*u, but we add -du/dn from the negative laplacien
      formulation(i).integral(formulation.artificialSource(-g(j, i)), tf(u(i)), sigma(i, j), gauss);
      formulation(i).integral(alpha * (dof(u(i))), tf(u(i)), sigma(i, j), gauss);
    }

    // Interface equations
    for(int j : topology[i]) {
      formulation(i, j).integral(dof(g(i, j)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(formulation.artificialSource(g(j, i)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(((-2.0 * alpha) * u(i)), tf(g(i, j)), sigma(i, j), gauss);
    }
  }


  formulation.pre();
  int iterMax = 1000;
  gmshDdm.userDefinedParameter(iterMax, "iter");
  formulation.solve("gmres", 1e-8, iterMax);


  for(unsigned int i = 0; i < nDom; ++i) {
    if(isItMySubdomain(i)) {
      save(u(i), omega(i), "u_" + std::to_string(i));
      save(u(i) - trueSolution, omega(i), "err_" + std::to_string(i));
    }
  }

  return 0;

}
