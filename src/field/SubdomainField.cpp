// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "SubdomainField.h"

#include <gmshfem/Exception.h>
#include <gmshfem/Post.h>
#include <string>
#include "MPIInterface.h"


namespace gmshddm
{


  namespace field
  {

    template< class T_Scalar, gmshfem::field::Form T_Form >
    SubdomainField< T_Scalar, T_Form >::SubdomainField() :
      _name(), _fields()
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    SubdomainField< T_Scalar, T_Form >::SubdomainField(const std::string &name, const domain::Subdomain &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const unsigned int degree) :
      _name(name), _fields()
    {
      _fields.reserve(domains.numberOfSubdomains());
      for(auto i = 0U; i < domains.numberOfSubdomains(); ++i) {
        _fields.emplace_back(_name + "_" + std::to_string(i), domains(i), type, degree);
      }
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    SubdomainField< T_Scalar, T_Form >::SubdomainField(const std::string &name, const std::vector< gmshfem::field::Field< T_Scalar, T_Form > > &fields) :
      _name(name), _fields(fields)
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    SubdomainField< T_Scalar, T_Form >::SubdomainField(const SubdomainField< T_Scalar, T_Form > &other) :
      _name(other._name), _fields(other._fields)
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    SubdomainField< T_Scalar, T_Form >::SubdomainField(SubdomainField< T_Scalar, T_Form > &&other) :
      _name(std::move(other._name)), _fields(std::move(other._fields))
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    SubdomainField< T_Scalar, T_Form > &SubdomainField< T_Scalar, T_Form >::operator=(const SubdomainField< T_Scalar, T_Form > &other)
    {
      _name = other._name;
      _fields = other._fields;
      return *this;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    SubdomainField< T_Scalar, T_Form >::~SubdomainField()
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    gmshfem::field::Form SubdomainField< T_Scalar, T_Form >::form() const
    {
      return T_Form;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    unsigned long long SubdomainField< T_Scalar, T_Form >::size() const
    {
      return _fields.size();
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    const gmshfem::field::Field< T_Scalar, T_Form > &SubdomainField< T_Scalar, T_Form >::operator()(const unsigned int i) const
    {
      if(i >= _fields.size()) {
        throw gmshfem::common::Exception("Trying to access field(" + std::to_string(i) + ") of field '" + _name + "'");
      }

      return _fields[i];
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    gmshfem::field::Field< T_Scalar, T_Form > &SubdomainField< T_Scalar, T_Form >::operator()(const unsigned int i)
    {
      if(i >= _fields.size()) {
        throw gmshfem::common::Exception("Trying to access field(" + std::to_string(i) + ") of field '" + _name + "'");
      }

      return _fields[i];
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    gmshfem::domain::Domain SubdomainField< T_Scalar, T_Form >::getLocalDomain() const
    {

      gmshfem::domain::Domain domainTotal;
      for(unsigned i = 0; i < _fields.size(); ++i) {
        if(gmshddm::mpi::isItMySubdomain(i)) {
          const auto &field = _fields[i];
          domainTotal |= field.domain();
        }
      }

      return domainTotal;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form >
    gmshfem::function::PiecewiseFunction< T_Scalar, gmshfem::field::DegreeOfForm< T_Form >::value > SubdomainField< T_Scalar, T_Form >::buildUnifiedField() const
    {

      decltype(buildUnifiedField()) piecewiseFunc;

      for(unsigned i = 0; i < _fields.size(); ++i) {
        if(gmshddm::mpi::isItMySubdomain(i)) {
          const auto &field = _fields[i];
          piecewiseFunc.addFunction(field, field.domain());
        }
      }

      return piecewiseFunc;
    }


    template class SubdomainField< std::complex< double >, gmshfem::field::Form::Form0 >;
    template class SubdomainField< std::complex< double >, gmshfem::field::Form::Form1 >;
    template class SubdomainField< std::complex< double >, gmshfem::field::Form::Form2 >;
    template class SubdomainField< std::complex< double >, gmshfem::field::Form::Form3 >;

    template class SubdomainField< double, gmshfem::field::Form::Form0 >;
    template class SubdomainField< double, gmshfem::field::Form::Form1 >;
    template class SubdomainField< double, gmshfem::field::Form::Form2 >;
    template class SubdomainField< double, gmshfem::field::Form::Form3 >;


    template class SubdomainField< std::complex< float >, gmshfem::field::Form::Form0 >;
    template class SubdomainField< std::complex< float >, gmshfem::field::Form::Form1 >;
    template class SubdomainField< std::complex< float >, gmshfem::field::Form::Form2 >;
    template class SubdomainField< std::complex< float >, gmshfem::field::Form::Form3 >;

    template class SubdomainField< float, gmshfem::field::Form::Form0 >;
    template class SubdomainField< float, gmshfem::field::Form::Form1 >;
    template class SubdomainField< float, gmshfem::field::Form::Form2 >;
    template class SubdomainField< float, gmshfem::field::Form::Form3 >;


  } // namespace field


} // namespace gmshddm
