// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_SUBDOMAINFIELD
#define H_GMSHDDM_SUBDOMAINFIELD

#include "Subdomain.h"

#include <gmshfem/FieldInterface.h>
#include <string>
#include <vector>


namespace gmshddm
{


  namespace field
  {


    template< class T_Scalar, gmshfem::field::Form T_Form >
    class SubdomainField
    {
     private:
      std::string _name;
      std::vector< gmshfem::field::Field< T_Scalar, T_Form > > _fields;

     public:
      SubdomainField();
      SubdomainField(const std::string &name, const domain::Subdomain &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const unsigned int degree = 1);
      SubdomainField(const std::string &name, const std::vector< gmshfem::field::Field< T_Scalar, T_Form > > &fields);
      SubdomainField(const SubdomainField< T_Scalar, T_Form > &other);
      SubdomainField(SubdomainField< T_Scalar, T_Form > &&other);
      ~SubdomainField();

      SubdomainField &operator=(const SubdomainField< T_Scalar, T_Form > &other);

      gmshfem::field::Form form() const;
      unsigned long long size() const;

      const gmshfem::field::Field< T_Scalar, T_Form > &operator()(const unsigned int i) const;
      gmshfem::field::Field< T_Scalar, T_Form > &operator()(const unsigned int i);

      gmshfem::function::PiecewiseFunction< T_Scalar, gmshfem::field::DegreeOfForm< T_Form >::value > buildUnifiedField() const;
      gmshfem::domain::Domain getLocalDomain() const;
    };


  } // namespace field


} // namespace gmshddm


#endif // H_GMSHDDM_SUBDOMAINFIELD
