// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "InterfaceFieldInterface.h"

#include <gmshfem/Exception.h>

namespace gmshddm
{


  namespace field
  {


    template< class T_Scalar >
    InterfaceFieldInterface< T_Scalar >::InterfaceFieldInterface(const std::string &name) :
      _name(name)
    {
    }

    template< class T_Scalar >
    InterfaceFieldInterface< T_Scalar >::~InterfaceFieldInterface()
    {
    }

    template< class T_Scalar >
    std::string InterfaceFieldInterface< T_Scalar >::name() const
    {
      return _name;
    }


    template class InterfaceFieldInterface< std::complex< double > >;
    template class InterfaceFieldInterface< double >;
    template class InterfaceFieldInterface< std::complex< float > >;
    template class InterfaceFieldInterface< float >;


  } // namespace field


} // namespace gmshddm
