// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_INTERFACEFIELDINTERFACE
#define H_GMSHDDM_INTERFACEFIELDINTERFACE

#include "Interface.h"

#include <gmshfem/FieldInterface.h>
#include <string>
#include <unordered_map>
#include <vector>


namespace gmshddm
{


  namespace field
  {

    template< class T_Scalar >
    class InterfaceFieldInterface
    {
     protected:
      std::string _name;

     public:
      InterfaceFieldInterface(const std::string &name = "");
      virtual ~InterfaceFieldInterface();

      virtual bool isDefined(const unsigned int i, const unsigned int j) const = 0;
      virtual const gmshfem::field::FieldInterface< T_Scalar > &operator()(const unsigned int i, const unsigned int j) const = 0;
      virtual gmshfem::field::FieldInterface< T_Scalar > &operator()(const unsigned int i, const unsigned int j) = 0;

      std::string name() const;
    };


  } // namespace field


} // namespace gmshddm


#endif // H_GMSHDDM_INTERFACEFIELDINTERFACE
