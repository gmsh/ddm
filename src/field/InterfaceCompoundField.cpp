// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "InterfaceCompoundField.h"

#include <gmshfem/Exception.h>

namespace gmshddm
{


  namespace field
  {


    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::InterfaceCompoundField() :
      InterfaceFieldInterface< T_Scalar >(), _fields()
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::InterfaceCompoundField(const std::string &name, const domain::Interface &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const unsigned int degree) :
      InterfaceFieldInterface< T_Scalar >(name), _fields(domains.numberOfSubdomains())
    {
      for(auto i = 0U; i < domains.numberOfSubdomains(); ++i) {
        for(auto it = domains[i].begin(); it != domains[i].end(); ++it) {
          _fields[i].emplace(it->first, gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields >(this->_name + "_" + std::to_string(i) + "_" + std::to_string(it->first), it->second, type, degree));
        }
      }
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::InterfaceCompoundField(const std::string &name, const domain::Interface &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const std::vector< unsigned int > &degree) :
      InterfaceFieldInterface< T_Scalar >(name), _fields(domains.numberOfSubdomains())
    {
      if(domains.numberOfSubdomains() > degree.size()) {
        throw gmshfem::common::Exception("Wrong size of degree");
      }
      for(unsigned int i = 0; i < domains.numberOfSubdomains(); ++i) {
        for(auto it = domains[i].begin(); it != domains[i].end(); ++it) {
          _fields[i].emplace(it->first, gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields >(this->_name + "_" + std::to_string(i) + "_" + std::to_string(it->first), it->second, type, degree[i]));
        }
      }
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::InterfaceCompoundField(const InterfaceCompoundField< T_Scalar, T_Form, T_NumFields > &other) :
      InterfaceFieldInterface< T_Scalar >(other._name), _fields(other._fields)
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::InterfaceCompoundField(InterfaceCompoundField< T_Scalar, T_Form, T_NumFields > &&other) :
      InterfaceFieldInterface< T_Scalar >(other._name), _fields(std::move(other._fields))
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    InterfaceCompoundField< T_Scalar, T_Form, T_NumFields > &InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::operator=(const InterfaceCompoundField< T_Scalar, T_Form, T_NumFields > &other)
    {
      this->_name = other._name;
      _fields = other._fields;
      return *this;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::~InterfaceCompoundField()
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    gmshfem::field::Form InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::form() const
    {
      return T_Form;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    bool InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::isDefined(const unsigned int i, const unsigned int j) const
    {
      auto it = _fields[i].find(j);
      if(it == _fields[i].end()) {
        return false;
      }
      return true;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    const gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > &InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::operator()(const unsigned int i, const unsigned int j) const
    {
      if(i >= _fields.size()) {
        throw gmshfem::common::Exception("Trying to access field(" + std::to_string(i) + ", " + std::to_string(j) + ") of field '" + this->_name + "'");
      }
      auto it = _fields[i].find(j);
      if(it == _fields[i].end()) {
        throw gmshfem::common::Exception("Trying to access field(" + std::to_string(i) + ", " + std::to_string(j) + ") of field '" + this->_name + "'");
      }

      return it->second;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > &InterfaceCompoundField< T_Scalar, T_Form, T_NumFields >::operator()(const unsigned int i, const unsigned int j)
    {
      if(i >= _fields.size()) {
        throw gmshfem::common::Exception("Trying to access field(" + std::to_string(i) + ", " + std::to_string(j) + ") of field '" + this->_name + "'");
      }
      auto it = _fields[i].find(j);
      if(it == _fields[i].end()) {
        throw gmshfem::common::Exception("Try to access field(" + std::to_string(i) + ", " + std::to_string(j) + ") of field '" + this->_name + "'");
      }

      return it->second;
    }


    template class InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >;
    template class InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form1, 2 >;
    template class InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form2, 2 >;
    template class InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form3, 2 >;

    template class InterfaceCompoundField< double, gmshfem::field::Form::Form0, 2 >;
    template class InterfaceCompoundField< double, gmshfem::field::Form::Form1, 2 >;
    template class InterfaceCompoundField< double, gmshfem::field::Form::Form2, 2 >;
    template class InterfaceCompoundField< double, gmshfem::field::Form::Form3, 2 >;


    template class InterfaceCompoundField< std::complex< float >, gmshfem::field::Form::Form0, 2 >;
    template class InterfaceCompoundField< std::complex< float >, gmshfem::field::Form::Form1, 2 >;
    template class InterfaceCompoundField< std::complex< float >, gmshfem::field::Form::Form2, 2 >;
    template class InterfaceCompoundField< std::complex< float >, gmshfem::field::Form::Form3, 2 >;

    template class InterfaceCompoundField< float, gmshfem::field::Form::Form0, 2 >;
    template class InterfaceCompoundField< float, gmshfem::field::Form::Form1, 2 >;
    template class InterfaceCompoundField< float, gmshfem::field::Form::Form2, 2 >;
    template class InterfaceCompoundField< float, gmshfem::field::Form::Form3, 2 >;


    template class InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >;
    template class InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form1, 3 >;
    template class InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form2, 3 >;
    template class InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form3, 3 >;

    template class InterfaceCompoundField< double, gmshfem::field::Form::Form0, 3 >;
    template class InterfaceCompoundField< double, gmshfem::field::Form::Form1, 3 >;
    template class InterfaceCompoundField< double, gmshfem::field::Form::Form2, 3 >;
    template class InterfaceCompoundField< double, gmshfem::field::Form::Form3, 3 >;


    template class InterfaceCompoundField< std::complex< float >, gmshfem::field::Form::Form0, 3 >;
    template class InterfaceCompoundField< std::complex< float >, gmshfem::field::Form::Form1, 3 >;
    template class InterfaceCompoundField< std::complex< float >, gmshfem::field::Form::Form2, 3 >;
    template class InterfaceCompoundField< std::complex< float >, gmshfem::field::Form::Form3, 3 >;

    template class InterfaceCompoundField< float, gmshfem::field::Form::Form0, 3 >;
    template class InterfaceCompoundField< float, gmshfem::field::Form::Form1, 3 >;
    template class InterfaceCompoundField< float, gmshfem::field::Form::Form2, 3 >;
    template class InterfaceCompoundField< float, gmshfem::field::Form::Form3, 3 >;


  } // namespace field


} // namespace gmshddm
