// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_INTERFACEFIELD
#define H_GMSHDDM_INTERFACEFIELD

#include "Interface.h"
#include "InterfaceFieldInterface.h"

#include <gmshfem/FieldInterface.h>
#include <string>
#include <unordered_map>
#include <vector>


namespace gmshddm
{


  namespace field
  {


    template< class T_Scalar, gmshfem::field::Form T_Form >
    class InterfaceField : public InterfaceFieldInterface< T_Scalar >
    {
      std::vector< std::unordered_map< unsigned int, gmshfem::field::Field< T_Scalar, T_Form > > > _fields;

     public:
      InterfaceField();
      InterfaceField(const std::string &name, const domain::Interface &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const unsigned int degree = 1);
      InterfaceField(const std::string &name, const domain::Interface &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const std::vector< unsigned int > &degree);
      InterfaceField(const std::string &name, const domain::Interface &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const std::vector< std::vector< unsigned int > > &degree);
      InterfaceField(const InterfaceField< T_Scalar, T_Form > &other);
      InterfaceField(InterfaceField< T_Scalar, T_Form > &&other);
      ~InterfaceField();

      InterfaceField &operator=(const InterfaceField< T_Scalar, T_Form > &other);

      gmshfem::field::Form form() const;

      bool isDefined(const unsigned int i, const unsigned int j) const override;
      const gmshfem::field::Field< T_Scalar, T_Form > &operator()(const unsigned int i, const unsigned int j) const override;
      gmshfem::field::Field< T_Scalar, T_Form > &operator()(const unsigned int i, const unsigned int j) override;
    };


  } // namespace field


} // namespace gmshddm


#endif // H_GMSHDDM_INTERFACEFIELD
