// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_RESOURCES
#define H_GMSHDDM_RESOURCES

namespace gmshddm
{


  namespace common
  {

    void getResources(double &cpuTime, double &peakRSS, double &currentRSS);
    void s_printResources(const std::string& step, bool first = false);

  }

} // namespace gmshddm

#endif // H_GMSHDDM_RESOURCES
