// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_OPTIONS
#define H_GMSHDDM_OPTIONS

#include "gmshddmDefines.h"
#include "optionsEnums.h"

#include <string>

namespace gmshddm
{


  namespace common
  {


    // Singleton
    class Options
    {
     private:
      static Options *_instance;

     protected:
      Options();
      ~Options();
      Options(const Options &other) = delete;
      Options(Options &&other) = delete;

      Options &operator=(const Options &other) = delete;
      Options &operator=(Options &&other) = delete;

      void _setDefault();

     public:
      bool memory;
      problem::ProcessAffinity processAffinity;

     public:
      static Options *instance();
      static void destroy();
    };


  } // namespace common


} // namespace gmshddm

#endif // H_GMSHDDM_OPTIONS
