// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "Options.h"

namespace gmshddm
{


  namespace common
  {


    // protected
    Options::Options()
    {
      _setDefault();
    }

    // protected
    Options::~Options()
    {
    }

    // protected
    void Options::_setDefault()
    {
      memory = false;
      processAffinity = problem::ProcessAffinity::ABCABC;
    }

    Options *Options::_instance = nullptr;

    Options *Options::instance()
    {
      if(_instance == nullptr) _instance = new Options();

      return _instance;
    }

    void Options::destroy()
    {
      if(_instance != nullptr) {
        delete _instance;
        _instance = nullptr;
      }
    }


  } // namespace common


} // namespace gmshddm
