// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_GMSHDDM
#define H_GMSHDDM_GMSHDDM

#include <string>

namespace gmshfem
{
  namespace common
  {
    class GmshFem;
  }
} // namespace gmshfem

namespace gmshddm
{


  namespace common
  {


    class GmshDdm
    {
     private:
      gmshfem::common::GmshFem *_gmshFem;

     public:
      GmshDdm(int argc, char **argv);
      ~GmshDdm();

      void setVerbosity(const int verbose);

      void userDefinedParameter(int &value, const std::string &name) const;
      void userDefinedParameter(unsigned int &value, const std::string &name) const;
      void userDefinedParameter(double &value, const std::string &name) const;
      void userDefinedParameter(float &value, const std::string &name) const;
      void userDefinedParameter(std::string &value, const std::string &name) const;
      void userDefinedParameter(bool &value, const std::string &name) const;

      static GmshDdm *currentInstance();
    };


  } // namespace common


} // namespace gmshddm

#endif // H_GMSHDDM_GMSHDDM
