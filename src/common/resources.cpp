// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include <cstdio>

#if defined(_WIN32) && !defined(__CYGWIN__)
#include <windows.h>
#include <psapi.h>
#endif

#if !defined(WIN32) || defined(__CYGWIN__)
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>
#endif

#if defined(__APPLE__) && defined(__MACH__)
#include <mach/mach.h>
#endif

#include "GmshDdm.h"
#include "MPIInterface.h"
#include "Options.h"

#include <gmshfem/Message.h>
#include <gmshfem/Timer.h>

#ifdef HAVE_MPI
#include <mpi.h>
#endif

#ifdef HAVE_PETSC
#include <petsc.h>
#endif

namespace gmshddm
{


  namespace common
  {

    void getResources(double &cpuTime, double &peakRSS, double &currentRSS)
    {
      cpuTime = peakRSS = currentRSS = 0.;
#if defined(WIN32) && !defined(__CYGWIN__)
      FILETIME creation, exit, kernel, user;
      if(GetProcessTimes(GetCurrentProcess(), &creation, &exit, &kernel, &user)) {
        cpuTime = 1.e-7 * 4294967296. * (double)user.dwHighDateTime +
                  1.e-7 * (double)user.dwLowDateTime;
      }
      PROCESS_MEMORY_COUNTERS info;
      GetProcessMemoryInfo(GetCurrentProcess(), &info, sizeof(info));
      peakRSS = info.PeakWorkingSetSize / 1024. / 1024.;
      currentRSS = info.WorkingSetSize / 1024. / 1024.;
#else
      struct rusage r;
      getrusage(RUSAGE_SELF, &r);
      cpuTime = (double)r.ru_utime.tv_sec + 1.e-6 * (double)r.ru_utime.tv_usec;
#if defined(__APPLE__)
      peakRSS = r.ru_maxrss / 1024. / 1024.;
      struct mach_task_basic_info info;
      mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;
      if(task_info(mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t)&info, &infoCount) == KERN_SUCCESS)
        currentRSS = info.resident_size / 1024. / 1024.;
#else
      peakRSS = r.ru_maxrss / 1024.;
#if defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
      FILE *fp = fopen("/proc/self/statm", "r");
      long rss = 0L;
      if(fp) {
        if(fscanf(fp, "%*s%ld", &rss) != 1) rss = 0L;
        fclose(fp);
      }
      currentRSS = (rss * sysconf(_SC_PAGESIZE)) / 1024. / 1024.;
#endif
#endif
#endif
    }


    void s_printResources(const std::string &step, bool first = false)
    {
      static gmshfem::common::Timer timeRef;
      if(first) timeRef.tick();
      gmshfem::common::Timer time = timeRef;
      time.tock();

      double cpuTime, peakRSS, currentRSS;
      common::getResources(cpuTime, peakRSS, currentRSS);
      if(mpi::getMPISize() > 1) {
        double val[3] = {cpuTime, peakRSS, currentRSS};
        double min[3] = {val[0], val[1], val[2]};
        double max[3] = {val[0], val[1], val[2]};
        MPI_Reduce(val, min, 3, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
        MPI_Reduce(val, max, 3, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
        if(mpi::getMPIRank() == 0) {
          if(gmshddm::common::Options::instance()->memory) {
            gmshfem::msg::info << "* Resources @ " << step << ": Wall = " << time << "s, "
                               << "CPU = [" << min[0] << "s, " << max[0] << "s], "
                               << "PeakRSS = [" << min[1] << "Mb, " << max[1] << "Mb], "
                               << "CurrentRSS = [" << min[2] << "Mb, " << max[2] << "Mb]"
                               << gmshfem::msg::endl;
          }
        }
        MPI_Barrier(MPI_COMM_WORLD);
      }
      else {
        if(gmshddm::common::Options::instance()->memory) {
          gmshfem::msg::info << "* Resources @ " << step << ": Wall = " << time << "s, "
                             << "CPU = " << cpuTime << "s, "
                             << "PeakRSS = " << peakRSS << "Mb, "
                             << "CurrentRSS = " << currentRSS << "Mb"
                             << gmshfem::msg::endl;
        }
      }
    }


  } // namespace common


} // namespace gmshddm
