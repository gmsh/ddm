## GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
##
## See the LICENSE.txt file for license information. Please report all
## issues on https://gitlab.onelab.info/gmsh/ddm/issues

set(SRC
  Formulation.cpp
  Solvers.cpp
)

file(GLOB HDR RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.h)
append_gmshddm_src(problem "${SRC};${HDR}")
