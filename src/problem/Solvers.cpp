// GmshDDM - Copyright (C) 2019-2021, A. Royer, C. Geuzaine, B. Martin, Université de Liège
// GmshDDM - Copyright (C) 2022, A. Royer, C. Geuzaine, B. Martin, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "Solvers.h"

#include "Formulation.h"
#include "MPIInterface.h"
#include "resources.h"

#include <gmshfem/Message.h>

#include <regex>


#ifdef HAVE_PETSC
#include <petsc.h>
#endif


namespace gmshddm
{


  namespace problem
  {


    // Instantiate the required templates
    template class AbstractIterativeSolver< std::complex< double > >;
    template class AbstractIterativeSolver< std::complex< float > >;
    template class JacobiIterativeSolver< std::complex< double > >;
    template class JacobiIterativeSolver< std::complex< float > >;
    template class KSPIterativeSolver< std::complex< double > >;
    template class KSPIterativeSolver< std::complex< float > >;

    template< class T_Scalar >
    void AbstractIterativeSolver< T_Scalar >::_runIterationOperations(const unsigned int iteration, const gmshfem::scalar::Precision< T_Scalar > rnorm)
    {
      _absoluteResidual.push_back(rnorm);
      _relativeResidual.push_back(rnorm / _absoluteResidual[0]);

      const unsigned int MPI_Rank = mpi::getMPIRank();
      if(MPI_Rank == 0) {
        gmshfem::msg::info << " - Iteration " << iteration << ": absolute residual = " << _absoluteResidual.back() << ", relative residual = " << _relativeResidual.back() << gmshfem::msg::endl;
      }
      gmshddm::common::s_printResources("DDM Iteration");
    }

    template< class T_Scalar >
    void JacobiIterativeSolver< T_Scalar >::solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol, int maxIter)
    {


      double r0;
      VecNorm(RHS, NORM_2, &r0);
      const double stopCriterion = relTol * r0;

#ifndef HAVE_PETSC
      gmshfem::msg::error << "PETSc is required for the Jacobi iterative solver." << gmshfem::msg::endl;
#endif

      Vec W; // residual
      VecDuplicate(RHS, &W);

      VecSet(X, 0.);
      PetscReal norm = 1;
      int i = 0;
      do { // jacobi
        MatVectProductA< T_Scalar >(A, X, Y);
        VecAYPX(Y, 1., RHS);
        VecWAXPY(W, -1., X, Y);
        VecCopy(Y, X);

        VecNorm(W, NORM_2, &norm);
        this->_runIterationOperations(i, norm);
        i++;

      } while(norm > stopCriterion && i < maxIter);
      VecDestroy(&W);
    }

    template< class T_Scalar >
    std::string JacobiIterativeSolver< T_Scalar >::solverName() const
    {
      return "jacobi";
    }

    template< class T_Scalar >
    ProductType JacobiIterativeSolver< T_Scalar >::productType() const
    {
      return ProductType::A;
    }


    template< class T_Scalar >
    void KSPIterativeSolver< T_Scalar >::solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol, int maxIter)
    {


#ifndef HAVE_PETSC
      gmshfem::msg::error << "PETSc is required for KSP iterative solvers." << gmshfem::msg::endl;
#endif
      std::string solverBase = _solverName;
      std::set< std::string > solverMod;
      {
        const std::regex sep("\\+");
        std::list< std::string > split;
        std::copy(std::sregex_token_iterator(_solverName.begin(), _solverName.end(), sep, -1),
                  std::sregex_token_iterator(),
                  std::back_inserter(split));

        if(!split.empty()) {
          std::list< std::string >::iterator it = split.begin();
          solverBase = *(it++);
          for(; it != split.end(); it++)
            solverMod.insert(*it);
        }
      }
      KSP ksp;
      PC pc;
      KSPCreate(MPI_COMM_WORLD, &ksp);
      KSPSetFromOptions(ksp);
      KSPSetOperators(ksp, A, A);
      KSPSetType(ksp, solverBase.c_str());
      KSPMonitorSet(ksp, PetscInterface< T_Scalar, PetscScalar, PetscInt >::kspPrint, this, NULL);
      KSPSetTolerances(ksp, relTol, PETSC_DEFAULT, PETSC_DEFAULT, maxIter);
      KSPGetPC(ksp, &pc);
      PCSetType(pc, PCNONE);
      if(solverBase == "gmres") {
        KSPGMRESSetRestart(ksp, maxIter);
        if(solverMod.count("mgs")) {
          KSPGMRESSetOrthogonalization(ksp, KSPGMRESModifiedGramSchmidtOrthogonalization);
          gmshfem::msg::info << "Using modified Gram-Schmidt in GMRES" << gmshfem::msg::endl;
        }
      }
      else if(solverBase == "bcgsl")
        KSPBCGSLSetEll(ksp, 6);
      KSPSolve(ksp, RHS, Y);
      KSPDestroy(&ksp);
    }

    template< class T_Scalar >
    std::string KSPIterativeSolver< T_Scalar >::solverName() const
    {
      return this->_solverName;
    }

    template< class T_Scalar >
    ProductType KSPIterativeSolver< T_Scalar >::productType() const
    {
      return ProductType::I_A;
    }


  } // namespace problem


} // namespace gmshddm
