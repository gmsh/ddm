// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "Interface.h"

#include "Subdomain.h"

#include <gmsh.h>
#include <gmshfem/Message.h>

namespace gmshddm
{


  namespace domain
  {


    Interface::Interface(const unsigned int nDom) :
      _domains(nDom), _emptyDomain()
    {
    }

    Interface::Interface(const Interface &other) :
      _domains(other._domains), _emptyDomain()
    {
    }

    Interface::Interface(Interface &&other) :
      _domains(std::move(other._domains)), _emptyDomain()
    {
    }

    Interface::~Interface()
    {
    }

    unsigned int Interface::numberOfSubdomains() const
    {
      return _domains.size();
    }

    unsigned int Interface::numberOfNeighbors(unsigned int i) const
    {
      return _domains[i].size();
    }

    const gmshfem::domain::Domain &Interface::operator()(unsigned int i, unsigned int j) const
    {
      auto it = _domains[i].find(j);
      if(it == _domains[i].end()) {
        _emptyDomain.clear();
        return _emptyDomain;
      }
      return _domains[i].at(j);
    }

    gmshfem::domain::Domain &Interface::operator()(unsigned int i, unsigned int j)
    {
      auto it = _domains[i].find(j);
      if(it == _domains[i].end()) {
        _domains[i].insert(std::make_pair(j, gmshfem::domain::Domain()));
        return _domains[i].at(j);
      }
      return _domains[i].at(j);
    }

    const std::unordered_map< unsigned int, gmshfem::domain::Domain > &Interface::operator[](unsigned int i) const
    {
      return _domains[i];
    }

    bool Interface::operator==(const Interface &other) const
    {
      return _domains == other._domains;
    }

    bool Interface::operator!=(const Interface &other) const
    {
      return !((*this) == other);
    }

    Interface &Interface::operator=(const Interface &other)
    {
      _domains = other._domains;
      return *this;
    }

    Interface &Interface::operator=(Interface &&other)
    {
      _domains = std::move(other._domains);
      return *this;
    }

    Interface &Interface::operator|=(const Interface &other)
    {
      for(auto i = 0ULL; i < other._domains.size(); ++i) {
        for(auto it = other._domains[i].begin(); it != other._domains[i].end(); ++it) {
          _domains[i][it->first] |= it->second;
        }
      }
      return *this;
    }

    Interface Interface::operator|(const Interface &other) const
    {
      Interface domain(*this);
      domain |= other;

      return domain;
    }

    Interface &Interface::operator&=(const Interface &other)
    {
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        for(auto it = _domains[i].begin(); it != _domains[i].end(); ++it) {
          _domains[i][it->first] &= it->second;
        }
      }
      return *this;
    }

    Interface Interface::operator&(const Interface &other) const
    {
      Interface domain(*this);
      domain &= other;

      return domain;
    }

    Interface Interface::buildInterface(std::vector< std::vector< unsigned int > > &topology)
    {
      topology.clear();

      const int modelDim = gmsh::model::getDimension();
      const int numPart = gmsh::model::getNumberOfPartitions();
      Interface interfaces(numPart);
      topology.resize(numPart);
      std::vector< std::pair< int, int > > entities;
      gmsh::model::getEntities(entities);
      for(auto i = 0ULL; i < entities.size(); ++i) {
        if(entities[i].first == modelDim - 1) {
          std::vector< int > part;
          gmsh::model::getPartitions(entities[i].first, entities[i].second, part);
          if(part.size() == 2) {
            if(part[0] < 1 || part[0] > numPart) {
              throw gmshfem::common::Exception("Invalid Gmsh partition " + std::to_string(part[0]));
            }
            if(part[1] < 1 || part[1] > numPart) {
              throw gmshfem::common::Exception("Invalid Gmsh partition " + std::to_string(part[1]));
            }
            interfaces(part[0] - 1, part[1] - 1).addEntity(entities[i].first, entities[i].second);
            interfaces(part[1] - 1, part[0] - 1).addEntity(entities[i].first, entities[i].second);
            auto it = std::find(topology[part[0] - 1].begin(), topology[part[0] - 1].end(), part[1] - 1);
            if(it == topology[part[0] - 1].end()) {
              topology[part[0] - 1].push_back(part[1] - 1);
            }
            auto it2 = std::find(topology[part[1] - 1].begin(), topology[part[1] - 1].end(), part[0] - 1);
            if(it2 == topology[part[1] - 1].end()) {
              topology[part[1] - 1].push_back(part[0] - 1);
            }
          }
        }
      }

      return interfaces;
    }


  } // namespace domain


} // namespace gmshddm
